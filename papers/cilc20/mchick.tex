% \paragraph{Mutation testing}
% \label{sec:mutaprolog}


%   A killed mutant  should simulate reasonable
% bugs that occur in a program without being planted by ourselves. 

% In previous work we either had to use faulty
% models from the literature~\cite{Pessina15}, or  to resort to
% \emph{manual} generation of mutants~\cite{KomauliM18} to compare
% \acheck with other PBT tools --- but this is obviously biased,
% labor-intensive and very hard to scale.


% %\begin{small}
% \begin{description}
% \item[Inductive mutations:]  deletion of a predicate in the premises of a constructor
%   deleting the whole  if none. 
% \item[Operator mutations:] arithmetic and relational operator
%   mutation, say $<$ in $\leq$. Replacement of disjunction by
%   conjunction and vice versa.
% \item[Variable mutations:] replacing  variables

% \item[Constant mutations:] replacing a constant by a constant (of the
%   same type), or by a variable and vice versa.
% \end{description}
%\end{small}



%\paragraph{MetaCoq}
Our tool is based on MetaCoq~\cite{sozeau2020metacoq}, a library that provides rich meta-programming features for Coq. Among those, we will use
%
\begin{inparaenum}[1)]
\item the (anti)quotation mechanism: quoting a Coq term \lsti{t}, 
  denoted by \lsti{<% t %>},
    induces a bijection between Coq kernel terms % (e.g., the \lsti{has_type} inductive definition of the previous Section)
    and a deep
    embedding in Coq itself of the syntax of the  terms of the underlying Calculus
    of (Co)Inductive Constructions; the embedding is realized as an \lsti{Inductive}
    definition of the type \lsti{term}; % --- in other terms \lsti{<% t : term %>});
    \item a reification of the static and dynamic semantics of the
      representation of the calculus, providing a hook for checking
      well-typedness of quoted terms.
\end{inparaenum}

For example, quoting the lambda expression
\lsti{fun x => 2 + hd x [9;8;7]} that sums $2$ to the head of a given list returns the
following abstract syntax tree (AST), which we have significantly abridged by using a number
such as $2$ to stand for its quotation --- keep in mind that in Coq
naturals and lists are not built-in, but obtained as inductive types.
\begin{lstlisting}
(tLambda (nNamed "x") 
   (tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} [])
   (tApp (tConst "Coq.Init.Nat.add" [])
      [2;tApp (tConst "Coq.Lists.List.hd" [])
         [tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} []; 
          tRel 0;
          tApp (tConstruct {| inductive_mind := "Coq.Init.Datatypes.list"; inductive_ind := 0 |} 1 [])
            [tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} [];
             tApp (tConstruct {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} 1 []) [9;8;7]]]]))
\end{lstlisting}

  
To access and modify Coq's terms and environment, MetaCoq provides a
monad (\lsti{TemplateMonad}) in which it is possible (among other
things) to describe quoting and unquoting actions of Coq terms, as
well as other (few) actions with side-effects, such as raising an exception
and inserting new terms inside Coq's environment; these monadic
meta-programs, once invoked with the \lsti{Run TemplateProgram}
command, will be executed by an interpreter inside the OCaml environment.

%\paragraph{\mutc}
\mutc aims to devise \emph{mutation operators} for a proof assistant,
here Coq (but they would also be relevant for other systems such as \emph{Lean} or \emph{Isabelle/HOL}), which embodies both functional and relational
specifications, with a particular attention to our intended domain of application.
% namely models of programming languages artifacts.
Those operators
specify the mutations that the tool will inject, trying to simulate
natural occurring bugs.  This is justified by the ``competent
programmer assumption''~\cite{Jia:2011}, according to which the latter
tends to develop programs close to the correct version and thus the
difference between current and correct code for each fault is small.


An MA tool is as effective as its operators are
relevant. Historically, MA comes from imperative languages and the
operators thereby still owe to those initially devised for
FORTRAN\@. Even operators proposed for declarative
programs (\cite{MutProlog,mucheck}) make only partial sense, being
untyped and too linked to the operational semantics of the programming
language. Usable examples  comprise arithmetic and relational operator mutation,
say turn $\times$ into $+$ and $<$ into $\leq$ and constant mutations, say
$0$ into $1$.


\begin{table}[t]  
 \begin{center}
   \begin{tabular}{l|l|r} \label{tab:tableOperators}
    \textbf{Kind} & \textbf{Operator} & \textbf{Description}\\
    \hline
	\multirow{2}{*}{General} & Substitute & Substitutes a term with another\\ 
			& Swap & Exchanges two terms\\ 
    		    & IfThenElse & Exchanges then and else branch\\ 
    		    & DelImplications & Deletes assumption\\ 
    		    & UserDefined & User defined operator\\
    \hline
    \multirow{2}{*}{Inductive} & NewConstructor & Adds new constructor\\ 
    	        & SubConstructor & Substitutes a constructor\\ 
            & OnConstructor & Maps an operator over a constructor\\ 
    	        & OnConstructors & Maps an  operator over an inductive def.\\
      \hline
    \end{tabular}
  \end{center}
  \caption{List of \mutc's operators.}
%\vspace{-0.8cm}
\end{table}

Table~\ref{tab:tableOperators} describes, at a high level, the
operators currently implemented in \mutc. We have two
(non-disjoint) main categories: \emph{general} operators, working on
any Coq term, and \emph{inductive} ones, which are particularly suited to relationally defined judgments.

The general syntax to mutate a terms is as follow (see Appendix~\ref{sec:dsl} for the BNF of the DSL):
\begin{lstlisting}
Run TemplateProgram (
    Mutate <% term_to_mutate %>
           using (operator)
           named "new_name").
\end{lstlisting}
%
Let us see a classic arithmetic mutation: we can use simple substitution
\verb+t1 ==> t2+ to substitute every
occurrence of  \lsti{t1} with \lsti{t2}.  This operator is
internally defined as an inductive definition
\lsti|Substitute {A} (t1 t2: A)|, which is indexed by  the type \lsti{A} of the terms in
order to ensure that the substitution is
type-preserving. For example:

% In the example that follows, we use \lsti{Substitute} to change the occurrence of multiplication with addition inside a simple arithmetic expression; note that both functions have the same type, i.e.. \lsti{nat -> nat -> nat}.
% ;

\vspace{5mm}
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram ( 
  Mutate <% 4 * 5 %>
         using (Init.Nat.mul $==$> Init.Nat.add)
         named "out").         
Print out.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[]{Name}
out = 4 + 5
     : nat
\end{lstlisting}
\end{minipage}
\noindent
All the generic operators works not only on plain Coq terms, but also on
\lsti{Definition,Fixpoint,Theorem} etc.

Since a quoted term is a (rather large) AST, 
a {mutation operator} is implemented by two components:
%
\begin{inparaenum}[1)]
%
\item a \emph{search phase}, in which we find \emph{all} locations
  inside the tree of the  to-be-mutated term; this is accomplished by 
  {a function \lsti{check :: term -> bool} that establishes if a
    given quoted term is suitable for mutation;}
%	
\item  a \emph{mutation phase}, in which a transformation function  \lsti{trans :: term -> term} is applied to the terms in the locations previously identified.
\end{inparaenum}

An important optimization in MA is \emph{random generation} of
mutants\cite{Jia:2011}: since a given mutation can be applied multiple
time in the same term, it is reasonable to select only a
sample thereof. Again, a pure language such as Coq cannot rely on an
external pseudo-random number generator, hence we had to deploy our
own  as a \emph{Linear-Feedback Shift Register} on $7$
bits% with feedback polynomial $x^7 + x^6 + 1$
; this generates
pseudo-random sequences of numbers starting from a seed (at this stage
provided by the user).

The DSL offers three choices:
\begin{inparaenum}[1)]
	\item \emph{mutate everywhere};
%
	\item \emph{pick mutations with probability $p$}, i.e., generate a random number for
          every mutable location in the AST:
          if it is bigger then a certain threshold (chosen by the
          user), then collect the mutant;
	%
	\item \emph{pick a maximum of $n$ mutations}, i.e.,  maximum of $n$ location are drawn among all the coordinates found in the \emph{search} phase.
\end{inparaenum}

Rather than seeing each operator in details (for which we
refer once again to~\cite{matteo}), we detail how \mutc realizes the
mutations of the previous Section.

%
% \begin{enumerate}
% \item 
%\paragraph{Swap}
%\label{par:swap} 
For the typo, we use the bidirectional substitution \lsti{Swap t1 t2}; it exchanges the
order of appearance of  \lsti{t1} and \lsti{t2} inside an
expression and is defined in term of \verb+Substitute+. Note that
\lsti{t1} and \lsti{t2} can be (bound) variables, which mirrors a large category of errors.
%[caption=An example of Swap,frame=tlrb]{Name}
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% forall t1, has_type t1 TNat -> has_type (tiszero t1) TBool. %>
         using (Swap <% TBool %>  <% TNat %>)
         named "T_Iszro_M").
\end{lstlisting}
We could have achieved this and the next mutation with the operator
\lsti{SubConstructor} although with less automation.

%\paragraph{DelImplications} \label{par:delimplications}
\lsti{DelImplications} mirrors missing assumptions by non-deterministically  removing implications
(technically, dependent products); in the following case it
will yield 6 different non-isomorphic mutants. 
%
%\vspace{5mm}
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% forall t1 t2 t3 T, has_type t1 TBool -> has_type t2 T -> has_type t3 T -> has_type (tif t1 t2 t3) T %>
         using (DelImplications)
         named "T_Test_M").
\end{lstlisting}
Interestingly, we can make this require less interaction by using the
\lsti{OnConstructors} combinator that maps the operator over the
whole relation; since this could generate a lot of mutants, we use random choice to select a few. 	
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram (
  Run TemplateProgram (
        Mutate <% has_type %>
               using (OnConstructors DelImplications)
               named "has_type_mut"
               generating (RandomMutations 34 64)).
\end{lstlisting}

Finally, we can add a new constructor to an \lsti{Inductive} definition via
%\paragraph{Add a new constructor}
 \lsti{NewConstructor} % (newNameCons: ident) (type: term)}: % the name of the constructor will be \lsti{newNameCons} and its type will be specified as a MetaCoq term.
%%[caption=Adding a constructor,frame=tlrb,label=addConstr]{Name}
\begin{lstlisting}[frame=tlrb,label=addConstr]{Name}
Run TemplateProgram (
 Mutate <% has_type %>
        using (NewConstructor "T_SccB"
              <% fun hastype_M1 => forall t, has_type_M1 t TBool -> 
                 has_type_M1 (tsucc t) TBool %>)
         named "hastype_M1").
\end{lstlisting}





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

%  LocalWords:  MetaCoq bijection CIC NewConstructor newNameCons hd
%  LocalWords:  ident monad TemplateProgram OCaml AST Peano BNF bool
%  LocalWords:  muCheck reification Fixpoint IfThenElse UserDefined
%  LocalWords:  DelImplications SubConstructor OnConstructor
%  LocalWords:  OnConstructors
