Formal verification of software via (interactive) theorem proving
gives the highest level of assurance, but it is still very
labor-intensive. This effort may be furthermore wasted in the design
phase of a software system, where mistakes and changes are likely to
occur both at the level of the specification and of the
implementation.  Testing/validation/model-checking can ameliorate the
situation by finding faults, in a mostly ``push-button'' way and help
refining the spec/code until theorem proving can start.
%
 Even so, when do we stop testing and start proving? This is
 analogous to asking  when to stop testing in the traditional software
 cycle and release the software itself. While taking into the right
 consideration Dijkstra's admonishments, in practical terms it much
 depends on how good our testing suite is.

 In this paper we fix our validation technique to be property-based
 testing~\cite{fink97sen} (PBT), which tries to {refute}
 executable specifications against automatically generated data,
 typically in a pseudo-random way. Once the idea broke out in the
 functional programming community with \textsf{QuickCheck}, it spread
 to most programming languages and turned also in a commercial
 enterprise~\cite{quickcheckfunprofit}.

 We can view PBT as a testing suite that consists of a set of
 properties plus a \emph{data generation strategy}: while PBT tools
 vary in those strategies and in the amount of automation they offer, when
 dealing with random generation uniform distributions are rarely the
 most effective choice and the art of PBT resides in writing very
 sophisticated generators --- see~\cite{HritcuLSADHPV16} for an
 intimidating example. But, how do we assess the fault detection
 capability of such a testing suite? If it stops finding new
 counterexamples, are we willing to consider the system-under-test ready
 for verification?

 Mutation analysis~\cite{Jia:2011} (MA) aims to evaluate software
 testing techniques with a form of white box testing, whereby a source
 program is changed in a localized way by introducing a single
 (syntactic) \emph{fault}. The resulting program is called a
 ``mutant'' and hopefully it is also semantically different from its
 ancestor. A testing suite should recognize the faulted code, which is
 known as ``killing'' the mutant. The higher the number of killed
 mutants, the better the testing suite. While typically used in
 combination with unit testing, MA fits fairly well with PBT (as
 proposed in~\cite{mucheck}) and, in fact, in the literature PBT tools
 are evaluated with manually inserted faults   (e.g., the PLT-redex
 benchmark suite, see
 \url{http://docs.racket-lang.org/redex/benchmark.html}).


 On the verification side, Coq (\url{https://coq.inria.fr}) is one of the
 leading proof assistants, which has shown its color both in
 formalizing general mathematics (see the proof of the Odd Order
 Theorem) and in specific domains such as the one dearest to our
 heart, namely the semantics of programming languages
 (\cite{Leroy09}).

 Coq is an extremely powerful logic, which accommodates programming,
 specification and proving in a constructive setting.  In accordance
 with the Peter Parker principle ``with great power comes great
 responsibility'', PBT in such an environment is no small feat: \qc
 (\cite{paraskevopoulou2015foundational}), while under active
 developments (\cite{GG,Lampropoulos0P19}), brings in additional challenges as we touch upon in the next Section. 

%  Here,
% just look at the listmachine gens: we can test them, conceivably we
% could prove them sound and even in a sense complete~\cite{paraskevopoulou2015foundational}, but
% they have a lot of heuristics builtin --- again, why on earth be 
% in the proving business for testing in the design phase --- and M.A.\
% is a way to evaluate them

 
% \smallskip

 This is where \mutc comes in: \mutc is a mutation analysis tool for
 Coq theories, whose main application is evaluating the ``oracle
 adequacy'' of PBT with \qc.  We accomplish this in a purely logical
 way via meta-programming with MetaCoq~\cite{sozeau2020metacoq}. Since
 any MA tool is as good as the \emph{mutation operators} it
 implements, we provide a DSL to specify and compute
 \emph{type-preserving} mutation operators, supporting both the
 functional and the relational aspects of Coq: we also give the users
 the possibility to define their own. Differently from related tools
 (\cite{mucheck,mutAnalCoq,MutProlog}), our guiding principle insists
 that operators are not just grammatical rules that injects faults,
 but must preserve types, so that any resultant mutant will type-check
 by construction; thanks to Coq's extremely strong type system, this
 also helps in reducing the combinatorial explosion intrinsic in
 operators application.



What \mutc does not (yet) do is the full automation of the cycle of
mutant generation, testing and scoring, in the sense that tools for imperative
programming languages support (\cite{Jia:2011}). This is problematic,
if one stays within the realm of a pure language such as (Meta)Coq.
% which is difficult because Coq is pure, MetaCoq does not permit
% arbitrary effects and even if it did, it would be computationally
% hard.

We acknowledge that this short paper may be hard to read and therefore
we have provided  Appendix~\ref{sec:appA} and~\ref{sec:dsl} with
additional information. We assume some familiarity with Coq and the
basics of PBT  %. We are going to present only some of \qc's features that are relevant to our project
and refer to to the relevant chapter
in \emph{Software Foundations}~\url{https://softwarefoundations.cis.upenn.edu/qc-current/} for \qc
and to~\cite{andrea} for testing the case studies we mention. Further,
we will use MetaCoq as a black box and will describe only some of the
features of \mutc without diving at all into its implementation. The tech
report~\cite{matteo} and the repo
({\url{https://bitbucket.org/matteo_cavada/tesi/src/master/mutantChick/})
  give full details. The file \lsti{demo.v} therein offers a gentle
  introduction to \mutc's main features.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

%  LocalWords:  Compcert SMTCoq QC's MetaCoq QuickCheck cex IFC repo
%  LocalWords:  PLT
