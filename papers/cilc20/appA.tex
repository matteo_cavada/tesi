
\section{Listings of the Typed Arithmetic Language}
\label{sec:appA}

In this section we list the code we have selectively quoted in Section~\ref{sec:ex}. For the sake of testing the dynamic semantics is encoded functionally (so that it is trivially a decidable \lsti{bool}ean property).
\begin{lstlisting}
Inductive tm : Type := | ttrue : tm  | tfalse : tm | tif : tm -> tm -> tm -> tm
 | tzero : tm | tsucc : tm -> tm   | tpred : tm -> tm   | tiszero : tm -> tm.

Inductive typ : Type := | TBool : typ  |TNat : typ.
\end{lstlisting}
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}
Fixpoint isnumericval (t:tm) : bool :=
 match t with
  | tzero => true
  | tsucc t1 => isnumericval t1
  | _ => false
 end.
\end{lstlisting}
\end{minipage}\hfill
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}
Fixpoint isval (t:tm) : bool :=
 match t with
  | ttrue  => true
  | tfalse => true
  | t => isnumericval t
  end.
\end{lstlisting}
\end{minipage}\hfill
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}
Fixpoint stepF (t:tm) : option tm  :=
 match t with
  | tif ttrue t2 t3 => ret t2
  | tif tfalse t2 t3 => ret t3
  | tif t1 t2 t3 => 
     t1' <- stepF t1 ;;
     ret (tif t1' t2 t3)
  | tsucc t1 =>    
     t1' <- stepF t1 ;;
     ret (tsucc(t1'))
  | tpred tzero => ret  tzero
  | tpred (tsucc nv1) =>
     if (isnumericval nv1) then ret nv1 else
     t1' <- stepF nv1 ;;
     ret (tpred(tsucc t1'))
  | tpred t1 =>    
     t1' <- stepF t1 ;;
     ret (tpred t1')
  | tiszero tzero => ret ttrue
  | tiszero(tsucc nv1) =>
     if (isnumericval nv1) then ret tfalse else
     t1' <- stepF nv1 ;;
     ret (tiszero(tsucc t1'))
  | tiszero t1  => 
     t1' <- stepF t1 ;;
     ret (tiszero t1')
  |_ =>  None
 end.
\end{lstlisting}
  \end{minipage}\hfill
 \begin{minipage}{.60\textwidth}
\begin{lstlisting}
Definition canStep (e:tm) : bool :=
 match stepF e with
 | Some _ => true
 | None => false
 end.
\end{lstlisting}
\end{minipage}\hfill
Next, we list the code of a generator for well-typed terms of size $n$, which
heavily relies on \qc's generators combinators:
 \begin{lstlisting}
Fixpoint gen_term_size (n:nat) (t:typ) : G tm :=
 match n with
  | 0 =>
   match t with
   |TNat => returnGen tzero
   |TBool => oneOf [returnGen ttrue; returnGen tfalse]
   end
  | S n' =>
    m <- choose (0, n');;
    match t with
    | TNat =>
     oneOf [returnGen tzero;
         liftGen tsucc (gen_term_size (n'-m) TNat) ;
         liftGen tpred (gen_term_size (n'-m) TNat) ;
         liftGen3 tif (gen_term_size (n'-m) TBool)
                (gen_term_size (n'-m) TNat)
                (gen_term_size (n'-m) TNat)]
    | TBool =>
     oneOf [returnGen ttrue; returnGen tfalse;
        liftGen tiszero (gen_term_size (n'-m) TNat);
        liftGen3 tif (gen_term_size (n'-m) TBool)
                (gen_term_size (n'-m) TBool)
                (gen_term_size (n'-m) TBool)]
    end
  end.
\end{lstlisting}
%\end{minipage}


To give a rough idea on how properties are encoded,
we report two \emph{Checkable} definitions of progress, the first using the custom generator \lsti{gen_term}, the second based on
automatic derivation of generators for types
\lsti{forAll arbitrary (fun tau : typ =>$\dots$)} and of well typed terms out of the
\lsti{Inductive} definition, namely \lsti{ forAll (genST (fun t => has_type t tau))}.
For a full explanation, please refer to~\cite{andrea}.

\noindent\begin{minipage}[t]{.45\columnwidth}
\begin{lstlisting}
Definition progressGen :=
 forAll arbitrary (
     fun tau =>
      forAll (gen_term tau)
       (fun t =>
       isval t || canStep t)).
\end{lstlisting}
\end{minipage}\hfill 
\begin{minipage}[t]{.45\columnwidth}
\strut\vspace*{-\baselineskip}\newline
\begin{lstlisting}
Definition progressST :=
 forAll arbitrary (fun tau =>
 forAll (genST (fun t => has_type t tau))
     (fun mt =>
      match mt with
      | Some t => (isval t || canStep t)
      | None => false 
      end)).
\end{lstlisting}
\end{minipage}


\section{\mutc's DSL}
\label{sec:dsl}
A BNF grammar of the DSL and of the operators is as follows:
\begin{grammar}\label{grammar:mutate}

<root> ::= `Mutate' <MetaTerm> `using' <op> `named' <string> \{<random>\} 
   \alt `MultipleMutate' <MetaTerm> `using' <opList> `named' <string> \{<random>\}

<random> ::= `generating' <randomConfig>

<randomConfig> ::= `AllMutations' \alt `RandomMutations' <nat> <nat>

<string>  ::= A Coq string

<checkFunction> ::= A Coq function of type \lsti{term -> bool}

<transFunction> ::= A Coq function of type \lsti{term -> term}

<coqExpr>  ::= Any Coq expression

<MetaTerm> ::= `<\%' <coqExpr> `\%>'

<op> ::= <coqExpr> `==>' <coqExpr>
	\alt `Swap' (<string> | <metaTerm>) (<string> | <metaTerm>)
	\alt `IfThenElse'
	\alt `DelImplications'
	\alt `NewConstructor' <string> <metaTerm>
	\alt `SubConstructor' <string> <string> <metaTerm>
	\alt `OnConstructor' <string> <op>
	\alt `OnConstructors' <op>
	\alt `UserDefined' <checkFunction> <transFunction>

\end{grammar}
 \lsti{Mutate} takes four arguments: the expression to mutate, the operator, the base-name for the newly generated definitions and an optional arguments indicating whether to use random selection or not. \lsti{MultipleMutate} is identical to \lsti{Mutate}, but it accepts a \emph{list} of operators. 
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

% LocalWords: bool typ genST MetaTerm MultipleMutate opList nat ean
% LocalWords: randomConfig AllMutations RandomMutations coqExpr BNF
% LocalWords: checkFunction transFunction metaTerm IfThenElse forAll
% LocalWords: DelImplications NewConstructor SubConstructor
% LocalWords: OnConstructor OnConstructors UserDefined minipage
% LocalWords: generatore Checkable
