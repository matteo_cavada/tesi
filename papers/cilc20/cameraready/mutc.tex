\documentclass{llncs}

\usepackage[subtle]{savetrees}
\usepackage{xspace,url}
\usepackage{paralist}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{proof}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{syntax}
\usepackage[utf8x]{inputenc}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{proof}
\usepackage{listings}


\input{lstcoq.sty}
\lstset{language=coq}
\lstset{basicstyle=\footnotesize,
		breaklines=true}

              %
%             \input{macros}
              \newcommand{\acheck}{{\ensuremath \alpha}{Check}\xspace}
              \newcommand{\qc}{\textsf{QuickChick}\xspace}
                            \newcommand{\mutc}{\textsf{MutantChick}\xspace}
\usepackage{hyperref}
% keep this
\setlength{\textwidth}{13cm}



\begin{document}
\title{\mutc: Type-Preserving Mutation Analysis for Coq\thanks{Copyright @ 2020 for this paper by its authors. Use permitted under Creative Commons License Attribution 4.0 International (CC BY 4.0). Partially supported by GNCS project ``METALLIC \#2: METodi di prova per il ragionamento Automatico per Logiche non-cLassIChe''. }}
\author{Matteo Cavada, Andrea Col\`{o} and Alberto Momigliano}
\institute{DI, Universit\`{a} di Milano}
\maketitle{}
\begin{abstract}
  We present \mutc, a mutation analysis tool for Coq to be used in
  combination with \qc to evaluate the fault detection capability of
  property-based testing in a proof assistant.  Mutation analysis of
  Coq theories is implemented via metaprogramming with
  \textsf{MetaCoq} and it is by construction type-preserving.
  % Formal verification of software via (interactive) theorem proving
  % gives the highest level of assurance, but it is still very
  % labor-intensive. Testing, in the form of counter-examples
  % generation, can be useful as a preliminary check especially in the
  % design phase of a software system where mistakes and changes are
  % likely. As in traditional software engineering we can stop testing
  % (and here start proving) only when testing is ``good enough''. A
  % way to gauge the effectiveness of a testing suite is mutation
  % analysis, where the code is seeded with intentional faults and the
  % suite is evaluated w.r.t.\ the ability to catch the planted bugs. In this
  % paper we present MutantChick, a mutation testing tool for Coq, to
  % help evaluate %the quality of the infrastructure required by
  % QuickChick, Coq's own property-based testing system. Mutation
  % testing of Coq theories is implemented via metaprogramming with
  % MetaCoq. We present a DSL to specify and compute type-preserving
  % mutation operators and we apply it to a specific domain of interest:
  % the mechanized semantics of programming languages, in particular to
  % Appel \& Leroy's list-machine benchmark.
\end{abstract}
% \keywords{Software validation, property-based testing, mutation analysis, proof assistants, meta-programming, compiler verification}
\section{Introduction}
\label{sec:intro}
%\input intro
Formal verification of software via (interactive) theorem proving
gives the highest level of assurance, but it is still very
labor-intensive. This effort may be furthermore wasted in the design
phase of a software system, where mistakes and changes are likely to
occur both at the level of the specification and of the
implementation.  Testing/validation/model-checking can ameliorate the
situation by finding faults in a mostly ``push-button'' way so that to
refine the specification/code until theorem proving can start.
%
 Even so, it poses the question: when do we stop testing and start proving? This is
 analogous to asking  when to stop testing in the traditional software
 cycle and release the software itself. While taking into the right
 consideration Dijkstra's admonishments, in practical terms it much
 depends on how good our testing suite is.

 In this paper we fix our validation technique to be
 \emph{property-based testing}~\cite{fink97sen} (PBT), which aims to
 {refute} executable specifications against automatically generated
 data, typically in a pseudo-random way. Once the idea broke out in
 the Haskell community with
 \textsf{QuickCheck}~\cite{quickcheckfunprofit}, it spread to most
 programming languages and turned also in a commercial enterprise.

 We can view PBT as a testing suite that consists of a set of
 properties plus a \emph{data generation strategy}: while PBT tools
 vary in those strategies and in the amount of automation they offer, when
 dealing with random generation uniform distributions are rarely the
 most effective choice; the art of PBT resides in writing very
 sophisticated generators, see~\cite{HritcuLSADHPV16} for an
 intimidating example. Hence, how do we assess the fault detection
 capability of such a testing suite? If the latter stops finding new
 counterexamples, are we willing to consider the system-under-test ready
 for verification?

 Mutation analysis~\cite{Jia:2011} (MA) aims to evaluate software
 testing techniques with a form of white box testing, whereby a source
 program is changed in a localized way by introducing a single
 (syntactic) \emph{fault}. The resulting program is called a
 ``mutant'' and hopefully it is also semantically different from its
 ancestor. A testing suite should recognize the faulted code, which is
 known as ``killing'' the mutant. The higher the number of killed
 mutants, the better the testing suite. While typically used in
 combination with unit testing, MA fits fairly well with PBT (as
 proposed in~\cite{mucheck}) and, in fact, in the literature PBT tools
 are evaluated with manually inserted faults   (e.g., the PLT-redex
 benchmark suite, see
 \url{http://docs.racket-lang.org/redex/benchmark.html}).


 On the verification side, Coq (\url{https://coq.inria.fr}) is one of
 the leading proof assistants, which has shown its color both in
 formalizing general mathematics (see the proof of the Odd Order
 Theorem~\url{https://github.com/math-comp/odd-order}) and in specific
 domains such as the one dearest to our heart, namely the semantics of
 programming languages (\cite{Leroy09,pltredex,FeltyMP18,AbelAHPMSS19}).

 Coq is an extremely powerful logic, which accommodates programming,
 specification and proving in a constructive setting.  In accordance
 with the Peter Parker principle ``with great power comes great
 responsibility'', PBT in such an environment is no small feat: \qc, while under active
 developments (\cite{GG,Lampropoulos0P19}), brings in additional challenges as we touch upon in the next section. 

%  Here,
% just look at the listmachine gens: we can test them, conceivably we
% could prove them sound and even in a sense complete~\cite{paraskevopoulou2015foundational}, but
% they have a lot of heuristics builtin --- again, why on earth be 
% in the proving business for testing in the design phase --- and M.A.\
% is a way to evaluate them

 
% \smallskip

 This is where \mutc comes in: \mutc is a mutation analysis tool for
 Coq theories, whose main application is evaluating the ``oracle
 adequacy'' of PBT with \qc.  We accomplish this in a purely logical
 way via meta-programming with \textsf{MetaCoq}~\cite{sozeau2020metacoq}. Since
 any MA tool is as good as the \emph{mutation operators} it
 implements, we provide a DSL to specify and compute
 \emph{type-preserving} mutation operators, supporting both the
 functional and the relational aspects of Coq: we also give the users
 the possibility to define their own. Differently from related tools
 (\cite{mucheck,mutAnalCoq}), our guiding principle insists
 that operators are not just grammatical rules that injects faults,
 but must preserve types, so that any resultant mutant will type-check
 by construction; thanks to Coq's extremely strong type system, this
 also helps to reduce the combinatorial explosion intrinsic in
 operators application.



What \mutc does not (yet) achieve is the full automation of the cycle of
mutant generation, testing and scoring, as tools for imperative
programming languages do (\cite{Jia:2011}). This is hard to accomplish,
if one stays within the realm of a pure language such as (Meta)Coq.
% which is difficult because Coq is pure, MetaCoq does not permit
% arbitrary effects and even if it did, it would be computationally
% hard.

\smallskip
%We acknowledge that this short paper may be hard to read and therefore
We assume some familiarity with Coq and the basics of
PBT %. We are going to present only some of \qc's features that are relevant to our project
and refer to the relevant chapter in \emph{Software Foundations}
(\url{https://softwarefoundations.cis.upenn.edu/qc-current}) for \qc
and to~\cite{andrea} for testing the case studies we mention. Further,
we will use \textsf{MetaCoq} as a black box and will describe only
some of the features of \mutc without diving at all into its
implementation. The technical report~\cite{matteo} and the repository
{\url{https://bitbucket.org/matteo_cavada/tesi/src/master/mutantChick}
  give full details.
   % The file \lsti{demo.v} therein offers a gentle
  % introduction to \mutc's main features. 
  Under the latter, one can also find the electronic appendix to the present paper
  with additional topical information.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

%  LocalWords:  Compcert SMTCoq QC's MetaCoq QuickCheck cex IFC repo
%  LocalWords:  PLT


\section{Running Example}
\label{sec:ex}
%\input qc
We are mainly interested in the mechanization of the meta-theory of
programming languages (PL) and related calculi. As any practitioner
may testify, the relevant properties are well known and have
mathematically shallow proofs. The difficulty lies in the potential
magnitude of the cases one must consider and in the trickiness that
some encodings require. Here, very minor mistakes, even at the level
of typos, may severely frustrate the verification effort, to the point
to make it not cost-effective.

As a running example, we consider a very simple model of a PL, 
a typed arithmetic language (\url{https://softwarefoundations.cis.upenn.edu/plf-current/Types.html}), featuring numerals with predecessor and Booleans with if-then-else and test-for-zero. For the sake of this section, we
concentrate on a rule-based presentation (akin to logic programs) of the typing rules, but
our approach covers the functional rendition as well.
%
As a property, we consider the \emph{progress} lemma, which ensures that if a
term is well typed, then either it is a value or it can take a step; 
 % is a sort of completeness property for the static and dynamic
% semantics of a PL and
 it is one of the building blocks in Milner's
 motto for which a well-typed program cannot go wrong.

 The typing
rules are encoded as an \lsti{Inductive} definition \lsti{has_type} to be read ``term \lsti{t} has type \lsti{T}''
(see the electronic appendix for the other definitions):

\begin{lstlisting}
Inductive has_type: tm -> typ -> Prop :=
   T_Tru: has_type ttrue TBool
 | T_Fls: has_type tfalse TBool
 | T_Test: forall t1 t2 t3 T, has_type t1 TBool -> has_type t2 T -> has_type t3 T -> has_type (tif t1 t2 t3) T
 | T_Zro: has_type tzero TNat
 | T_Scc: forall t1, has_type t1 TNat -> has_type (tsucc t1) TNat
 | T_Prd: forall t1 has_type t1 TNat -> has_type (tpred t1) TNat
 | T_Iszro: forall t1, has_type t1 TNat -> has_type (tiszero t1) TBool.

Theorem progress: forall t T, has_type t T -> value t \/ exists t', step t t'.
\end{lstlisting}

We now introduce some
mutations % (using \qc's facilities that turns a comment into a bug at at testing time)
that should mirror typical mistakes while designing a PL artifact ---
see for example the manual mutations in the cited PLT-Redex benchmark suite. A first
mutation\footnote{Each mutation yields a new \lsti{Inductive} definition named by convention \lsti{has_type_Mn}.} represents a simple typo where we have swapped
\lsti{TBool} for \lsti{TNat} in the test-for-zero rule:
\begin{lstlisting}
$\dots$ | T_Iszro_M : forall t1 : tm, has_type_M1 t1 TBool -> has_type_M1 (tiszero t1) TNat.
\end{lstlisting}
The second one 
corresponds to having forgotten an hypothesis in the rule for
if-then-else, namely that the test \lsti{t1} is a Boolean:
\begin{lstlisting}
$\dots$ | T_Test_M: forall t1 t2 t3 T, 
         has_type_M2 t2 T -> has_type_M2 t3 T -> has_type_M2 (tif t1 t2 t3) T
\end{lstlisting}
%
Finally, suppose instead we \emph{add} an additional rule to the given
ones (this is exercise $2$ from \lsti{Types.html}):
\begin{lstlisting}
$\dots$ | T_Scc_M: forall t, has_type_M3 t TBool -> has_type_M3 (tsucc t) TBool.
\end{lstlisting}

All mutants do \emph{not} satisfy progress: it would stand to reason that a
PBT tool should find the relevant counterexamples, as say
$\alpha$Check~\cite{Pessina15} does  quite easily and without any complicated setup.  However, this is not immediately the case with
 \qc. %  is just a clone of Haskell's QuickCheck is an
% understatement:
Coq is, under the Curry-Howard view, a version of
constructive higher-order logic, enriched with (co)inductive types and
universe polymorphism, where  only   pure total functional
programs are permitted, while allowing highly undecidable specifications. This
duality is reflected in the type \textsf{Set} of computations and
\textsf{Prop} of arbitrary propositions. If we wish to test a
conjecture, it must be shown effectively computable, in \qc's terminology
  a \textsf{Checkable} property. There are two ways to go
about it: first, if the notion under test is defined relationally,
that is at \textsf{Prop}, we can manually provide a proof that it is
indeed decidable. In certain simple cases such as the \lsti{has_type}
relation, we can rely on the rather unpredictable tool for automatic
derivations of generators out of \lsti{Inductive} definitions~\cite{GG}. % This may be
% non-trivial, hard to factorize and therefore more importantly, it can
% be wasted effort in the design phase of the SUT.
The second and
fail-safe way is to translate every specification into a Boolean-valued
function: since in Coq every such function must be total, decidability
and therefore Checkability is  guaranteed. This approach
has its drawbacks, since it needs to explicitly address the
partiality of rule-based specifications and to accommodate
Coq's rather temperamental totality checker, or we may even have to provide our own termination argument. 
% However, this depends on how the relevant judgments are formalized:
% if functionally, generators for well-typed terms must be
% implemented; if relationally,
Even stating the \lsti{progress} property so that it can be tested is
not immediate, neither with automatic derivation
% (\lsti{Definition progressST}), nor with a custom-made generator (\lsti{Definition progressGen})
,  see~\cite{andrea}.


All these layers point to the utility of a MA tool to
assess the ``oracle adequacy'' of \qc. In the next section we will see how
\mutc can derive those mutations. Note that in our example properties
(viz.\ progress) are \emph{trusted}.
% (if not their implementation).
This is generally the case for PL semantics, since they come from the
theorems that should hold for the underlying calculus; however, a
large number of surviving mutants may  suggest a possible
under-specification in such properties or a fault in its testable
implementation. Finally, since properties are just Coq terms, 
our
tool could mutate them as well, although we do not see this as particularly useful.
% certainly conceivable to conjecture properties that may be ``almost''
% right or such that their executable versions may be buggy.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

%  LocalWords:  QuickChick QuickCheck SUT Checkability TBool TNat PLT
%  LocalWords:  MMT html Checkable progressST progressGen

% In our implementation, a simple driver produces a given number of
% randomly generated mutants from an \aprolog source $P$ and a list
% \emph{LM} of mutation operators. At every generation step we make a
% random choice of operator in \emph{LM} and of clause in $P$ such that
% the operator is applicable, in the spirit
% of~\cite{ZhangGMK13}. If the mutant is new, i.e., it has not been
% produced before, it is passed to the next phase: here, we
% try to weed out semantically equivalent mutants following the proposal
% in~\cite{Offutt} of comparing them with their ancestor on a finite
% portion of input. We achieve this by asking \acheck to try to refute
% their equivalence up to a given bound. Keeping the bound small makes
% this a fairly quick, if imperfect, filter. If the mutant survives, we
% pass it  again to \acheck, this time under the property that the
% original model was supposed to satisfy. 

\section{MutantChick}
\label{sec:mchick}

%\input mchick


%\paragraph{MetaCoq}
Our tool is based on \textsf{MetaCoq}~\cite{sozeau2020metacoq}, a library that provides rich meta-programming features for Coq. Among those, we will use
%
\begin{inparaenum}[1)]
\item the (anti)quotation mechanism: quoting a Coq term \lsti{t}, 
  denoted by \lsti{<% t %>},
    induces a bijection between Coq kernel terms % (e.g., the \lsti{has_type} inductive definition of the previous Section)
    and a deep
    embedding in Coq itself of the syntax of the  terms of the underlying Calculus
    of (Co)Inductive Constructions; the embedding is realized as an \lsti{Inductive}
    definition of the type \lsti{term}; % --- in other terms \lsti{<% t : term %>});
    \item a reification of the static and dynamic semantics of the
      representation of the calculus, providing a hook for checking
      well-typedness of quoted terms.
\end{inparaenum}

For example, quoting the lambda expression
\lsti{fun x => 2 + (hd x [9;8;7])} that sums $2$ to the head of a given list returns the
following abstract syntax tree (AST), which we have significantly abridged by using  numbers to stand for their quotation --- keep in mind that in Coq
naturals and lists are not built-in, but obtained as inductive types.
\begin{lstlisting}
(tLambda (nNamed "x") 
   (tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} [])
   (tApp (tConst "Coq.Init.Nat.add" [])
      [2;tApp (tConst "Coq.Lists.List.hd" [])
         [tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} []; 
          tRel 0;
          tApp (tConstruct {| inductive_mind := "Coq.Init.Datatypes.list"; inductive_ind := 0 |} 1 [])
            [tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} [];
             tApp (tConstruct {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} 1 []) [9;8;7]]]]))
\end{lstlisting}

  
To access and modify Coq's terms and environment, \textsf{MetaCoq} provides a
monad (\lsti{TemplateMonad}) in which it is possible  to describe quoting and unquoting  of Coq terms, as
well as other (few) actions with side-effects, such as raising an exception
and inserting new terms inside Coq's environment; these monadic
meta-programs, once invoked with the \lsti{Run TemplateProgram}
command, will be executed by an interpreter inside the OCaml environment.

%\paragraph{\mutc}
\mutc aims to devise \emph{mutation operators} for a proof assistant,
here Coq (but they would also be relevant for other systems such as \emph{Lean} or \emph{Isabelle/HOL}), which encompasses both functional and relational
specifications, with a particular attention to our intended domain of application.
% namely models of programming languages artifacts.
Those operators
specify the mutations that the tool will inject, trying to simulate
natural occurring bugs.  This is justified by the ``competent
programmer assumption''~\cite{Jia:2011}, according to which the programmer
tends to develop code ``close'' to the correct version and thus the
delta for each fault is small.


An MA tool is as effective as its operators are
relevant. Historically, MA comes from imperative languages and the
operators thereby still owe to those initially devised for
FORTRAN\@. Even operators proposed for declarative
programs (\cite{mucheck}) make only partial sense, being
untyped and too linked to the operational semantics of the programming
language. Usable examples  comprise arithmetic and relational operator mutations,
say turn $\times$ into $+$ and $<$ into $\leq$ and constant mutations, say
$0$ into $1$.


\begin{table}[t]  
 \begin{center}
   \begin{tabular}{l|l|r} \label{tab:tableOperators}
    \textbf{Kind} & \textbf{Operator} & \textbf{Description}\\
    \hline
	\multirow{2}{*}{General} & Substitute & Substitutes a term with another\\ 
			& Swap & Exchanges two terms\\ 
    		    & IfThenElse & Exchanges \emph{then} and \emph{else} branch\\ 
    		    & DelImplications & Deletes assumptions\\ 
    		    & UserDefined & User defined operator\\
    \hline
    \multirow{2}{*}{Inductive} & NewConstructor & Adds new constructor\\ 
    	        & SubConstructor & Substitutes a constructor\\ 
            & OnConstructor & Maps an operator over a constructor\\ 
    	        & OnConstructors & Maps an  operator over an inductive def.\\
      \hline
    \end{tabular}
  \end{center}
  \caption{List of \mutc's operators.}
%\vspace{-0.8cm}
\end{table}

Table~\ref{tab:tableOperators} describes at a high level the
operators currently implemented in \mutc. We have two
(non-disjoint) main categories: \emph{general} operators, working on
any Coq term, and \emph{inductive} ones, which are particularly suited to relationally defined judgments.

The general syntax to mutate a terms is as follow (see the electronic appendix for the BNF of the DSL):
\begin{lstlisting}
Run TemplateProgram (
    Mutate <% term_to_mutate %>
           using (operator)
           named "new_name").
\end{lstlisting}
\begin{sloppypar}
Let us see a classic arithmetic mutation: 
\verb+t1 ==> t2+ substitutes every
occurrence of  \lsti{t1} with \lsti{t2}.  This operator is
internally defined as an inductive definition
\mbox{\lsti|Substitute {A} (t1 t2: A)|}, which is indexed by  the type \lsti{A} of the operands in
order to ensure that the substitution is
type-preserving. For example:
\end{sloppypar}

% In the example that follows, we use \lsti{Substitute} to change the occurrence of multiplication with addition inside a simple arithmetic expression; note that both functions have the same type, i.e.. \lsti{nat -> nat -> nat}.
% ;

\vspace{5mm}
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram ( 
  Mutate <% 4 * 5 %>
         using (Init.Nat.mul $==$> Init.Nat.add)
         named "out").         
Print out.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[]{Name}
out = 4 + 5
     : nat
\end{lstlisting}
\end{minipage}

\noindent
All the generic operators work both on plain Coq terms and on
\mbox{\lsti{Definition,Fixpoint}} etc.

Since a quoted term is a (rather large) AST, 
a {mutation operator} is implemented by two components:
%
\begin{inparaenum}[1)]
%
\item a \emph{search phase}, in which we find \emph{all} locations
  inside the tree of the  to-be-mutated term; this is accomplished by 
  {a function \lsti{check :: term -> bool} that establishes if a
    given quoted term is suitable for mutation;}
%	
\item  a \emph{mutation phase}, in which a transformation function  \lsti{trans :: term -> term} is applied to the terms in the locations previously identified.
\end{inparaenum}

An important optimization in MA is \emph{random generation} of
mutants~\cite{Jia:2011}: since a given mutation can be applied multiple
times in the same term, it is reasonable to select only a
sample thereof. Again, a pure language such as Coq cannot rely on an
external pseudo-random number generator, hence we had to deploy our
own  as a \emph{Linear-Feedback Shift Register} on $7$
bits% with feedback polynomial $x^7 + x^6 + 1$
; this generates
pseudo-random sequences of numbers starting from a seed (at this stage
provided by the user).

The DSL offers three choices:
\begin{inparaenum}[1)]
	\item \emph{mutate everywhere};
%
	\item \emph{pick mutations with probability $p$}, i.e., generate a random number for
          every mutable location in the AST\@:
          if it is bigger then a certain threshold (chosen by the
          user), then collect the mutant;
	%
	\item \emph{pick a maximum of $n$ mutations}, i.e.,  maximum of $n$ locations are drawn among all the coordinates found in the {search} phase.
\end{inparaenum}

Rather than seeing each operator in details (for which we
refer once again to~\cite{matteo}), we detail how \mutc realizes the
mutations of the previous section.
%
%
% \begin{enumerate}
% \item 
%\paragraph{Swap}
%\label{par:swap} 

For the typo, we use bidirectional substitution \lsti{Swap t1 t2}; it exchanges the
order of appearance of  \lsti{t1} and \lsti{t2} inside an
expression and is defined in terms of \verb+Substitute+. Note that
\lsti{t1} and \lsti{t2} can be (bound) variables, which mirrors a large category of errors.
%[caption=An example of Swap,frame=tlrb]{Name}
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% forall t1, has_type t1 TNat -> has_type (tiszero t1) TBool. %>
         using (Swap <% TBool %>  <% TNat %>)
         named "T_Iszro_M").
\end{lstlisting}
We could have achieved this and the next mutation with the operator
\lsti{SubConstructor} although with less automation.

%\paragraph{DelImplications} \label{par:delimplications}
\lsti{DelImplications} mirrors missing assumptions by non-deterministically  removing implications
(technically, dependent products); in the following case it
will yield 6 different non-isomorphic mutants. 
%
%\vspace{5mm}
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% forall t1 t2 t3 T, has_type t1 TBool ->
                      has_type t2 T -> has_type t3 T -> has_type (tif t1 t2 t3) T %>
         using (DelImplications)
         named "T_Test_M").
\end{lstlisting}
Interestingly, we can achieve this more directly by using the
\lsti{OnConstructors} combinator that maps the operator over the
whole relation; since this could generate several mutants, we use random choice to select a few. 	
\begin{lstlisting}[frame=tlrb]{Name}
Run TemplateProgram (
  Run TemplateProgram (
        Mutate <% has_type %>
               using (OnConstructors DelImplications)
               named "has_type_M"
               generating (RandomMutations 34 64)).
\end{lstlisting}

Finally, we can add a new constructor to an \lsti{Inductive} definition via
%\paragraph{Add a new constructor}
 \lsti{NewConstructor} % (newNameCons: ident) (type: term)}: % the name of the constructor will be \lsti{newNameCons} and its type will be specified as a MetaCoq term.
%%[caption=Adding a constructor,frame=tlrb,label=addConstr]{Name}
\begin{lstlisting}[frame=tlrb,label=addConstr]{Name}
Run TemplateProgram (
 Mutate <% has_type %>
        using (NewConstructor "T_Scc_M"
              <% fun hastype_M1 => forall t, has_type_M1 t TBool -> 
                 has_type_M1 (tsucc t) TBool %>)
         named "hastype_M3").
\end{lstlisting}





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

%  LocalWords:  MetaCoq bijection CIC NewConstructor newNameCons hd
%  LocalWords:  ident monad TemplateProgram OCaml AST Peano BNF bool
%  LocalWords:  muCheck reification Fixpoint IfThenElse UserDefined
%  LocalWords:  DelImplications SubConstructor OnConstructor
%  LocalWords:  OnConstructors

%   \section{Mutating and testing the list-machine benchmark}
% \label{sec:lm}
% \input lm
\section{Conclusions, related and future work}
\label{sec:concl}

% \input conc
In this short paper we have sketched the rationale and the high level
design of \mutc, a mutation analysis tool for the Coq proof assistant.
We have advocated the usefulness of such a tool, given the popularity
that testing, in particular PBT, is having in the Coq world in terms
of applications (\cite{HritcuLSADHPV16}) and development
(\cite{GG,Lampropoulos0P19}). Still, mutation \emph{testing}, by which we
mean the \emph{improvement} of a testing suite via MA, has been lagging
behind, being only informally and manually used to evaluate PBT\@.
%
The exception is \verb+mCoq+~\cite{mutAnalCoq}, which follows a very
different philosophy from ours: it exports the AST of a Coq term as an
\emph{S-expression} thanks
to % \verb+SerAPI+ \cite{GallegoArias2016SerAPI},
an OCaml serializer and then applies mutations to the S-expression
using a tool written in Java.  Following this choice, it cannot
enforce that mutation operators are type-preserving, nor can they be
easily extended by the user. In fact, the hardwired operators are
quite limited, being mostly concerned with functional programming. We
also do not share \verb+mCoq+'s motivation, which is not mutation
testing but mutation \emph{proving}, whereas mutations are introduced
in definitions to see if the proof scripts of related theorems still
hold: if they do, it means that those definitions are
under-specified. It seems to us a very big hammer for such a small
nail.

We have evaluated the tool on two case studies (\cite{andrea,matteo}):
the arithmetic language and much more significantly, Leroy \& Appel's
\emph{list-machine} benchmark~\cite{appel2006list}. In both, we have
realized the mutations described in the literature~\cite{KomauliM18},
implemented the relevant generators and tested various forms of type
soundness with \qc. The list-machine case study is particularly
meaningful, as the properties under test have extremely sparse
precondition: this forces us to implement complex generators with
intricated heuristics built-in, whose fault detection capability would
be hard to gauge without a MA tool.

The main limitation of \mutc is performance: mutation analysis is
computationally expensive and (Meta)Coq is definitely not an efficient
general-purpose programming language, neither w.r.t.\ memory
consumption nor the boilerplate required to generate and test mutants
in a completely automated way. We have managed to use operators such as \verb+==>+ to
percolate mutated terms within all the definitions that depend on them
in a Coq theory, but to address the whole mutation cycle fully, we
would need, among others,  to  read and write files, which is out of Coq's
remit.

To address the performance issue, a  possibility is the \emph{extraction} to OCaml of the 
\textsf{MetaCoq} code responsible for the heaviest computations: this is
possible but not trivial (see Sec.~5 of~\cite{sozeau2020metacoq}). A
more radical solution would be to port \mutc to \emph{Elpi}~\cite{ELPI}, the
$\lambda$Prolog interpreter embedded inside Coq, and exploit
full-range logic programming.



% Testing Smart Contracts Gets Smarter
% Erfan Andesta, Fathiyeh Faghih and Mahdi Fooladgar 

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

%  LocalWords:  mCoq AST SerAPI sexp Elpi Appel's OCaml MetaCoq


\bibliographystyle{abbrv}
\bibliography{b}
%\appendix
% \input appA
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%  LocalWords:  PBT Ornaghi QuickCheck Ok Aho Ulmann's b's NAF WA's
%  LocalWords:  ss aa bb Asm pragma pred  TODO Milano lm TAPL MuCheck
%  LocalWords:  mutator di OCAML Universit ablist SLD finitary AE asm
%  LocalWords:  Matteo Cavada QuickChick qc MutantChick mchick conc
%  LocalWords:  Appel MetaCoq appA GNCS METodi prova il ragionamento
% LocalWords:  Automatico Logiche cLassIChe
