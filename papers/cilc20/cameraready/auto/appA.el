(TeX-add-style-hook
 "appA"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8x")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "lstcoq"
    "llncs"
    "llncs10"
    "xspace"
    "url"
    "paralist"
    "amssymb"
    "wrapfig"
    "multirow"
    "proof"
    "amsmath"
    "amsfonts"
    "syntax"
    "chngcntr"
    "inputenc"
    "enumerate"
    "graphicx"
    "listings"
    "hyperref")
   (TeX-add-symbols
    "acheck"
    "qc"
    "mutc")
   (LaTeX-add-labels
    "sec:appA"
    "sec:dsl"
    "grammar:mutate")
   (LaTeX-add-bibliographies
    "b"))
 :latex)

