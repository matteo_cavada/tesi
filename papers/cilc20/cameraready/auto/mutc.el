(TeX-add-style-hook
 "mutc"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("savetrees" "subtle") ("inputenc" "utf8x")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "lstcoq"
    "llncs"
    "llncs10"
    "savetrees"
    "xspace"
    "url"
    "paralist"
    "amssymb"
    "multirow"
    "proof"
    "amsmath"
    "amsfonts"
    "syntax"
    "inputenc"
    "enumerate"
    "graphicx"
    "listings"
    "hyperref")
   (TeX-add-symbols
    "acheck"
    "qc")
   (LaTeX-add-labels
    "sec:intro"
    "sec:ex"
    "sec:mchick"
    "tab:tableOperators"
    "addConstr"
    "sec:concl")
   (LaTeX-add-bibliographies
    "b"))
 :latex)

