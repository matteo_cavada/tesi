\documentclass{beamer}

% \usepackage[utf8]{inputenc}
\usepackage{todo}
\usepackage{listings}
\usepackage{multirow} % Required for tables
\usepackage{tabularx, ragged2e}
\usepackage{syntax}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{proof}
\usepackage{adjustbox}
\usepackage{xcolor}
\usepackage{cancel}

\usepackage{bussproofs}
\usepackage{graphicx}
\graphicspath{ {./} }
\graphicspath{ {./ast/} }



\title{Type-Preserving Mutation Analysis for Coq}
\author{\fbox{Matteo Cavada}, Andrea Colò and Alberto Momigliano}
\institute{Università degli Studi di Milano}
\date{October 2020}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\input{lstcoq.sty}
\lstset{language=coq}
\lstset{basicstyle=\footnotesize,
		breaklines=true}

\frame{\titlepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Motivations}

\begin{itemize}

\item{Formal verification via (interactive) theorem proving gives the
    highest assurance of correctness, but is a time-consuming and labor-intensive activity.}

\item{Validation/testing is usually ``cheaper'', but more unreliable.}

\item{What is the point of testing in proof assistants?
    Why perform testing in a language when you can prove correctness instead?}
  
\item{The main problem with validation: when do we stop testing and start proving? In practice, the answer depends on how good our test suites are.}

\item{We will show how Mutation Analysis and Property-Based Testing (PBT) can cooperate to perform validation on a given domain: the semantics of programming languages.}
  
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \section{Coq}


\section{Mutation Analysis}

\begin{frame}
  \frametitle{Mutation Analysis --- a high-level overview}
  \begin{itemize}
    \item{Mutation Analysis is a (meta)testing technique, employed also in mainstream languages.}
    \item{How can we be sure that a given test suite $T$ is actually a \textbf{good} test suite?}
    \item{Mutation Analysis rates $T$ by injecting faults in the program $P$, resulting in a number of \textbf{mutants} induced by a given \textbf{mutant operator}}
    \item{When a bug is found in a mutant, we say that the mutant was \textbf{killed}; otherwise, we say that that mutant is \textbf{alive}.}
    \item The ratio between alive and killed mutants is an indication of the quality of the test suite.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Mutation Analysis and Proof Assistants}
  \begin{itemize}
    \item{MA as a tool to rate how good your PBT suite is}
    \item{PBT (think \textbf{QuickCheck}) is a testing technique with
        which we attempt to falsify a computable property by means of
        randomly-generated data test.}
    \item{The core idea is: given a model of a language or abstract machine, we inject faults and, through PBT, we attempt to falsify a given property.} 
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{MutantChick}

\begin{frame}
  \frametitle{MutantChick: an overview}
  \begin{itemize}
  \item{We present a mutation library, \textsf{MutantChick}, for the proof assistant Coq using MetaCoq, a metaprogramming library.}
  \item{MutantChick, provides a set of predefined mutant operators and a DSL to define custom ones.}
  \item{Default mutant operators are guaranteed to be type-safe; no \textbf{stillborn} mutants.}  
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
  \frametitle{MutantChick: a simple example}
    \begin{lstlisting}[frame=single]
    Run TemplateProgram (
      Mutate <% 4 * 5 %>
      using (Init.Nat.mul => Init.Nat.add)
      named "mul_to_add"
    ).
      
    Print mul_to_add.
  \end{lstlisting}

  Result: \lsti{mul_to_add = 4 + 5 : nat}
\end{frame}


\begin{frame}[fragile]
  \frametitle{MutantChick: a simple example}
  \begin{itemize}
  \item{Original expression: \lsti{4*5}} \pause
  \item{Reificated expression: \lsti{<% 4*5 %>}} \pause
  \item{(simplified) AST representation:
      \begin{columns}
        \begin{column}{0.7\textwidth}
          \begin{lstlisting}[]
tApp (tConst "Init.Nat.mul")
  [4; 5]
        \end{lstlisting}\end{column}
        \begin{column}{0.3\textwidth}
          \includegraphics[width=0.8\textwidth]{graph}
        \end{column}\end{columns}}

    \pause
    
  \item{(simplified) AST representation after mutation \lsti{mul => add}:
      \begin{columns}
        \begin{column}{0.7\textwidth}
          \begin{lstlisting}[]
tApp (tConst "Init.Nat.add")
  [4; 5]
          \end{lstlisting}\end{column}
        \begin{column}{0.3\textwidth}
          \includegraphics[width=0.8\textwidth]{graph2}
        \end{column}\end{columns}} \pause
   \item{Result: \lsti{4 + 5}}
 \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
  \frametitle{MutantChick's mutation operators}
\begin{table}[h!]  
  \begin{center}
  \begin{adjustbox}{width=1\textwidth}
    \small
   \begin{tabular}{l|l|r} \label{tab:tableOperators}
    \textbf{Kind} & \textbf{Operator} & \textbf{Description}\\
    \hline
	\multirow{2}{*}{General} & Substitute & Substitutes a term\\ 
			& Swap & Exchanges two appearing terms\\ 
    		    & IfThenElse & Exchanges then and else\\ 
    		    & DelImplications & Deletes logical implications\\ 
    		    & UserDefined & User defined operator\\
    \hline
    \multirow{2}{*}{Inductive} & NewConstructor & Adds new constructor\\ 
    	        & SubConstructor & Substitutes a constructor\\ 
            & OnConstructor & Applies a general operator to a constructor\\ 
    	        & OnConstructors & Applies a general operator to a whole inductive definition\\ 
      \hline
    \end{tabular}
  \end{adjustbox}
  \end{center}
\end{table}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{MA for a model of a programming language}
  \begin{itemize}
  \item{We consider a toy arithmetic  language taken from Pierce et al.'s \textbf{Software Foundations} book.}
  \item We will insert some mutations and we will see if our PBT suite is ``good enough''
  \item{We will use QuickChick, the \textbf{de facto} standard PBT library for Coq, with
    both derived and custom generators (not detailed here)}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% NOTA: farò a breve modifiche estetiche per le seguenti 2 slides
\begin{frame}[fragile]
  \frametitle{Typing rules for an arithmetic language}
\[
  \begin{array}{c}
    \infer[\scriptsize{\texttt{T\_Tru}}]{\vdash tru \in TBool}{}
    \qquad
    \infer[\scriptsize{\texttt{T\_Fls}}]{\vdash fls \in TBool}{}
    \qquad
    \infer[\scriptsize{\texttt{T\_Zro}}]{\vdash zro \in TNat}{}
    \medskip \\[0.2cm]
    \infer[\scriptsize{\texttt{T\_Scc}}]{\vdash \mbox{scc t1} \in TNat }{\vdash t1 \in TNat}
    \qquad  
    \infer[\scriptsize{\texttt{T\_Test}}]
      {\vdash \mbox{if t1 then t2 else t3} \in T}
      {\vdash t1 \in TBool & t2 \in T & t3 \in T} \qquad
    \medskip \\[0.2cm]

  \only<1>{
    \color{gray}\infer[\scriptsize{\texttt{T\_isZro}}]
      {\vdash \mbox{isZro t1} \in TBool}
      {\vdash t1 \in TNat}
    }
    
    \only<2>{
      \color{gray}\xcancel{
        \infer[\scriptsize{\texttt{T\_isZro}}]
          {\vdash \mbox{isZro t1} \in TBool}{\vdash t1 \in TNat}
      }
      \medskip \\[0.2cm]
      \color{red}
      \infer[\scriptsize{\texttt{T\_isZroMUT}}]
        {\vdash \mbox{isZro t1} \in TNat}{\vdash t1 \in TBool}}
    
  \end{array}
\]

% \only<1>{
\color{gray}
\begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{lstlisting}
Inductive has_type : tm -> typ -> Prop := (...)
 | T_Iszro : forall t1,
    has_type t1 TNat ->
    has_type (tiszero t1) TBool.
  \end{lstlisting}
  \end{column}
  \pause

  \color{red}
  \begin{column}{0.4\textwidth}
   \begin{lstlisting}
Inductive has_type : tm -> typ -> Prop := (...)
 | T_Iszro : forall t1, 
    has_type t1 TBool -> 
    has_type (tiszero t1) TNat.
  \end{lstlisting}
  \end{column}
  \end{columns}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
  \frametitle{Arithmetic Expressions: mutation process}
  \begin{itemize}
    \item{Let's build a mutant version of \lsti{has_type}, the Coq implementation of the aforementioned typing rules:}
  \end{itemize}

  \begin{lstlisting}
Run TemplateProgram (
    Mutate <% has_type %>
           using (OnConstructors (Swap <% TNat %> <% TBool %>))
           named "has_type_mut" ).
   \end{lstlisting}
         
  \begin{itemize}
  \item{Here we use the operator \lsti{Swap}, which exchange the order of appearance of two terms (in this case, \lsti{TNat} and \lsti{TBool}) in a given expression}
    \item{\lsti{OnConstructors}  applies an operator \lsti{op} to \textbf{all the constructors} of a given inductive type}
    
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
  \frametitle{Arithmetic Expressions: PBT validation}
  \begin{itemize}
  \item{Will QuickChick spot the injected fault in our language model?}
  \item{Will the \textbf{progress} property still holds?}
    \item{Progress means: if we have a well-typed term $t$, then either $t$ is a value or there is a $t'$ to which $t$ can be reduced.} 
   \begin{lstlisting}
     QuickChecking progress...
     
      TNat
      tpred (tpred (tiszero tru))
      *** Failed after 26 tests and 0 shrinks. (0 discards)

   \end{lstlisting}
  \item Mutant is killed, so our PBT suite survives to live another day.
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{Conclusions}
  \begin{itemize}
  \item{Validation is more and more used prior to formal verification to reduce the effort of trying to prove false theorems}

  \item{Mutation Analysis (MutantChick) and Property-Based Testing (QuickChick) can cooperate to perform validation of a given model of a language}
    
  \item{We successfully applied this MA+PBT approach also on more complex
      models, such as Appel \&  Leroy's \textbf{ListMachine} benchmark}

  \item{We plan in the future to optimize the mutation process with MutantChick, both on a computational level (speed-up computations) and on an ergonomic level (less boilerplate needed)}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%

\begin{frame}
  \frametitle{The end}
  Thanks for watching!
\end{frame}

\end{document}
% LocalWords:  QuickCheck MutantChick AST
