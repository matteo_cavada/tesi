Our main interest is verification in a particular domain: the
mechanization of the meta-theory of programming languages (PL) and
related calculi (\cite{pltredex}). As any practitioner may testify,
the relevant properties are well known and have mathematically shallow
proofs. The difficulty lies in the potential magnitude of the cases
one must consider and in the trickiness that some encodings
require. Here, very minor mistakes, even at the level of what we would
consider a typo, may severely frustrate the verification effort, to
the point to make it not cost-effective.

As a running example, we take a very simple model of a PL, 
a typed arithmetic language (\url{https://softwarefoundations.cis.upenn.edu/plf-current/Types.html}), featuring numerals with predecessor and Booleans with if-then-else and test-for-zero. For the sake of this Section, we
concentrate on a rule-based presentation (akin to logic programs) of the typing rules, but
our approach covers the functional rendition as well.
%
As a property, we consider the \emph{progress} lemma, which ensures that if a
term is well typed, then either it is a value or it can take a step; 
 % is a sort of completeness property for the static and dynamic
% semantics of a PL and
 it is one of the building blocks in Milner's
 motto, for which a well-typed program cannot go wrong.

 The typing
rules are encoded as an \lsti{Inductive} definition \lsti{has_type} to be read ``term \lsti{t} has type \lsti{T}''
(see Appendix~\ref{sec:appA} for the other definitions):

\begin{lstlisting}
Inductive has_type: tm -> typ -> Prop :=
   T_Tru: has_type ttrue TBool
 | T_Fls: has_type tfalse TBool
 | T_Test: forall t1 t2 t3 T, has_type t1 TBool -> has_type t2 T -> has_type t3 T -> has_type (tif t1 t2 t3) T
 | T_Zro: has_type tzero TNat
 | T_Scc: forall t1, has_type t1 TNat -> has_type (tsucc t1) TNat
 | T_Prd: forall t1 has_type t1 TNat -> has_type (tpred t1) TNat
 | T_Iszro: forall t1, has_type t1 TNat -> has_type (tiszero t1) TBool.

Theorem progress: forall t T, has_type t T -> value t \/ exists t', step t t'.
\end{lstlisting}

We now introduce some
mutations % (using \qc's facilities that turns a comment into a bug at at testing time)
that should mirror typical mistakes while designing a PL artifacts ---
see for example the manual mutations in the cited PLT-Redex benchmark suite. A first
mutation~\footnote{Each mutation yields a new \lsti{Inductive} definition named by convention \lsti{has_type_Mn}.} represents a simple typo where we have swapped
\lsti{TBool} for \lsti{TNat} in the test-for-zero rule:
\begin{lstlisting}
$\dots$ | T_Iszro_M : forall t1 : tm, has_type_M1 t1 TBool -> has_type_M1 (tiszero t1) TNat.
\end{lstlisting}
The second one 
corresponds to having forgotten an hypothesis in the rule for
if-then-else, namely that the test \lsti{t1} is a Boolean:
\begin{lstlisting}
$\dots$ | T_Test_M: forall t1 t2 t3 T, has_type_M2 t2 T -> has_type_M2 t3 T -> has_type_M2 (tif t1 t2 t3) T
\end{lstlisting}
%
Finally, suppose instead we \emph{add} an additional rule to the given
ones (this is exercise $2$ from \lsti{Types.html}):
\begin{lstlisting}
$\dots$ | T_SccB: forall t, has_type_M3 t TBool -> has_type_M3 (tsucc t) TBool.
\end{lstlisting}

All mutants do \emph{not} satisfy progress: it would stand to reason that a
PBT tool should find the relevant counterexamples, as say
$\alpha$Check does  quite easily and without any complicated setup~\cite{alphacheck}.  However, this is not immediately the case with
 \qc. %  is just a clone of Haskell's QuickCheck is an
% understatement:
Coq is, under the Curry-Howard view, a version of
constructive higher-order logic, enriched with (co)inductive types and
universe polymorphism, where  only   pure total functional
programs are permitted, while allowing highly undecidable specifications. This
duality is reflected in the type \textsf{Set} of computations and
\textsf{Prop} of arbitrary propositions. If we wish to test a
conjecture, it must be shown effectively computable, in \qc's terms
 it is a \textsf{Checkable} property. There are two ways to go
about it: first, if the notion under test is defined relationally,
that is at \textsf{Prop}, we can manually provide a proof that it is
indeed decidable. In certain simple cases such as the \lsti{has_type}
relation, we can rely on the rather unpredictable tool for automatic
derivations of generators out of \lsti{Inductive} definitions~\cite{GG}. % This may be
% non-trivial, hard to factorize and therefore more importantly, it can
% be wasted effort in the design phase of the SUT.
The second and
fail-safe way is to translate every spec into a Boolean-valued
function: since in Coq every such function must be total, decidability
and therefore Checkability is  guaranteed. This approach
has its drawbacks, since it needs to explicitly address the
partiality of rule-based specifications and to accommodate
Coq's rather temperamental totality checker. Still, 
% However, this depends on how the relevant judgments are formalized: if functionally, generators for well-typed terms must be implemented; if relationally, 
even stating the \lsti{progress} property so that it can be tested is
not immediate, neither with automatic derivation  (\lsti{Definition progressST}), nor with a custom-made generator (\lsti{Definition progressGen}),  see  Appendix~\ref{sec:appA}
and~\cite{andrea}.


All these layers point to the utility of a MA tool to
assess the ``oracle adequacy'' of \qc. In the next Section we will see how
\mutc can derive those mutations. Note that in our example properties
(viz.\ progress) are \emph{trusted}.
% (if not their implementation).
This is generally the case for PL semantics, since they come from the
theorems that should hold for the underlying calculus; however, a
large number of surviving mutants may also suggest a possible
under-specification in such properties or a fault in its testable
implementations. Finally, since properties are just Coq terms, 
our
tool could mutate them as well, although we do not see this as particularly useful.
% certainly conceivable to conjecture properties that may be ``almost''
% right or such that their executable versions may be buggy.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mutc"
%%% End:

%  LocalWords:  QuickChick QuickCheck SUT Checkability TBool TNat PLT
%  LocalWords:  MMT html Checkable progressST progressGen
