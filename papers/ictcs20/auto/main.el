(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "llncs"
    "llncs10"
    "cdsty"
    "listings"
    "wrapfig"
    "xcolor"
    "paralist"
    "stmaryrd"
    "xspace"
    "url"
    "hyperref"
    "cite"
    "amsfonts"
    "amssymb"
    "threeparttable"
    "breakurl"
    "babel"
    "proof")
   (TeX-add-symbols
    '("blu" 1)
    '("initExpert" 2)
    '("someExpert" 3)
    '("andExpertLJF" 6)
    '("andExpert" 3)
    '("unfoldExpert" 2)
    '("red" 1)
    '("ab" 1)
    '("abs" 2)
    '("instan" 1)
    "andd"
    "impp"
    "Pscr"
    "vds"
    "til"
    "gen"
    "unitTy"
    "conc"
    "unit"
    "step"
    "steps"
    "NF"
    "trueExpert"
    "eqExpert"
    "orExpert"
    "lP"
    "true"
    "XXi"
    "ok"
    "noc")
   (LaTeX-add-labels
    "sec:intro"
    "sec:ps"
    "fig:ce")
   (LaTeX-add-bibliographies
    "bib"))
 :latex)

