func subst(exp,id,exp) = exp.

subst(u,_,_) = u.

subst(var(Y),Y,N) = N.
subst(var(Y),X,N) = var(Y) :- X # Y.
subst(app(M1,M2),X,N) = app(subst(M1,X,N), subst(M2,X,N)).
subst(lam(M),X,N) = lam(M') :- new y. subst(M@y,X,N)  = M'@y.

pred substp(exp,id,exp,exp).
substp(M,X,N,M') :- subst(M,X,N) = M'.

pred tc (ctx,exp,ty).
tc(G,u,unitTy).
tc([(X,T)|G],var X,T).
tc([(_,_)| G],var X,T) :- tc(G,var(X),T).
tc(G,app(M,N),U) :- tc(G,M,funTy(T,U)), tc(G,N,U).
tc(G,lam(M),funTy(T,U)) :- new x:id. tc([(x,T)|G],M@x,U).


pred value(exp).

value(lam(_)).
value(u).


pred step(exp,exp).
step(V,V) :- value(V).
step(app(M,N),P) :- step(M,lam(MM)), step(N,NN), new a.  substp(MM@a,a,NN,PP), step(PP,P).
step(app(M1,M2),app(M1',M2)) :- step(M1,M1').
step(app(M1,M2),app(M1,M2')) :- value M1, step(M2,M2').




pred valid_ctx(ctx).
valid_ctx([]).
valid_ctx([(X,T)|G]) :- X # G, valid_ctx(G).

