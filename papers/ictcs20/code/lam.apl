exp : type.
id  : name_type.
ty  : type.
u   : exp.
var : id -> exp.
app : (exp,exp) -> exp.
lam : id\exp -> exp.
funTy  : (ty,ty) -> ty.
unitTy : ty.

type ctx = [(id,ty)].


