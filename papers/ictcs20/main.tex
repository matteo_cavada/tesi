\documentclass{llncs} 

%\usepackage{todo}
%\usepackage{proof}
\usepackage{cdsty}
\usepackage{listings}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{paralist}
%\usepackage{columns}
\usepackage{stmaryrd}
\usepackage{xspace,url,hyperref}
\usepackage{cite,xspace}
\usepackage{amsfonts,amssymb}
%\usepackage{graphicx,proof,pifont} 
\usepackage{threeparttable,url,breakurl}
%\usepackage{booktabs,wrapfig}
%\usepackage[active]{srcltx}
\usepackage{listings}
\usepackage[english]{babel} 
%\usepackage{todonotes}
%\usepackage{mathptmx}
\usepackage{proof}
\newcommand{\andd}{\wedge}
\newcommand{\impp}{\supset}

\newcommand{\instan}[1]{\hbox{\sl grnd\/}~(#1)}
\newcommand{\Pscr}{{\mathcal P}}

\newcommand{\vds}{\vdash}
\renewcommand{\hastype}{\mathrel{:}}

\newcommand{\til}{\symbol{126}}
\newcommand{\gen}{{gen}}
\newcommand{\unitTy}{\mathrm{nat}}
\newcommand{\abs}[2]{{\ab{#1}{#2}}}
\newcommand{\conc}{\mathop{@}}
\newcommand{\unit}{n}
\newcommand{\ab}[1]{\langle #1 \rangle}
\newcommand{\step}{\Downarrow}
\newcommand{\steps}{\leadsto^*}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\NF}{\emph{NAF}\xspace}
\newcommand{\trueExpert }[1]{{\true_e}(#1)}
\newcommand{\eqExpert }[1]{{=_e}(#1)}
\newcommand{\unfoldExpert}[2]{{\hbox{\sl unfold\/}_e}(#1,#2)}
\newcommand{\andExpert}[3]{{\wedge_e}(#1,#2,#3)}
\newcommand{\andExpertLJF}[6]{{\wedge_e}(#1,#2,#3,#4,#5,#6)}
\newcommand{\orExpert  }[3]{{\vee_e}(#1,#2,#3)}
\newcommand{\someExpert}[3]{\exists_e(#1,#2,#3)}
\newcommand{\initExpert}[2]{\hbox{\sl init}_e(#1,#2)}
\newcommand{\lP}{$\lambda$Prolog\xspace}
\newcommand{\true}{tt}
\newcommand{\XXi}{{\color{blue}{\Xi}}}
\newcommand{\blu}[1]{{\color{blue}{#1}}}
\newcommand{\ok}{\checkmark}
% DM The use of pifont and txfonts broke typesetting for me: equality
% signs never appearred and some parentheses did not appear either.
%\newcommand{\noc}{\ding{56}}
\newcommand{\noc}{\ddag}

%\usepackage[subtle]{savetrees}

\setlength{\textwidth}{12.7cm}
\setlength{\textheight}{20.3cm}



\begin{document}
\title{Why Proof-Theory Matters in \\Specification-Based Testing}
\author{Alberto Momigliano}
\institute{Dipartimento di Informatica,
  Universit\`{a} degli Studi di Milano\\
  \email{momigliano@di.unimi.it}}
\maketitle
\begin{abstract}
  We survey some recent and ongoing developments in giving a logical
  reconstruction of specification-based testing via the lenses of structural proof-theory. 
 \end{abstract}

\section{Introduction}
\label{sec:intro}

Formal verification of software properties is still a labor intensive
endeavor, notwithstanding recent advances: automation
plays only a partial role and the engineer is heavily involved not
only in the specification stage, but in the proving one as well,
even with the help  of a proof assistant.
%
This effort is arguably misplaced in the \emph{design} phase of a
software artifact, when mistakes are inevitable and even in the best
scenario the specification and its implementation may change. A
\emph{failed} proof attempt is hardly the best way to debug either.

These remarks are, of course, not novel, as they lie at the basis of 
model checking and other counter-examples generation techniques, where
the emphasis is on \emph{automatically refuting}, rather than proving,
that some code respects its spec.
% 
Contrary to Dijkstra’s diktat, testing, and more in general
validation, has found an increasing niche in formal verification,
prior or even in alternative to theorem
proving~\cite{BlanchetteBN11,QChick}. % The beauty of this synergy is
% that the user does not have to invent the specs: they are exactly the
% theorems the formal system is supposed to satisfy.

The message of this brief summary is that, somewhat surprisingly,
structural proof-theory~\cite{negri} offers a unifying approach to the field.  While the ideas
that I am going to sum up here may have a wider applicability, I am
going to narrow it to:

\begin{itemize}
\item \emph{Specification-based testing} (SBT, also known as
  \emph{property}-based testing~\cite{quickcheckfunprofit}),  a lightweight validation
  technique whereby the user specifies executable properties that the
  code should satisfy and the system tries to refute them via
  automatic (typically random) data generation.
\item One specific domain of interest: the mechanization of the
  semantics of programming languages and related artifacts, where
  proofs tend to be shallow, but may have hundreds of cases and are
  therefore a good candidate to SBT.
\end{itemize}



\section{SBT as Proof-Search}
\label{sec:ps}
We use as a running examples a call-by-value $\lambda$-calculus (where
values are lambdas and numerals) whose static and big-step dynamic
semantics follows --- readers should substitute it with a more
substantial specification of a programming language whose meta-theory
they wish to investigate:
\begin{small}
  \[
  \begin{array}{c}
    \infer[\texttt{T-N}]
    {\vds \unit \hastype \unitTy}
    {}
    \qquad
    \infer[\texttt{T-VR}]
    {\Gamma \vds x \hastype A}
    {x \hastype A \in \Gamma}
    \qquad
    \infer[\texttt{T-AB}]
    {\Gamma \vds \lambda x.\, M \hastype A \rightarrow B}
    {x \not\in \mathrm{dom}(\Gamma) & \Gamma,x\hastype A \vds M \hastype B}
                        \smallskip\\
                                     \infer[\texttt{T-AP}]
                                     {\Gamma \vds M_1\cdot M_2 \hastype B}
    {\Gamma \vds M_1 \hastype A \rightarrow B & \Gamma \vds M_2 \hastype B}\medskip\\
        \infer[\texttt{E-V}]{V \step V}{\mathrm{value\ } V}
  \qquad
    \infer[\texttt{E-AP}]{M_1\cdot M_2 \step V}{M_1 \step \lambda x.\, M\quad M_2\step V_2\quad  M\{V_2/x\}\step V}
      % \infer[\texttt{E-AB}]{(\lambda x\hastype{A}.~M) \cdot V \step  M\{V/x\}}{}
  % \quad
  %   \infer[\texttt{E-AP}]{M_1~M_2 \step M_1'~M_2}{M_1 \step M_1'}
  %   \quad
  %       \infer[\texttt{E-AP}]{V_1~M_2 \step V_1~M_2'}{M_2 \step M_2'}
  \end{array}
  \]
\end{small}

Consider now the type preservation property for closed terms:
\[\forall M\, M'\, A.~M \step M' \longrightarrow M \hastype A \longrightarrow M'
  \hastype A \]
In fact, the result does \emph{not} hold for this calculus, since we
have managed to slip  a typo in one of the rules. % Refuting the
% property amounts to serach for a proof of
% $\exists M\ M'\ A, M \step M' \land M \hastype A \land \neg ( M'
% \hastype A )$.
One counter-example is
$M= (\lambda x.\, x \cdot \unit) \cdot \unit, M' = \unit \cdot \unit, A
= \unitTy$. How to go from it to the origin of the bug is another
research topic in itself~\cite{MomiglianoO19}; suffices to say that it points to a
mistake in rule \texttt{T-AP}, namely the type in the minor premise
should be $A$. A tool that automatically provides such a
counter-example would save us from wasting time on a potentially very long failed proof
attempt.

While this issue can and has been successfully tackled in a functional programming
setting~\cite{Klein12short
  %,FachiniM17
}, at least two factors make a proof-theoretic reconstruction fruitful:
\begin{inparaenum}[1)]
\item it fits nicely with a (co)inductive reading of rule-based
  presentations of our system-under-test
\item it easily generalizes to logics that intrinsically handle issues
  that are pervasive in the domain of programming languages semantics,
  such as naming and scoping.
\end{inparaenum}
In fact, as argued in~\cite{Blanco0M19}, the SBT literature is
rediscovering (constraint) logic programming ideas such as narrowing,
mode checking, random back-chaining etc.

 If we view  a specification (property) as a logical formula
\(\forall x [(\tau(x)\wedge P(x)) \supset Q(x)]\) where $\tau$ is a
typing predicate and $P$ and $Q$ are two other predicates defined
using Horn clause specifications (to begin with), providing a counter-example
consists of  negating the property, and searching for a proof of 
\(\exists x [(\tau(x)\land P(x)) \land \neg Q(x)]\).

Stated in this way the problem points to a logic programming solution,
and since the seminal work of Miller et al.~\cite{miller91apal},
{structural proof-theory} formulates it as a proof-search problem
in the sequent calculus, where the specification is a fixed set of
assumptions (typically sets of clauses) and the negated property is the goal.

A first solution that I was involved with is
$\alpha$Check~\cite{cheney_momigliano_2017}, which supplements a logic
programming interpreter~\cite{cheney08toplas} to account for
counter-example search. The tool uses
\begin{inparaenum}[1)]
\item nominal logic %~\cite{Pitts06}
  as a logical foundation, which is
  particularly apt at encoding binding structures,
\item automatically derived type-driven \emph{exhaustive} generators for data enumeration,
\item two approaches to implementing negation: negation as failure and
  negation elimination~\cite{Pessina16},
\item a fixed search strategy, namely iterative-deepening based on
  the height of partial proof trees.
\end{inparaenum}

\section{SBT via FPC}


While $\alpha$Check turned out to be quite effective (see the case
studies at~\url{https://github.com/aprolog-lang/checker-examples}), the
approach was unnecessarily rigid, in particular wiring-in a fixed data
generation and search strategy, and did not reflect the richness of
features that SBT offers.  However, being the proof-theory of
$\alpha$Check based on the notion of \emph{uniform
  proofs}~\cite{miller91apal}, it is easy to generalize it via the
subsuming theory of \emph{focused proof systems}~\cite{andreoli92jlc}.
We can roughly characterize focusing as a complete strategy to
organize the  rules of the sequent calculus into
 two phases: % of proof construction.
\begin{inparaenum}[1)]
\item a \emph{negative} 
%
  phase corresponding to goal-reduction, where we apply rules involving
  \emph{don't-care-non\-de\-ter\-min\-ism}; as a result, there is no
  need to consider backtracking, and
 %
\item a \emph{positive} phase corresponding to back-chaining
  (\emph{don't-know-non\-de\-ter\-min\-ism)}: here, inference rules
  need to be supplied with external information (e.g., which clause to
  back-chain on) in order to ensure that a completed proof can be
  built.
%
\end{inparaenum}
%
Thus, when building a proof tree  from the
conclusion to its leaves, the negative phase corresponds to a simple
deterministic computation, while the positive
phase may need to be guided by an oracle. 

The connection with SBT is that in a query the \emph{positive phase}
(which corresponds to the generation of possible counter-examples) is
represented by \(\exists x\) and \((\tau(x)\land P(x))\).
%
That is followed by the \emph{negative phase} (which corresponds
to counter-example testing) and is represented by \(\neg Q(x)\). This
formalizes the intuition that generation may be hard, while testing is
just computation.

The final ingredient is how to supply the external information to the
positive phase: this is where the theory of \emph{foundational proof
  certificates}~\cite{chihani17jar} (FPC) comes in.  In their fully
generality, FPCs can be seen as a generalization of proof-terms in the
Curry-Howard tradition, able to define a range of proof structures
used in various theorem provers (e.g., resolution refutations,
Herbrand disjuncts, tableaux, etc).  They can be programmed as
\emph{clerks and experts} predicates that decorate the sequent rules
used in an FPC proof checking kernel. An FPC system is a specification
of the certificate format together with the clerks and experts
processing it. In our setting, we can view
% 
FPCs as simple logic programs that guide the search for potential
counter-examples using different generation strategies.

\begin{figure}[t]
  \centering
  \begin{small}
\[
\infer{\XXi\vdash G_1\wedge G_2}
      {\XXi_1\vdash G_1\qquad \XXi_2\vdash G_2\qquad \andExpert{\XXi}{\XXi_1}{\XXi_2}}
\qquad
\infer{\XXi\vdash \true}
{\trueExpert{\XXi}}
\qquad
\infer{\XXi\vdash \exists x. G}
      {\XXi'\vdash G[t/x]\qquad \someExpert{\XXi}{\XXi'}{t}}
\]
% \vskip -6pt
% \[
% % \infer{\XXi\vdash G_1\vee G_2}
% %       {\XXi'\vdash G_i\qquad \orExpert{\XXi}{\XXi'}{i}}
% % \qquad
% \infer{\XXi\vdash \exists x. G}
%       {\XXi'\vdash G[t/x]\qquad \someExpert{\XXi}{\XXi'}{t}}
% \]
\vskip -6pt
\[
% \infer{\XXi\vdash t = t}
%       {\eqExpert{\XXi}}
% \qquad
\infer{\XXi\vdash A}
      {\XXi'\vdash G \quad (A~\hbox{\tt :-}~G)\in\instan\Pscr
        \quad \unfoldExpert{\XXi}{\XXi'}}
    \]
                     \vskip -8pt
          \dotfill

%                 \vskip -6pt
   \[
\infer{\blu{n}\vdash G_1\wedge G_2}
{\blu{n}\vdash G_1\qquad \blu{n}\vdash G_2 %\qquad \andExpert{\XXi}{\XXi_1}{\XXi_2}
}
\qquad
\infer{\blu{n}\vdash \true}
{
  % \trueExpert{\XXi}
}
% \]
% \vskip -6pt
% \[
% \infer{n,m\vdash G_1\vee G_2}
% {n,m\vdash G_i%\qquad \orExpert{\XXi}{\XXi'}{i}
% }
% \qquad
% \infer{n,m\vdash \exists x. G}
% {n,m\vdash G[t/x]%\qquad \someExpert{\XXi}{\XXi'}{t}
% }
% \]
% \vskip -6pt
% \[
% \infer{n,n\vdash t = t}
% {
%   % \eqExpert{\XXi}
% }
\qquad
\infer{\blu{n}\vdash \exists x. G}
 {\blu{n}\vdash G[t/x]%\qquad \someExpert{\XXi}{\XXi'}{t}
 }
\]
\[
\infer{\blu{n + 1,}\vdash A}
      {\blu{n}\vdash G \quad (A~\hbox{\tt :-}~G)\in\instan\Pscr
                     \quad n \geq 0}
\]

\end{small}

\caption{Clerks and expert presentation of the sequent calculus for Horn logic}
\label{fig:ce}
\end{figure}


Figure~\ref{fig:ce} contains a simple proof system for a fragment of
Horn clause provability in which each inference rule is augmented with
an additional premise involving an {expert predicate}, a
certificate $\Xi$, and possibly continuations of certificates ($\Xi'$,
$\Xi_1$, $\Xi_2$), if one reads the rules from conclusion to
premises. The logic programmers among us will recognize it as an
instrumented version of the vanilla meta-interpreter over a fixed Horn program $\Pscr$. For example, the
$\exists$-expert may be in charge of extracting from $\Xi$ the term
$t$ with which to instantiate $G$ so that we can build the rest of the
proof according to the resulting certificate $\Xi'$.
%
In the
bottom part of the figure, we instantiate the framework with the simplest form of
proof certificate, namely a positive integer, where the only active expert is a
    simple non-zero check while back-chaining: this
characterizes exhaustive generation bounded by \emph{height}, which
happens to be the generation strategy of
$\alpha$Check. As detailed in~\cite{Blanco0M19},
different FPCs capture random generation, via randomized
backtracking, as well as diverse
features such as $\delta$-debugging,  bug-provenance,
etc.

\section{To infinity and beyond}
Reasoning about infinite computations via coinduction and corecursion
has an ever-increasing relevance in formal methods and, in particular,
in the semantics of programming languages,
(see~\cite{2007-Leroy-Grall} for a compelling example) and, of course,
coinduction underlies (the meta-theory of) process calculi.  To our
knowledge, there are no SBT approaches for coinductive specifications, save
for the quite limited features provided by Isabelle/HOL's
\emph{Nitpick}~\cite{BlanchetteBN11}.

%
When addressing potentially infinite computations, where in our setup
we strive to model infinite \emph{behavior} (think divergence of a finite program) rather
than infinite \emph{objects} (i.e., infinite terms such as streams), we need to go
significantly beyond the simple proof-theory of~\cite{Blanco0M19} and adopt a
much stronger logic with explicit rules for induction and coinduction.

A natural choice is the fixed point linear logic
$\mu\mathrm{MALL}$~\cite{Baelde12}, which is associated to the
\emph{Bedwyr} model-checker~\cite{baelde07cade}.  In fact, this logic
has already shown its colors in the proof-theoretic reconstruction and
certification of model checking problems such as
(non)-reachability~\cite{HeathM15}. $\mu\mathrm{MALL}$ consists of a
sequent calculus presentation of \emph{multiplicative additive linear
  logic} with least and greatest fixed points operators in lieu of
exponentials, over a simply-typed term language.


Continuing with our running example, let us now consider a
\emph{coinductive} definition of CBV evaluation
following~\cite{2007-Leroy-Grall}. In other terms, we take the same
rules as in Section~\ref{sec:ps}, but we read them as the greatest fixed
point of the defined relation.
%
This is represented by the following formula (reminiscent of a
linearization of Clark's completion), where $\nu$ is the greatest fixed point
operator, $\Lambda$ is the abstractor in the meta-logic and \textit{val} stands for the $\mu$-formula characterizing values:
\[
\begin{small}
\begin{array}{lll}
   \mathrm{coeval}&\equiv& \nu (\Lambda {\mathit{CE}}.\Lambda m.\Lambda m'. (\exists V.~m = V\otimes m' = V \otimes \mathrm{val\ } V)  \oplus\mbox{}\\
  &&(\exists M_1 M_2 M V_2 V.\ m = M_1\cdot M_2  \otimes m' =  V \otimes
     ({\mathit{CE}} \ M_1 \ (\lambda x.\, M)) \otimes\mbox{}\\
  &&({\mathit{CE}\ } M_2\ V_2)
  \otimes ({\mathit{CE}} \ (M \{ V_2/x \}) \ V))
 %   \nu \mathrm{coeval}\lambda m\lambda m'. (\exists M\ V. m = (\lambda x.~M) \cdot V \otimes  m' =    M\{V/x\}) \oplus\mbox{}\\
 % \quad(\exists M_1 M_1'M_2.\ m = M_1 \cdot M_2  \otimes m' =  M_1' \cdot M_2\ \otimes  \mathrm{coeval} \ M_1'\ M_2) \oplus\mbox{}
%    \\
% \quad   (\exists V_1 M_2 M_2'.\ m = V_1 \cdot M_2 \ \otimes  m' =  V_1\cdot M_2'\
%    \otimes  \mathrm{coeval} \ V_1\ M_2')
 \end{array}
\end{small}
 \]
 $\otimes$ and $\oplus$ are multiplicative conjunction and
 additive disjunction and for the sake of space we do not make explicit
 the encoding of the object-level syntax and of substitution inside the meta-logic.
% concrete syntax, this is written as: 
% \begin{verbatim}
% Define coinductive coeval: tm -> tm -> prop by
%  coeval (con C) (con C);
%  coeval (fun R) (fun R);
%  coeval (app M N) V := 
%    exists R W, coeval M (fun R) /\ coeval N W /\ coeval (R W) V.
% \end{verbatim}
 
 Whether or not this notion of co-evaluation makes sense (see for a
 fair criticism~\cite{AnconaDZ17}), we would like to investigate
if standard properties such as type soundness or determinism of
 evaluation hold: they do not --- to refute the latter, just note that
 a divergent term such as $\Omega$ co-evaluates to anything. Type
 preservation, for a \emph{correct} version of the rules in
 Sec.~\ref{sec:ps}, is falsified by a variant of the $Y$-combinator.

 We have a prototype implementation of SBT for coinductive specs on top of
 Bedwyr, which we use both for the generation of test cases
 (controlled using specific FPCs~\cite{Blanco0M19}) and for the
 testing phase.
% example?
Such an implementation has the advantage of allowing us to piggyback
on Bedwyr's facilities for efficient proof-search via \emph{tabling} for
(co)inductive predicates, thus avoiding costly meta-interpretation.

To make it more concrete, let me report the query refuting determinism
of co-evaluation. We use Bedwyr's concrete syntax, where
\texttt{check}, implementing the kernel rules in the top of
Fig.~\ref{fig:ce}, is in charge of controlling the generation of
lambda terms (predicate \texttt{is\_exp} parameterized over a context
of bound variables), here in the exhaustive fashion detailed
\emph{ibidem}(generator \texttt{height 4}); \texttt{coeval} encodes
the coinductive CBV operational semantics using higher-order abstract
syntax and the last conjunct corresponds to ground disequality.
\begin{small}
\begin{verbatim}
?= check (height 4) ((is_exp void M) && (is_exp void M1) && (is_exp void M2)) 
   /\ coeval M M1 /\ coeval M M2 /\ (M1 = M2 -> false).

Found a solution (+ 4173ms):
 M2 = con one
 M1 = con zero
 M =  app (fun (x\ app x x)) (fun (x\ app x x))
\end{verbatim}
\end{small}
%
The system finds in reasonable time the expected counter-example, for
\texttt{M} being the encoding of $\Omega$. Note that we only generate
\emph{finite} terms, but the testing phase now appeals (twice) to the
coinductive hypothesis.

Other applications of SBT w.r.t.\ infinite behavior are in separating various notion of
equivalences in  lambda and process calculi: for example,
 applicative and ground similarity in PCFL~\cite{PITobtpe},
or analogous standard results in the $\pi$-calculus.

These examples put forward another challenge: the specification of a
coinductive notion such as applicative similarity goes beyond the Horn fragment, to wit:
 \[
\begin{array}{lll}
  \mathrm{asim} &\equiv& \nu(\Lambda \mathit{AS}.\Lambda m.\Lambda n.\forall M'.\ \mathrm{eval} \ m\ (\lambda x.\, M')
  \multimap
                         \exists N'.\ \mathrm{eval} \ m\ (\lambda x.\, N') \otimes\mbox{}\\
  && \qquad\forall R.\ ({\mathit{AS}\ } (M' \{R/x\}) \  (N'\{R/x\})))
\end{array}
\]
This makes the treatment of negation in the testing phase of a SBT
query problematic since the interpretation of finite failure as
provability of falsehood breaks down, at least in an intuitionistic
setting~\cite{Momigliano00}.  Here, the adoption of linear logic as a
meta-logic comes to the rescue, since in linear logic occurrences of
negations can be eliminated by using De Morgan duality and inequality.

\section{Conclusion}

I have tried to delineate a path where structural proof-theory
reconstructs, unifies and extends current trends in SBT, with a particular emphasis to
ongoing work on extending the paradigm to infinite computations. A
natural next step is \emph{concurrency}: logical framework such as
\emph{CLF} (and its implementation \emph{Celf}~\cite{Schack}) based
on sub-structural logics have been designed to encode concurrent
calculi, e.g., session types~\cite{PfenningG15}. However, the
meta-theory of such frameworks is still in the workings and this
precludes so far any reasoning about them. On the other hand, a FPC
approach to the validation of those properties seems a low hanging fruit.

\paragraph{Acknowledgment} A shout-out to my co-authors in this line of
work: Rob Blanco, James Cheney, Francesco Komauli, Dale Miller and Matteo Pessina.


% \begin{table}[h!]


%\newpage
\bibliographystyle{abbrv}
\bibliography{bib}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:


%  LocalWords:  Universit Studi Milano Informatica impredicative di
%  LocalWords:  bisimilarity Hur's Dipartimento degli SBT Francesco
%  LocalWords:  bisimilarity bisimilar precongruence impredicativity
%  LocalWords:  PCFL nat bool llcl DeBruijn Untypable precongruence
%  LocalWords:  STmExp TODO lcase CompRel Exps ExpRelations CuS OaSim



%  LocalWords:  Komauli Matteo Pessina colp FP Blanco VR corecursion
%  LocalWords:  coinductive CBV Bedwyr reachability linearization SBT
%  LocalWords:  dom Herbrand disjuncts intuitionistic Celf eval
