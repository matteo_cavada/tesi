# Tesi - PBT and Mutation Analysis in Coq

## Matteo Cavada, 893942

+ `mutantChick`: libreria per mutazione scritta in Coq, attualmente in uso
+ `tesi`: file testuali per la tesi; comprende file in Latex e indice


### Link significativi

+ [metaCoq (github)](https://github.com/MetaCoq/metacoq)
+ [quickchick (github)](https://github.com/QuickChick)
+ [Software Foundations 4 - quickchick](https://softwarefoundations.cis.upenn.edu/qc-current/toc.html)
