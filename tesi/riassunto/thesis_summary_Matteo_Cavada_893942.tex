\documentclass[12pt,a4paper]{article}
\usepackage{tocvsec2}
\setcounter{tocdepth}{3}
\usepackage{geometry}

%\geometry{letterpaper,margin=1in}

\newcommand{\summary}[3]{%
  \section*{%
    \parbox{\textwidth}{%
      \centering
      #1\\[1.5ex]% the title and the separation
      \normalfont\large
      #2\\[1.5ex]% 
      \large Relatore: Dr Alberto Momigliano
      #3\\[2ex]% the author and the separation
      \large  Termine tirocinio 03/06/2020
    }%
  }%
  %\addcontentsline{toc}{chapter}{Summary}
}

\usepackage{listings}
% \usepackage{url}
% \usepackage[utf8]{inputenc}

 \usepackage{natbib}

% \usepackage{xcolor}

\usepackage{fullpage}

%\usepackage{hyperref}
\usepackage{xspace}

\usepackage{amssymb,amsmath,amsthm}
\usepackage{thesis}

\title{Property-Based Testing and Mutation Analysis in Coq}
\author{Matteo Cavada}
\dept{Corso di Laurea Triennale in Informatica}
\anno{2019-2020}
\matricola{893942}
\relatore{Dr.\ Alberto \sc{Momigliano}}



 \input{lstcoq.sty}
 \lstset{language=coq}
 \lstset{basicstyle=\footnotesize}

\lstdefinelanguage{none}{
  identifierstyle=,
  keywordstyle=
}

% \usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage[pdf13]{pdfx}
% \usepackage[a-1a]{pdfx}


\begin{document}


\summary{Property-Based Testing and Mutation Analysis in Coq}
        {MATTEO CAVADA – 893942}
        {}


\section{Introduction}
Many testing techniques can be found within the realms of Software Engineering; hence, a question naturally arises: is there a way to ``grade'' all these techniques? Can we, in a way, test the tests themselves?

The answer is, in fact, yes. Among these meta-testing techniques we find Mutation Analysis (MA) \cite{mutAnalSurvey}, which is built upon a conceptually simple but robust idea: suppose we have a program $P$ and a testing suite $T$; we can then deliberately inject faults inside $P$, resulting in a \emph{mutant program} $P'$ which is, with high probability, incorrect. If the testing suite $T$ correctly spots the fault, then the mutant is said to be \emph{killed}; otherwise, the mutants remains \emph{alive}. The higher the number of killed mutants, the better the testing suite.

Perhaps counter-intuitively, Mutation Analysis has a place even in the land of Proof Assistants (and specifically in Coq, the main focus of this thesis). This may be surprising, as proof assistants use mathematical proofs to demonstrate that a given program is correct -- what is the point, one could wonder, of doing testing on mathematical proofs?

A major difficulty with proof assistants is that, in general, it may take quite some time to prove a theorem. Sometimes, we are not even sure if a given theorem is wrong or not. It is not difficult to imagine a scenario where we spend hours and hours of tedious work only to discover that we are attempting to prove the wrong thing! This is certainly an undesirable situation, and here lies the benefits of doing testing in proof assistants: it is a way of quickly and roughly search for counterexamples prior to attempting a proof.

Property-Based Testing (PBT)~\cite{claessen2011quickcheck}, in particular, is a well-known testing
technique based on the idea of specifying executable \emph{properties}
 that some code must always satisfy; these properties are
then tested against a possibly big number of automatically generated test
cases, typically in a pseudo-random way. If a counterexample is found for a certain property, 
we know that there is  mismatch between the code and the specification.

Coq's PBT library is called
QuickChick~\cite{paraskevopoulou2015foundational}. While QuickChick
contains most of the main features of his programming language
cousins, in practice it is harder to use: it is in fact almost always
mandatory (for all non-trivial datatypes) to manually write
\emph{random generators}, and it is often necessary to explicitly
write decidability proofs for properties which lie in \lsti{Prop},
Coq's logic domain. Notwithstanding some case studies, it is not yet
clear if how effective is PBT in the Coq's world.

In this thesis we describe a small Coq library with which it is
possible to perform Mutation Analysis on PBT. Our workflow will be
roughly the following: we will first generate many mutants of a Coq
script, then (with the aid of PBT) we will try and find whether the
newly generated mutants can be killed. If not, this points to
something unsatisfactory in the PBT setup.


\section{Project overview and results}
We decided to implement a small mutation library, \emph{MutantChick}, with which we can mutate Coq scripts using a simple DSL.

MutantChick is built on top of  \emph{MetaCoq} \cite{sozeau2020metacoq}, a powerful metaprogramming library which gives the developer all the tools to quote, unquote and perform AST modification of Coq's terms within Coq itself.

The rather unusual choice of performing mutation using metaprogramming
facilities stems from our priority to permit only \emph{type-safe} mutation
operators; a more common approach (which we can find, for example, in
\emph{mCoq} \cite{mutAnalCoq}) is to perform syntactical manipulations in a
different programming language (in that case, Java): while this latter
option can be more easily optimized, it does not give tools for
the user to define custom and safe operators.

Despite being in its infancy, MutantChick is  able to automatically emulate most 
of  manually-written mutants for Leroy's \emph{Listmachine}
(\cite{appel2006list} and \cite{komauli}) and for Software Foundations
(chapter \emph{Types.v}) (\cite{SwFoundations} and \cite{andrea}) into its DSL with relative ease.

Currently, the biggest limitation lies in the performances of
MutantChick, which is very memory-hungry and therefore not
particularly fast. This is mostly expected, as we are performing
computationally heavy manipulation of ASTs inside Coq, which is not meant as programming language, being as it is completely pure.


\section{Conclusions and future work}
The results we obtained with MutantChick are certainly encouraging: we
were able to replicate most of the  mutants in the literature, and to
generate many others. We found particularly interesting and useful the
possibility to declare custom, type-safe operators.

There are however two main improvements from which MutantChick would greatly benefit:

\begin{itemize}
\item{\textbf{Extraction to OCaml}: Coq is built upon a specific
    branch of constructive logic which makes it possible to 
    \emph{extract} Coq terms into OCaml programs
    \cite{letouzey2002new}. If we could extract at least the slower
    procedures into OCaml, we would definitively see a great speed
    increase and a significant reduction of memory usage. This is
    however non-trivial to do.}
\item{\textbf{Less boilerplate}: if a great number of mutants is generated, the boilerplate code needed to test each of them via PBT is significant.}
\end{itemize}


%\newpage\cleardoublepage
\renewcommand\refname{Main References}
\nocite{
  mutAnalCoq,
  mutAnalSurvey,
  SwFoundations,
  paraskevopoulou2015foundational,
  komauli,
  andrea,
  Lampropoulos0P19}
\bibliography{bibliografia}\newpage\cleardoublepage

\bibliographystyle{plain}



\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%  LocalWords:  QuickChick MATTEO CAVADA MutantChick MetaCoq AST mCoq%  LocalWords:  Listmachine
%  LocalWords:  ASTs OCaml
