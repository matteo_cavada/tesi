\chapter{Introduction}
\label{ch:intro}

\section{What is Mutation Analysis?}
Mutation Analysis is a well known testing techinque, used to evaluate the quality of a given test suite \cite{mutAnalSurvey}. 

In principle, the idea that lies behind mutation analysis is simple:
given a program $P$ and a test suite $T$ for $P$, a certain number of
\emph{mutant} versions of $P$ are generated - a mutant is a copy of
$P$ except that slight modifications are introduced. Every mutant
$M_i$ is then tested against $T$: if $T$ is \emph{good enough}, it
will be able to spot any bug introduced in the mutation phase, and if
so the mutant is said to be \emph{killed}. If on the contrary $T$ is
not able to find any bug introduced in $M_i$ then the mutant is said
\emph{live}

Living mutants may require manual analysis - sometimes a mutant is
\emph{semantically equivalent} to the original program, and hence no
bugs are introduced in the mutation phase; more often, though, a
living mutant implies that the test suite does not reach every
critical state of the original program, possibly indicating an
under-specified test-suite.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{capitoli/immagini/mutation-testing}
\caption{A graphical overview of mutation testing.}
\end{figure}

Mutation analysis efficacy relies on two generally-agreed principles: the \emph{Competent Programmer Hypothesis} (CPH) and the \emph{Coupling effect} \cite{offutt1992investigations}. The former assumes that competent programmers tend to write code which is just slightly different from the correct version of the program. The latter can be paraphrased as: test data which highlights simple errors will also probably highlight more complex errors. This last hypothesis justifies a simplification of the mutation process: only generating \emph{1\textsuperscript{st}-order mutants}\footnote{A n\textsuperscript{th}-order mutant is a mutant on which \emph{n} mutations have been sequentially applied.} is good enough to cover 99\% of 2\textsuperscript{nd} and 3\textsuperscript{rd}-order mutants, as shown in \cite{offutt1992investigations}.


While the idea behind Mutation Testings is simple, its implementation can be far from easy. In general, the biggest problem is that of performances: simple operators may induce a huge number of mutants - in that case it would be computationally unfeasible to exhaustively test all of them. Hence, two possible optimization approaches are possible: you can \emph{speed up} the computation by parallelizing code generation and testing, or you can \emph{reduce} the number of mutants you are going to test. As always, a mixture of these two philosphies is probably the best option, when possible. 



\subsection{Testing in Coq}
In programming languages, testing usually suffers from the consequences of what is known as \emph{Dijkstra corollary}\cite{dijkstra1972chapter}:

\begin{quote}
Program testing can be used to show the presence of bugs, but never to show their absence.
\end{quote}
that is to say that exhaustive testing is usually unfeasible because of the astronomical (if not infinite) number of states that a program can possibly reach.

Coq (and proof assistants in general) solve this problem by giving the user the possibility to write \emph{proofs} of correctness, which are guaranteed to be valid by the underlying logic (in Coq, constructive logic). 

In a way, mathematical proofs appear to be the ultimate form of testing, one that even overcomes the limitations of Dijkstra corollary - it seems therefore counterintuitive to perform mutation analysis (and, in general, any form of non-exhaustive testing) in Coq, because we would essentially be doing meta-testing on a form of testing which we know cannot be wrong (how can a proof, deemed correct by the compiler, be wrong?).

In practice, there are quite a few reason to perform mutation analysis: for example, as argued in~\cite{mutAnalCoq} it may be useful to understand if parts of the program are under-specified. Mostly, though, it can be useful, in combination with a Property-Based Testing library (see section \ref{pbt}), to discover if a theorem is wrong before actually attempting to prove it - this approach can potentially save many hours of tedious and pointless proof writing!


\paragraph{Property Based Testing (PBT)} \label{pbt}
Property Based Testing (PBT for short) is a form of automatic testing: instead of unit tests, PBT relies on user-defined \emph{properties} which are tested against automatically generated test cases. Data is generated on the spot by so-called \emph{generators} - generators for complex datatypes can be created through the use of combinators. Found counterexamples may be \emph{shrunk} - a \emph{shrinker} is a piece of code that can reduce a potentially big counterexample to a smaller one.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{capitoli/immagini/pbt}
\caption{A graphical overview of Property Based Testing. In general,
  two outcomes are possible: either a counterexample is found or
  not. It is however necessary to take into account the possibility
  that not enough adequate test cases are generated, possibly dueto \emph{sparse preconditions}, leading to insuffcient \emph{coverage} and the
  \emph{"too many rejections"} case.}
  \label{fig:pbt}
\end{figure}

PBT can be used to perform testing both in imperative and functional languages, although languages with strong type systems and pure code can especially benefit from it - one of the most famous PBT library, QuickCheck \cite{claessen2011quickcheck}, was in fact first developed in 1999 for Haskell. Since then, numerous PBT libraries have been released, such as \verb+fsCheck+ for \verb+F#+, \verb+ScalaCheck+ for Scala, \verb+Hypothesis+ for Python and \verb+jqwik+ for Java.

\paragraph{PBT in Coq}
QuickChick \cite{paraskevopoulou2015foundational} is the main PBT library for Coq; it can be used to test both property expressed within \lsti{Set} (in a similar fashion to how it is done in Haskell), but also property in \lsti{Prop}.

% Especially in the latter case, checking a property is rarely a completely automatic procedure: it is often needed to write possibly complex generators, maybe while struggling against Coq dreaded termination checker. 

% Tratto da tesi di andrea:
%So we have written custom generators following the implementation of [8].
%However, any programming task is more complicated in Coq. For example, QuickChick
%is not able to automatically derive a printer for parameterized types such as maps
%and we had to do it ourselves. The code tends to be more verbose since the
%only way to deal with partiality is via the Maybe monad, while FsCheck can use
%exceptions and avoid modifying the typed involved.
%More seriously, every equality that QuickChick touches needs to be proven
%decidable. Most of these proof are simple and can be solved with the dec_eq tactic,
%but sometimes, especially when comparing complex data structures such as maps,
%they can be long and slow down the testing process.

Let's look at an example of QuickChick in action. Suppose we have a simple list reversing function, which we want to validate via PBT. We may write a simple property testing if the function is involutive (that is, if applying it twice on any input yields the input itself):

\begin{lstlisting}
Fixpoint reverse (l: list nat) : list nat :=
  match l with
  | [] => []
  | x::xs => (reverse xs) + + xs
  end.

Definition prop_involutive (l: list nat) :=
  lists_equality (reverse (reverse l)) l.
\end{lstlisting}

How long may it take to prove a property like this? In general, it is quite difficult to make estimates beforehand\footnote{According to Hofstadter's law, \emph{it always takes longer than you expect, even when you take into account Hofstadter's Law} \cite{geb}!}. QuickChick may help us to find counterexamples before attempting to actually prove a statement; and in fact, when we invoke QuickChick on \lsti{prop_involutive}:

\begin{lstlisting}
QuickChick prop_involutive.

>>> QuickChecking prop_involutive...
>>> [0]
>>> *** Failed after 2 tests and 0 shrinks. (0 discards)
\end{lstlisting}

We obtain the counterexample \verb+[0]+ which suggests that our original \lsti{reverse} function is not correct -- and in fact, we can see that the right-hand side of the append in \lsti{reverse} is \lsti{xs} instead of \lsti{x}. 

While this is an example of successful and relatively painless usage of QuickChick, things are not always this easy:

\begin{itemize}
	\item{For complex datatypes, it is often necessary to manually write \emph{generators} - QuickChick provides a number of useful combinators, but it is not easy to establish if a given generator is a good one. What defines a good generator for a programming language instruction set, for example? It is clear that this is a highly non-trivial problem}
	
	\item{An hypothetical property \lsti{sorted xs => remove x (sorted xs)} presents a \emph{conditional} statement, that is: the property is valid on \verb+xs+ if \verb+xs+ is sorted to begin with. It is clear that, without ad-hoc generators, the probability to randomly draw a \emph{sorted} list on which we can test this property decreases quickly w.r.t. the length of the list. This problem is that of \emph{sparse preconditions}, and can lead to the \emph{too many rejections} case shown in figure \ref{fig:pbt}.}

	\item{Testing in \lsti{Prop} is often difficult, mostly because we need to convince QuickChick that every equality it encounters is decidable. Sometimes we are in luck and decidability can be proved with a one-liner \lsti{dec_eq} invocation; sometimes, though, especially when dealing with complex data structures, decidability proofs can be long and tedious, slowing down the testing process. Sadly, in Coq this is an inevitable obstacle, as no general procedure for automatic decidability proving can exist.}

\end{itemize}


\section{Thesis outline}
The goal of this thesis is to evaluate whether it is possible (and convenient) to write a mutation testing library directly in Coq and using metaprogramming techniques, and whether it is possible to integrate it with QuickChick. 

We hence introduce MutantChick, a small library built upon MetaCoq \cite{sozeau2020metacoq}, comprising of a set of built-in type-safe (where possible) operators and the possibility for the user to specify custom ones. We show a simple monadic DSL in which it is possible to express such mutations, possibly applying a random choice of mutants to improve perfomances, at the time of writing the biggest problem of MutantChick. 

We will then show MutantChick and QuickChick in action, performing some testing first on a simple model for typed arithmetic expressions as found in \emph{Software Foundations}\cite{SwFoundations}, then on theorems taken from the Coq implementation of the List Machine benchmark \cite{appel2006list}, building upon the work of Andrea Colò \cite{andrea}. 

We will assume basic knowledge of Coq and of monadic programming in general. 

The repository containing the source code of MutantChick can be found on Bitbucket\footnote{\url{https://bitbucket.org/matteo_cavada/tesi/src/master/mutantChick/}}.

% end