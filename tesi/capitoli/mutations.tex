\chapter{Mutation of Coq Scripts}
\label{ch:mutationsCoq}

In this chapter we will discuss the implementation details of MutantChick, a small mutation library written in Coq via metaprogramming. 

We begin by introducing MetaCoq, which gives us procedures for quoting and unquoting of Coq objects; in section~\ref{sec:mutantOperators} we then describe MutantChick built-in mutant operators with numerous examples. Section~\ref{sec:randomChoice} introduces a feature which enables us to randomly select just a subset of all the possibly generated mutants. Finally, in section~\ref{sec:codeGen} we present a simple DSL which unifies all these features and then, in section~\ref{sec:limitations} we discuss some limitations of MutantChick, which mainly relate to performances issues. A brief discussion on two other mutation analysis libraries and their differences with MutantChick will close the chapter.


\section{An introduction to MetaCoq} \label{sec:metaCoq}
MetaCoq \cite{sozeau2020metacoq} is a library that provides meta-programming features for Coq. A complete retelling of all the capabilities of this library is outside of the scope of this thesis; we will just focus on a subset of functionalities that will enable us to work on Abstract Syntax Tree (AST) of Coq objects\footnote{Incidentally, this subset of functionalities was previously a library on its own, known as TemplateCoq \cite{templateCoq} and later incorporated into MetaCoq.}.

\subsection{Quoting and unquoting} \label{sec:quoteUnquote}
\paragraph{\lsti{TemplateMonad}}
To access and modify Coq's environment, MetaCoq provides a monad, \lsti{TemplateMonad}, in which it is possible (among other things) to describe quoting and unquoting actions of Coq terms. Within the monad we can also describe other actions with side-effects, such as printing a string or injecting new objects inside Coq environment - this is allowed as these monadic programs, once invoked with the \lsti{Run TemplateProgram} vernacular command, will be run inside an OCaml environment.

Beyond the usual \lsti{bind} and \lsti{return} functions, MetaCoq gives many other monadic primitives, the most important ones being:
\begin{itemize}
	\item{\lsti|tmQuote : A -> TemplateMonad Ast.term|, which takes an object of any type and returns its AST representation}
	\item{\lsti|tmQuoteRec : A -> TemplateMonad program|, which quotes an object and every other object that it's dependent on, directly on indirectly (here, \lsti{program} refers to the quoted environment + the quoted expression)}
	\item{\lsti{tmQuoteInductive : qualid -> TemplateMonad mutual_inductive_body}, which quotes an inductive type}
	\item{\lsti|tmUnquote : Ast.term -> TemplateMonad typed_term|, which transforms back an AST into a Coq object, \emph{without} adding the resulting object to Coq environment}
	\item{\lsti{tmMkDefinition : ident -> Ast.term -> TemplateMonad unit}, which generates a new Coq definition from an AST and adds it in the environment}
	\item{\lsti{tmMkInductive' : mutual_inductive_body -> TemplateMonad unit}, which behaves similarly to \lsti{tmMkDefinition} but only on inductive types.}
\end{itemize}


\paragraph{MetaCoq Ast.term}
The AST which we obtain from quoting a Coq expression is as follows (some constructors are omitted for clarity of exposition, the full list can be found in \cite{sozeau2020metacoq}):

\begin{lstlisting}
Inductive term : Set :=
| tRel (n : nat)
| tProd (na : name) (ty : term) (body : term)
| tLambda (na : name) (ty : term) (body : term)
| tLetIn (na : name) (def : term) (def_ty : term) (body : term)
| tApp (f : term) (args : list term)
| tInd (ind : inductive) (u : universe_instance)
| tConstruct (ind : inductive) (idx : nat) (u : universe_instance)
| tCase (ind_and_nbparams: inductive×nat) (type_info:term)
        (discr:term) (branches : list (nat × term))
| tFix (mfix : mfixpoint term) (idx : nat)
\end{lstlisting}

\lsti{tRel} refers to a variable introduced by either a lambda (\lsti{tLambda}), a dependent product (\lsti{tProd}) or a local definition (\lsti{tLetIn}), its argument being is its DeBrujin index. \lsti{tApp} refers to the application of a function. \lsti{tInd} references an inductive type defined in the environment, while \lsti{tConstruct} references the constructor on an inductive type. \lsti{tCase} is used to internally represent if-then-else and pattern matching. Finally, \lsti{tFix} represents a fixpoint.


\subsection{An example of monadic metaprogramming}

A simple example of usage of the Template monad is shown in figure~\ref{templateMonadEx}: we define the function \lsti{copy} which takes any object, print its AST representation and then 
copies the same object in a new definition called \lsti{out}. We then execute this monadic action by invoking it in the \lsti{Run TemplateProgram} environment. As we can see in figure~\ref{templateMonadExOut}, \lsti{out} contains a perfect copy of the original expression.


\vspace{5mm}
\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[caption=An example of TemplateMonad use,frame=tlrb,label=templateMonadEx]
Definition copy {T} (t: T) 
  : TemplateMonad unit :=
    ast <- tmQuote t ;;
    tmPrint ast ;;
    tmMkDefinition "out" ast.

Run TemplateProgram (
      copy (fun x => 2 + hd x [9;8;7])
    ).

Print out.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[frame=tlrb,caption=result,label=templateMonadExOut]{Name}
out = fun x : nat, 2 + hd x [9; 8; 7]
     : nat → nat
\end{lstlisting}
\end{minipage}

In addition, here is the AST of the expression \lsti{fun x => 2 + hd x [9;8;7]} as printed by \lsti{tmPrint} and, in figure~\ref{fig:AstNoEdgeNumber}, its simplified\footnote{MetaCoq internally represents \lsti{nat}s as Peano numbers, i.e. the number 2 is stored as \lsti{S (S 0))}. Therefore, for clarity of exposistion we will represent the AST of numbers as the number themselves} graphical representation.

\begin{lstlisting}
(tLambda (nNamed "x") 
   (tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} [])
   (tApp (tConst "Coq.Init.Nat.add" [])
      [2;
      tApp (tConst "Coq.Lists.List.hd" [])
        [tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} []; 
         tRel 0;
         tApp (tConstruct {| inductive_mind := "Coq.Init.Datatypes.list"; inductive_ind := 0 |} 1 [])
            [tInd {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} [];
             tApp (tConstruct {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} 1 [])
             [9;8;7]]]]))
\end{lstlisting}

\begin{figure}
	\centering
	\begin{tikzpicture}
   		 \scalebox{.80}{\input{dot/astNoEdgeNumber.tex}}
	\end{tikzpicture}	
 	\caption{The first few nodes from Metacoq AST of expression \lsti{(fun x => 2 + hd x [9;8;7])}}
 	\label{fig:AstNoEdgeNumber}
\end{figure}


\vspace{5mm}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Mutant operators} \label{sec:mutantOperators}

\subsection{On the choice of mutant operators for Coq} \label{sec:onTheChoice}

Our aim was to develop a library in which operators by construction
are \emph{type-preserving}, i.e., they  generate terms that are not only well-typed, but have the same type of the source term --- we will
need to relax this assumption when we will introduce user-defined
operators, in which case the responsability to write such
terms is on the user; but even in that case, we can guarantee some
form of type-safeness. We will discuss this in~\ref{op:userDefined}
and in~\ref{ch:listmachine}.


\subsection{General description of operators}
\paragraph{A skeleton for operators in MutantChick}
In MutantChick, \emph{mutant operators} are defined by two components:
 
\begin{itemize} \label{implementationOperators} 
	\item{A function \lsti{check :: term -> bool} which establishes if a given TemplateCoq term is suitable for mutation.}
	
	\item{A function \lsti{trans :: term -> term} which describes what a term is transformed into.}
\end{itemize}

This duality will closely mirror the two-phase algorithm for applying operators on ASTs, where we first search for subtrees \emph{suitable} for mutation, and then we effectively apply the mutation, \emph{transforming} the subtree; more on this in section~\ref{sec:astManip}.

Table~\ref{tab:tableOperators} describes, at a very high level, the operators currently implemented in MutantChick.  

\begin{table}[h!]  
 \begin{center}
   \begin{tabular}{l|l|r} \label{tab:tableOperators}
    \textbf{Kind} & \textbf{Operator} & \textbf{Description}\\
    \hline
	\multirow{2}{*}{General} & Substitute & Substitutes a term\\ 
			& Swap & Exchanges two appearing terms\\ 
    		    & IfThenElse & Exchanges then and else\\ 
    		    & DelImplications & Deletes logical implications\\ 
    		    & UserDefined & User defined operator\\
    \hline
    \multirow{2}{*}{Inductive} & NewConstructor & Adds new constructor\\ 
    	        & SubConstructor & Substitutes a constructor\\ 
            & OnConstructor & Applies a general operator to a constructor\\ 
    	        & OnConstructors & Applies a general operator to a whole inductive definition\\ 
      \hline
    \end{tabular}
  \end{center}
  
  \caption{List of MutantChick's operators.}
\end{table}


\paragraph{BNF grammar}
Here we show a BNF grammar for the operators DSL.

\begin{grammar} 
<string>   ::= A Coq string

<checkFunction> ::= A Coq function of type \lsti{term -> bool}

<transFunction> ::= A Coq function of type \lsti{term -> term}

<coqExpr>   ::= Any Coq expression

<MetaTerm> ::= `<\%' <coqExpr> `\%>'

<op> ::= <coqExpr> `==>' <coqExpr>
	\alt `Swap' (<string> | <metaTerm>) (<string> | <metaTerm>)
	\alt `IfThenElse'
	\alt `DelImplications'
	\alt `NewConstructor' <string> <metaTerm>
	\alt `SubConstructor' <string> <string> <metaTerm>
	\alt `OnConstructor'  <string> <op>
	\alt `OnConstructors' <op>
	\alt `UserDefined' <checkFunction> <transFunction>
\end{grammar}


\subsection{General operators} \label{simple-operators} 
We now explore some very simple operators, which can be applied to every Coq expression.

\paragraph{Simple substitution}
A substitution \verb+t1 ==> t2+ substitutes every occurrence of the object \lsti{t1} with object \lsti{t2}. 

This operator is defined as an inductive constructor: \lsti|Substitute {A} (t1 t2: A)|: we have decided to (implicitly) carry the type \lsti{A} in order to assure that the mutation induced by \verb+t1 ==> t2+ is type-preserving.

In the example that follows, we use \lsti{Substitute} to change the occurrence of multiplication with addition inside a simple arithmetic expression; note that both functions have the same type, i.e.. \lsti{nat -> nat -> nat}.


\vspace{5mm}
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}[caption=A simple substitution,frame=tlrb]{Name}
Run TemplateProgram ( 
  Mutate <% 4 * 5 %>
         using (Init.Nat.mul $==$> Init.Nat.add)
         named "out"
  ).         
Print out.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[caption=result]{Name}
out = 4 + 5
     : nat
\end{lstlisting}
\end{minipage}


\paragraph{Swap}
\label{par:swap} 
\lsti{Swap t1 t2} is a bidirectional substitution; it exchanges the order of appearance of the two objects \lsti{t1} and \lsti{t2} inside an expression; for the mutation to succeed, both \lsti{t1} and \lsti{t2} must occur in the expression. 

\lsti{t1} and \lsti{t2} can be either variable names and/or constant terms; however, \lsti{Swap} has type \lsti{Swap : Swappable -> Swappable -> Operator}: \lsti{Swappable} is a convenience type used to represents either the name of a variable bound by a lambda or a \lsti{forall}, or a meta-term - thanks to coercions, however, it is not necessary to explicitly denote a string or a meta-term as belonging to \lsti{Swappable}.

In the following example, we will exchange $+$ with $*$ using \lsti{Swap}:

\vspace{5mm}
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}[caption=An example of Swap,frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% 3 + 4 * 5 %>
         using (Swap <% Init.Nat.mul %> 
                     <% Init.Nat.add %>)
         named "out"
    ).
Print out.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[caption=result]{Name}
out = 3 * (4 + 5)
     : nat
\end{lstlisting}
\end{minipage}

Another example: this time we will exchange two variables introduced by a lambda function.

\vspace{5mm}
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}[caption=Another example of Swap,frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% fun x => fun y => x - y %>
         using (Swap "x" "y")
         named "out"
    ).
Print out.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[caption=result,mathescape=true]{Name}
out = $\lambda$ x y : nat, y - x
     : nat → nat → nat
\end{lstlisting}
\end{minipage}


\paragraph{DelImplications} \label{par:delimplications}
\lsti{DelImplications}, as the name suggests, removes logical implications: this means that if we have an expression in the form \lsti{A -> B}, two mutants will be induced by \lsti{DelImplications}, \lsti{A} and \lsti{B}. 

In case an expression has more than one implication, all possible combinations of "reduced" implications are generated. Bearing in mind that the number of such combinations is exponential in the number of implications, it is strongly advised to enable random selection of mutants (as described in section~\ref{sec:randomChoice}). 

A simple example of \lsti{DelImplications} would be:

\vspace{5mm}
\noindent\begin{minipage}{.60\textwidth}
\begin{lstlisting}[caption=An example of DelImplications,frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% forall n, even n -> even (n+2) %>
         using (DelImplications)
         named "out"
).
Print out.
Print out0.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.30\textwidth}
\begin{lstlisting}[caption=result,mathescape=true]{Name}
out = forall n : nat, even (n + 2)
     : Prop
out0 = forall n : nat, even n
     : Prop
\end{lstlisting}
\end{minipage}

It may happen that \lsti{DelImplications} will remove the only term containing a certain variable \lsti{v} bound by a forall. As an example, applying \lsti{DelImplications} on \lsti{forall n, even 0 -> even n} will result in a mutant \lsti{mutant = nat → even 0 : Prop}.


\paragraph{IfThenElse}
\lsti{IfThenElse} exchanges the order of \lsti{then} and \lsti{else} clauses in an \lsti{if} expression. This is actually a special case of the \lsti{Swap} operator but, for convenience, we decided to implement it as an ad-hoc operator.


\paragraph{UserDefined} \label{op:userDefined}
It is possible to define custom operators in MutantChick. To do this, \\ \lsti{UserDefined (check: term -> bool) (trans: term -> list term)} is used. 

\lsti{check: term -> bool} is a function that establishes if a given TemplateCoq term is suitable for mutation; \lsti{trans: term -> term} describes instead what a term is transformed into.

This way of creating custom operators is clearly not typesafe, as we are merely describing transformation on a syntax tree. However, we will show in section~\ref{par:m16} (mutation M16)  a way to overcome this obstacles and declare custom, typesafe operators.



\subsection{Inductive operators}
We describe here some operators useful to mutate inductively-defined types.

For convenience, let's define a simple inductive type on which we will later apply some of the new operators:

\begin{lstlisting}
Inductive even: nat -> Prop :=
| ev_Z: even 0
| ev_SS: forall n, even n -> even (S (S n)).
\end{lstlisting}


\paragraph{Add a new constructor}
\lsti{NewConstructor (newNameCons: ident) (type: term)} adds a new constructor to an \lsti{Inductive} type; the name of the constructor will be \lsti{newNameCons} and its type will be specified as a TemplateCoq term.

In the following example, we will add a new constructor called \lsti{ev_double}, according to which if a number \lsti{n} is \lsti{even}, then $2 \cdot n$ is \lsti{even} too.

\vspace{5mm}
\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[caption=Adding a constructor,frame=tlrb,label=addConstr]{Name}
Run TemplateProgram (
  Mutate <% even %>
         using (NewConstructor "ev_double"
               <% fun even_mut => forall n,
                   even_mut n -> 
                   even_mut (n + n) %>)
         named "even_mut"
).
Print even_mut.    
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[caption=result,mathescape=true]{Name}
Inductive even_mut : nat → Prop :=
  | ev_Z0 : even_mut 0
  | ev_SS0 : forall n : nat, 
    even_mut n → even_mut (S (S n))
  | ev_double : forall n : nat, 
    even_mut n → even_mut (n + n)
\end{lstlisting}
\end{minipage}


\paragraph{Substitution of a constructor}
\lsti{SubConstructor (oldName newName: ident) (type : term)} substitute the constructor named \lsti{oldName} with a new constructor called \lsti{newName} which has form \lsti{type}. 

Here with \emph{substitution} we basically mean: delete a constructor and add a new constructor with a new type, possibly with a new name too.

In the following example, we substitute the constructor \lsti{ev_Z: even 0} with the equivalent (but odd-looking) \lsti{ev_PS: even (Nat.pred 1)}.

\vspace{5mm}
\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[caption=Constructor substitution,frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% even %>
    using (SubConstructor
            "ev_Z" "ev_PS"
            <% fun (even_mut: nat -> Prop) => 
            		 even_mut (Nat.pred 1) %>)
    named "even_mut"
). 
Print even_mut.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[caption=result,mathescape=true]{Name}
Inductive even_mut : nat → Prop :=
  | ev_PS : 
    even_mut (Nat.pred 1)
  | ev_SS0 : forall n : nat, 
    even_mut n → even_mut (S (S n))
\end{lstlisting}
\end{minipage}


\paragraph{Mapping over a constructor}
Given the fact that a constructor for an inductive type is, after all, nothing else than a  Coq expression, we may want to apply some of the general operators described in section~\ref{simple-operators} on them. 

This is done by the operator \lsti{OnConstructor (nameConstructor: ident) (op: Operator)} which, given the name of a constructor, applies as operator \lsti{op} to it. 

As an example, we use the operator \lsti{DelImplications} (see~\ref{par:delimplications}) to remove the sequent of constructor \lsti{ev_SS} of \lsti{even}.

\vspace{5mm}
\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[caption=Map on constructor,frame=tlrb]{Name}
Run TemplateProgram (
  Mutate <% even %>
         using (OnConstructor "ev_SS"
                 DelImplications)
         named "even_mut"
  ).
Print even_mut0.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[caption=result,mathescape=true]{Name}
Inductive even_mut0 : nat → Prop :=  
  | ev_Z0  : even_mut0 0 
  | ev_SS0 : forall n : nat, even_mut0 n
\end{lstlisting}
\end{minipage}


\paragraph{Mapping over \emph{all} constructors}
As an extension of \lsti{OnConstructor}, we implemented \lsti{OnConstructors} \lsti{(op: Operator)} which applies operator \lsti{op} to all constructors of an inductive.

Usual precautions should be taken when the number of possible induced mutations is high: in these cases, it is strongly advised to enable the option for random selection of mutants.



\section{Mutant generation}

\subsection{AST manipulation}\label{sec:astManip}
As we have shown in section~\ref{sec:quoteUnquote}, a Coq expression translated into a Metacoq term is a (possibly big) tree-like data structure, on which we can implement usual tree-traversing functions such as \lsti{map}, \lsti{fold}s and so on. 

A mutation on an AST is implemented in two phases:

\begin{itemize}
	\item A \emph{search phase}, in which we find \emph{all} locations inside the tree corresponding to the kind of term we want to mutate
	
	\item A \emph{mutation phase}, in which a transformation function is applied to the terms in the locations identified in the \emph{search phase} 
\end{itemize}

These two phases closely mirror the inner implementation of operators, which we described in section~\ref{implementationOperators}. 

As we will see in~\ref{sec:randomChoice}, the \emph{mutation phase} can be subject to some optimization which can reduce the number of locations to mutate, hence improving performances.


\paragraph{An overview of the algorithms}
An example AST is shown in figure~\ref{fig:AST} in a simplified version. The labels on the edge represent "coordinates" inside the tree; every node in the AST can be hence identified with a unique sequence of coordinates: for example, the function \lsti{List.hd} is reachable only by the path \verb+3,3,1+.

\begin{figure}
	\centering
	\begin{tikzpicture}
   		 \scalebox{.75}{\input{dot/astWithEdgeNumber.tex}}
	\end{tikzpicture}	
 	\caption{The first few nodes from Metacoq AST of expression \lsti{(fun x => 2 + hd x [9;8;7])}, this time with number on the edges used to identify paths in the tree.}
 	\label{fig:AST}
\end{figure}

With this approach we uniquely identify nodes which can be a target for a mutational operator of our choice. The tree is traversed in a depth-first pre-order fashion: first we establish if the node is suitable for mutation (using mutational operator \lsti{check} function); then, we recurse on its sub-trees.

This first phase is in fact a \emph{search} phase, and is useful for performance reasons: a more traditional functional-oriented approach (using \lsti{map} and \lsti{fold}-like functions) was significantly slower and more difficult to integrate with randomness-related features (see section~\ref{sec:randomChoice}).

If enabled by the user, a pre-emptive and random selection of a subset of these coordinates is drawn, in order to limit the final number of mutants that will be generated. 

Once identified, the nodes pointed by these coordinates are mutated according to the mutational operator \lsti{trans} function. 


\subsection{Random choice of mutants} \label{sec:randomChoice}
The number of mutants induced by an operator can be quite big~\cite{mutAnalSurvey}, especially when we are mutating complex pieces of code. Given the fact that MutantChick is, at the time of writing, entirely written in Coq, this represents a huge problem: secondary memory can be quickly saturated and, as a result, a crash of \verb+coqtop+ can happen.

To reduce the probability of this, we implemented mechanisms to randomly draw mutations with a certain probability or to draw only and exactly $n$ mutations. 

\paragraph{Pseudo-Random Number Generator (PRNG)} \label{par:prng}
Coq, being a pure language, doesn't allow to write programs dealing with the outside world, as that would compromise its logical consistency: the only way to interact with the outside world is by writing an Ocaml plugin for Coq. While this latter strategy is surely an effective one, it was outside of the scope of this thesis. 

Hence, we implemented a PRNG  in pure Coq as a Linear-Feedback Shift Register (LFSR) on 7 bits with feedback polynomial $x^7 + x^6 + 1$; this gives the possibility to generate pseudo-random sequences of numbers starting from a seed. A graphical representation of a LFSR is in figure~\ref{fig:lfsr}. Of course, this does not solve entirely the problem of randomness inside Coq: the user has to give a seed to the PRNG for it to start generating numbers; the seed should be generated randomly, but for our purpose we decided that a manually-chosen seed is good enough. 

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{capitoli/immagini/LFSR-F16}
\caption{A visual representation of a LFSR on 16 bit with feedback polynomial $x^{16} + x^{14} + x^{13} + x^{11} + 1$; when a new number is needed, the last generated number is shifted right by one place, and the XOR between bits 16,14,13,11 and 1 is placed in the leftmost bit.}
%Taken from \href{https://en.wikipedia.org/wiki/Linear-feedback_shift_register#/media/File:LFSR-F16.svg}{here}.}
\label{fig:lfsr}
\end{figure}

Helper functions are defined to draw a single number, a list of numbers or to choose random elements from a list.


\paragraph{Random draws}
In between the \emph{search phase} and \emph{mutation phase} we can choose which location in the AST we want to mutate. The user has three possibilities:

\begin{itemize}
	\item \emph{Mutate everywhere} - that corresponds to \emph{not} making any selection

	\item \emph{Pick mutations with probability $p$} - for every mutable location in the AST, a random number is generated; if it is bigger then a certain threshold value (chosen by the user), then the mutant is generated; otherwise, it doesn't.
	
	\item \emph{Pick a maximum of $n$ mutations} - a maximum of $n$ location are drawn among all the coordinates found in the \emph{search} phase.
\end{itemize}

These three concepts are embedded, respectively, in the sum type \lsti{MutationNumber}:

\begin{lstlisting}[]{Name}
Inductive MutationNumber : Type :=
| AllMutations
| RandomMutations (seed threshold: nat)
| MaxNMutations (num seed threshold: nat)
\end{lstlisting}
 %
where \lsti{seed} is a seed value as described in the section above; when we need to establish if a mutation will take place or not, the PRNG is executed with this value as a starting seed. The generated number will then be compared against the \lsti{threshold} value: only if it is greater, no mutation will take place. Both \lsti{seed} and \lsti{threshold} are values between 0 and 128~\footnote{This is not strictly necessary, but because the PRNG is implemented only on 7 bits, it doesn't make sense to use values greater than 128.}.

The parameter $num$, only present in \lsti{MaxNMutations}, is the maximum number of mutants that will be drawn.


\section{Mutation workflow}

\subsection{Code generation} \label{sec:codeGen}

We now describe a simple DSL, embedded in MetaCoq TemplateMonad (see~\ref{sec:metaCoq}), to implement code generation.

\paragraph{A formal grammar}
A BNF grammar of the DSL is as follows:

\begin{grammar}\label{grammar:mutate}

<root> ::= `Mutate' <MetaTerm> `using' <op> `named' <string> \{<random>\} 
      \alt `MultipleMutate' <MetaTerm> `using' <opList> `named' <string> \{<random>\}

<random> ::= `generating' <randomConfig>

<randomConfig> ::= `AllMutations' \alt `RandomMutations' <nat> <nat>

\end{grammar}


\paragraph{\lsti{Mutate} syntax}
The \lsti{Mutate} syntax is used to apply a single operator to an expression. At its fullest, \lsti{Mutate} consists of four arguments: the expression to mutate, the operator, the basename for the newly generated definitions and an optional arguments indicating whether to use random selection or not.

We can see an example in figure~\ref{mutateEx}. We can note the following:

\begin{itemize}
	\item It is necessary to quote the expression to mutate.
	\item If more than a mutant is generated, new definitions will be named \lsti{basename} followed by a number.
	\item The \lsti{generating (...)} clause can be omitted; in that case, \lsti{AllMutations} will be used by default.
\end{itemize}

\begin{lstlisting}[caption=A full example of \lsti{Mutate} syntax,label=mutateEx]
Run TemplateProgram (
  Mutate <% firstn 2 [2;3;2] %>
         using (2 => 9)
         named "two_nine_rand"
         generating (RandomMutations 34 64)
  ).
\end{lstlisting}



\paragraph{\lsti{MultipleMutate} syntax}
\lsti{MultipleMutate} is identical to \lsti{Mutate}, but it accepts a \emph{list} of operators instead of just a single one. As before, the \lsti{generating} clause can be omitted.

An example is shown in~\ref{multipleMutateEx}. 

\begin{lstlisting}[caption=A full example of \lsti{MultipleMutate} syntax,label=multipleMutateEx]
Run TemplateProgram (
  MultipleMutate
     <% firstn 2 [3;2;0] %>
     using [
        firstn 2 => (@skipn nat 1);
        firstn 2 => (List.map (fun x => x*2))
     ]
     named "multMut" 
     generating (RandomMutations 4 45)
    ).
\end{lstlisting}


\subsection{Current limitations}\label{sec:limitations}
\paragraph{Performances}
Performances are, at the moment, the biggest limitation of MutantChick: AST manipulation is carried out in vanilla Coq; this makes the whole mutation workflow quite slow, because Coq is of course not thought to be a performant, general-purpose language. There are also problems regarding memory use.

A question naturally arises: why develop MutantChick entirely in Coq, then? Our main interests were being able to express typesafe mutant operators using Coq (or, rather, MetaCoq) directly, and being able to apply QuichChick tests interactively. \verb+mCoq+, for example, follows a completely different approach, which we describe briefly in section~\ref{par:mCoq}. 

A possible solution to the performance problem is the \emph{extraction} to OCaml of (Meta)Coq code responsible for the heaviest computations on the AST. In this way, it could be possible to write an OCaml plugin which could exploit OCaml speed while still being able to have interactive Coq sessions. A similar approach is mentioned in chapter 5 of \cite{sozeau2020metacoq}; however, it is outside the scope of this thesis. 


\paragraph{Mutation of pattern matching}
It is well known that Coq does not permit partial pattern-matching, as they will be rejected by Coq remarking that we just defined a \emph{non exhaustive pattern-matching}. 

It is in fact non-trivial to apply mutations to the left-hand side of a pattern matching while assuring that the resulting mutant will compile. A way around this may be to always add a catch-all case to reject non-exhaustiveness errors --- at that point, however, we could possibly face \emph{pattern redundancy} errors, which again are non-trivial to detect when working with syntactic trees. 

We hence decided that, while this kind of mutation may be desireable, we could not immediatly implement it in MutantChick.

\paragraph{Implicit arguments}
A minor inconvenience is that when a mutation takes places on an object with implicit types, the resulting mutant will have all those implicit types explicited. 

\vspace{5mm}
\noindent\begin{minipage}{.43\textwidth}
\begin{lstlisting}[caption=A simple example,frame=tlrb,label=implicitArgs]{Name}
Definition example {A: Type} (l: list A) 
                 : list A :=  firstn 2 l.

Run TemplateProgram (
  Mutate <% @example %>
         using (firstn => skipn)
         named "example_mut"
    ).
Print example.
Print example_mut.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.50\textwidth}
\begin{lstlisting}[caption=results,mathescape=true]{Name}
example = $\lambda$ (A : Type) (l : list A), 
 		  firstn 2 l
     : $\forall$ A : Type, list A → list A
Argument A is implicit and maximally inserted


example_mut = $\lambda$ (A : Type) (l : list A), 
		 skipn 2 l
     : forall A : Type, list A → list A
\end{lstlisting}
\end{minipage}

In figure~\ref{implicitArgs} is an example of such phenomenon: \lsti{example} has an implicit argument \lsti{A: Type} which, during code generation, is made automatically explicit.


\paragraph{Boilerplate} \label{par:boilerplate}
In real-world code, it is often the case that the property we want to test does not directly call a mutant definition we have generated; rather, we have a series of intermediate functions which, at some depth in the stack, eventually call our mutant definition. In that case, it is necessary to mutate not only a single definition but all the functions interposed between the mutant and the property.

These "chains" of mutation, at the time of writing, need to be manually written. This can clearly lead to a lot of boilerplate code. We partially address this repetitive code by introducing an helper function, \lsti{change_occurrences}, which does the renaming without using the \lsti{Mutate} syntax% , but at the moment this does not help much
. An example of usage of \lsti{change_occurrences} can found in section~\ref{par:m3}, mutation M3.




\section{Related work}
A brief discussion of two mutation testing frameworks follows, highlighting some differences with MutantChick.

The literature on mutation testing focuses mainly on imperative languages \cite{mutAnalSurvey}; while it is not difficult to port some proposed operators for imperative languages to Coq, many others cannot be easily translated to functional languages in general.

Some work has been done to study mutation testing also on functional programming languages such as Haskell, with the library \verb+muCheck+ \cite{mucheck} from which we initially took inspiration as we shall see next in  section~\ref{par:muCheck}. 

At the time of writing, a new mutational framework for Coq is being developed \cite{mutAnalCoq}; a discussion on the differences with MutantChick is in section~\ref{par:mCoq}.


\subsection{muCheck} \label{par:muCheck}
As said before, muCheck \cite{mucheck} was of great ispiration in the beginning of the work for this thesis. As MutantChick started to take shape, however, an intrinsic limitations appeared: namely, the impossibility to express "compilable" operators, operators which always induce a type-correct mutant. 

\verb+muCheck+ mutant operators are in fact mostly syntactical substitutions. As an example, suppose we want to mutate this simple function:

\begin{lstlisting}[language=haskell]
simpleFoo :: Int -> Int
simpleFoo x = x + 0
\end{lstlisting}

We may want to apply the operator \lsti[language=haskell]{(Symbol "+" => Symbol "**")}, which substitutes the occurrences of \verb|+| with \verb+**+. 

However, \verb|+| and \verb+**+ have different types (respectively, \lsti{Num a => a -> a -> a} and \lsti{Floating a => a -> a -> a}); applying this operator would result in a mutant which cannot typecheck, and hence cannot compile. On the contrary, correct MutantChick operators will always induce a compilable mutant (possibly with the only exception of \lsti{UserDefined} operators, see~\ref{op:userDefined}).

Apart from this, we re-implemented in MutantChick some muCheck concepts such as \verb+==>+ (the \lsti{Substitute} operator) and \verb+==>*+ (applying a list of operators to the same expression). 


\subsection{mCoq} \label{par:mCoq}
\verb+mCoq+ \cite{mutAnalCoq} was released few months after the beginning of the work on this thesis (although the original paper had already been publicly available for some time). Its main purpose is to provide a fast, parallelizable library for mutation testing, alongside a nice web interface to analyze the results. 

While MutantChick attempts to perform much of the work within Coq, \verb+mCoq+  follows a completely different philosophy: it exports the AST of a Coq script as an \emph{S-expression} using \verb+SerAPI+ \cite{GallegoArias2016SerAPI}, an OCaml-based tool; it applies mutations to the sexp using a tool written in Java; then the results are exported in an HTML file, suitable for human analysis.

Sadly, at the time of writing, there is little support for custom operators outside of built-in ones. It is theoretically possible to add new operators by writing a Java class and implementing the \lsti{Mutation} interface, but this does not seem encouraged nor easy to do: see for example the code in listing~\ref{mCoqOp} for the \verb+ReplaceFalseWithTrue+ operator. Of course, even if it was encouraged, it is not trivial to implement operators in a type-safe fashion, as no type control can be easily enforced within Java.

\begin{lstlisting}[language=java,caption=Java source code of a mCoq operator,label=mCoqOp]
public class ReplaceFalseWithTrue implements Mutation {

    @Override
    public void mutate(Sexp sexp) {
        if (canMutate(sexp)) {
            sexp.get(ONE).get(ONE).get(ZERO).get(ONE).get(TWO).set(ONE, SexpFactory.newAtomicSexp(SEXP_true));
        }
    }

    @Override
    public boolean canMutate(Sexp sexp) {
        return SexpUtils.isCRefSexp(sexp, SEXP_false);
    }
}
\end{lstlisting}

In general, mCoq approach of \emph{mutation theorem proving} seems to suffer from inherent problems. Some of its operators are perplexing, such as \verb+GIC+ (reverse the order of the constructors in the definition of an inductive type): it is difficult to imagine a meaningful way to use this operator. Let's look at an example of killed \verb+GIC+ mutant, where the order of appereance of the first two constructor of \lsti{lex_order} is reversed:

\begin{lstlisting}
Inductive lex_order : string -> string -> Prop := 
  | lex_order_empty :   (...)  
  | lex_order_char_lt : (...)
  | lex_order_char_eq : (...)
  | lex_order_empty_string : (...)
\end{lstlisting}
      
Reversing the order means that every proof that relies on that previous ordering will not typecheck anymore; for example, in the following proof we will get an error in proximity of the first inductive case: the proof expects the \lsti{lex_order_empty} case, while the mutant gives to the proof the \lsti{lex_order_char_lt} case instead. 

\begin{lstlisting}[caption={This proof, when executed with the mutant version of \lsti{lex_order}, fails to compile.}]
Lemma lex_le_in_lex_order : forall (s1 s2 : string),
      lex_order s1 s2 -> lex_le s1 s2.
Proof.
    intros s1 s2 H.
    induction H.
    - right. (* Expects lex_order_empty, 
                receives lex_order_char_lt instead. *)
      reflexivity. (* Raises error *)
    - left.  (* The correct branch for the lex_order_char_lt case *)
      apply lex_lt_lt.
      assumption.
    - (* ... *)
\end{lstlisting}
  
Is this a proper \emph{bug}, though? One could argue that, while it's true that the file doesn't type-check anymore, the introduced error is absolutely irrelevant from a practical point of view; indeed, reordering the branches of e.g. a proof employing induction cannot but create a semantically equivalent proofs. 
%Indeed, for the principle of \emph{proof irrelevance}, all proofs of a proposition \verb+P+ are interchangable with one another; and reordering the branches of said proofs falls into such kind of variation.

Having said that, there is no doubt that \verb+mCoq+ is an impressive piece of software, undoubtely useful for fast mutation testing of big programs, and with an ever-increasing support of new functionalities.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Main"
%%% End:

%  LocalWords:  MutantChick MetaCoq AST TemplateCoq TemplateMonad Ast
%  LocalWords:  monad TemplateProgram OCaml tmQuote tmQuoteRec qualid
%  LocalWords:  tmQuoteInductive tmUnquote tmMkDefinition ident tRel
%  LocalWords:  tmMkInductive tLambda tProd tLetIn DeBrujin tApp tInd
%  LocalWords:  tConstruct tCase
