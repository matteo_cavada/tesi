\chapter{Mutation examples}
\label{ch:mutationExamples}

In this chapter, we will show how MutantChick is able to generate significant mutations on two language models, that is the language of simply typed arithmetic expressions\footnote{\url{https://softwarefoundations.cis.upenn.edu/plf-current/Types.html}}, a chapter of Software Foundations\cite{SwFoundations}, and that of the List Machine \cite{appel2006list}, a model for a typed assembly language. 

In both cases, we will focus on:

\begin{itemize}
	\item{Apply mutant operators, whether they are built-in ones or custom-made, to obtain meaningful mutants}
	\item{Employ QuickChick to validate whether the mutants follow some choosen theorems}
\end{itemize}

As for the second point, given that we are working with models of \emph{typed} languages, we will study two properties in particular:

\begin{itemize}
	\item{\emph{Progress} - given a well-typed program and an environment, it is always possible to either run an evaluation step or halt.}
	\item{\emph{Preservation} - if a well-typed block steps, then an environment typechecking the next instruction exists.}
	\item{\emph{Soundness} - given a well-typed program starting from an initial configuration, it will never get stuck.}
\end{itemize}



\section{Typed arithmetic expressions}
This section presents two examples of mutations applied within a toy language, the Typed Arithmetic Language (TAL from now on), introduced in the relative chapter of Software Foundations.

\paragraph{Mutating the typechecker using \lsti{IfThenElse}}

In TAL only two types exists: \lsti{TBool} and \lsti{TNat}. 

Obviously, not every possible expression in TAL in typeable; hence, the typechecker (implemented in the function \lsti{typeOf}) must take into account the possibility of encountering an untypeable term - we can do this using the \lsti{option} monad, which allows us to express partiality. The output of \lsti{typeOf} can therefore be either \lsti{Some TBool}, \lsti{Some TNat} or \lsti{None}. 

Full code of \lsti{typeOf} follows:

\begin{lstlisting}
Fixpoint typeof t :=
  match t with
  | ttrue  => ret TBool
  | tfalse => ret TBool
  | tzero  => ret TNat
  | tsucc(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TNat
      else None
  | tpred(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TNat
      else None
  | tiszero(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TBool
      else None
  | tif t1 t2 t3 =>
      t' <- typeof t1 ;;
      if eqTy t' TBool then
        t' <- typeof t2 ;;
        t'' <- typeof t3 ;;
        if eqTy t' t'' then ret t'
        else None
      else None
  end.
\end{lstlisting}

With this premise, we try to declare a mutation on \verb+T_Test+ where we enforce that the first argument of \lsti{tif} must not be of type \lsti{TBool}. To do this, we employ the \lsti{IfThenElse} operator which inverts a \verb+then+ clause with its \verb+else+ clause, so that when we check that the type of \lsti{t1} is \lsti{TBool}, we do the opposite of what we would do in the correct version of \lsti{typeof}.
%
%$$
%\infer{\vdash (\text{tif t1 t2 t3}) \in T} {
%t1 \in \text{T} & t2 \in T' & t3 \in T'' & T \neq TBool } 
%$$


\vspace{5mm}
\noindent\begin{minipage}{.55\textwidth}
\begin{lstlisting}[frame=r]
Run TemplateProgram (
  Mutate <% typeof %>
         using IfThenElse
         named "typeof_mut"
         generating (RandomMutations 51 50) ;;
  
  change_occurrences "typeable"
                     "typeof" "typeof_mut"
                     "typeable_mut" ;;
  change_occurrences "progressP"
                     "typeable" "typeable_mut"
                     "progressP_mut"
).
QuickChick progressP_mut.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[]
QuickChecking progressP_mut...
tif tzero tfalse ttrue
*** Failed after 49 tests and 0 shrinks. (42 discards)
\end{lstlisting}
\end{minipage}

A counterexample is quickly found, and progress is falsified. The counterexample shown is interesting, and we can reconstruct \emph{why} our mutation introduces a bug:

\begin{itemize}
	\item{\lsti{tif tzero tfalse ttrue} is typeable, so it must either be a value or we can take an evaluation step from it;}
	\item{Clearly, a \lsti{tif} is not a value, so it must be "step-able";}
	\item{\lsti{canStep}, given that the first argument of \lsti{tif} is neither \lsti{ttrue} nor \lsti{tfalse}, attempts to \lsti{eval} that first argument, i.e. \lsti{tzero};}
	\item{However, \lsti{eval tzero} is defined as \lsti{None} as it is an atomic value (\lsti{isVal tzero = true})}
	\item{This \lsti{None} is propagated; \lsti{canStep} hence outputs \lsti{false}, as no step of evaluation are possible from the current instruction. This falsifies the right hand side of \lsti{progressP_mut}.}
\end{itemize}





\paragraph{Mutating the typechecker using \lsti{Substitute}}

We will now mutate the same \lsti{typeof}, but this time using the operator \lsti{Substitute}. In particular, we will mutate the typing rule \verb+T_Test+, where we will enforce that the types of \lsti{t2} and \lsti{t3} must be different:

%$$
%\infer{\vdash (\text{tif t1 t2 t3}) \in T} {
%t1 \in \text{TBool} & t2 \in T & t3 \in T} 
%$$
 
%$$
%\infer{\vdash (\text{tif t1 t2 t3}) \in T} {
%t1 \in \text{TBool} & t2 \in T & t3 \in T' & T \neq T' } 
%$$
\begin{lstlisting}
Run TemplateProgram (
   Mutate <% typeof %>
          using (eqTy => (fun x y => negb (eqTy x y)))
          named "typeof_mut"
          generating (RandomMutations 51 50)
).
\end{lstlisting}

We use here a simple substitution where we negate the output of \lsti{eqTy} (the equality for types). Note that we use random selection of mutants, although this is not strictly necessary in this case (there are only three mutants resulting from this operator, so there are no problems of performances).

We then proceed to mutate all intermediate functions that use \lsti{typeof}. Finally, we mutate our definition of \verb+Progress+ and we quickCheck the resulting property.

\vspace{5mm}
\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[frame=r]
Definition progressP e :=
  implication (typeable e) 
              (isVal e || canStep e).

Run TemplateProgram (
  change_occurrences "typeable"
                     "typeof" "typeof_mut"
                     "typeable_mut" ;;
  change_occurrences "progressP"
                     "typeable" "typeable_mut"
                     "progressP_mut"
).

QuickChick progressP_mut.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[]
QuickChecking progressP_mut...

+ + + Passed 500 tests 
      (472 discards)
\end{lstlisting}
\end{minipage}

The result shows that QuickChick cannot find a counterexample, showing that a bug inserted into the definition of progress does not necessarily mean that \verb+progressP+ is falsified. % -- this is expected, as our progress definition does not rely on ...


\paragraph{Inductive mutations}
Here we show an example of mutations on inductive definitions; in particular, we mutate the inductive definition \lsti{has_type}, which establishes (in \lsti{Prop}) the type of an expression. The definition of \lsti{has_type} is as follows:

\begin{lstlisting}
Inductive has_type : tm -> typ -> Prop :=
    T_Tru : has_type ttrue TBool
  | T_Fls : has_type tfalse TBool
  | T_Test : forall (t1 t2 t3 : tm) (T : typ),
             has_type t1 TBool -> has_type t2 T -> has_type t3 T -> has_type (tif t1 t2 t3) T
  | T_Zro : has_type tzero TNat
  | T_Scc : forall t1 : tm, has_type t1 TNat -> has_type (tsucc t1) TNat
  | T_Prd : forall t1 : tm, has_type t1 TNat -> has_type (tpred t1) TNat
  | T_Iszro : forall t1 : tm, has_type t1 TNat -> has_type (tiszero t1) TBool
\end{lstlisting}

We create a mutant using the following MutantChick code. Here we use the operator \lsti{OnConstructors} to \emph{map} a simpler operator (\lsti{Swap} in this case) onto all the constructors of an inductive type.

\begin{lstlisting}
Run TemplateProgram (
  Mutate <% has_type %>
         using (OnConstructors 
                 (Swap <% TNat %> <% TBool %>))
         named "has_type_mut"
).

Print has_type_mut.
>>> Inductive has_type_mut : tm -> typ -> Prop :=
>>>    (...)
>>>  | T_Iszro_MUT : forall t1 : tm, 
>>>      has_type_mut t1 TBool -> has_type_mut (tiszero t1) TNat
\end{lstlisting}

We then test the resulting mutant using the progress property. Note that here we are using Quickchick inside \lsti{Prop}, something which is not always easy to do -- in this case however we have already-written generators, which greatly reduce the amount of work we need to do.

\begin{lstlisting}
QuickChick progressST_mut,
>>> QuickChecking progressST_mut...
>>> TNat
>>> Some tiszero ttrue
>>> *** Failed after 14 tests and 0 shrinks. (0 discards)
\end{lstlisting}

As we can see, QuickChick correctly spots the bug we introduced. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{List machine}\label{ch:listmachine}

The List-machine is a model for an abstract machine based on lists. It is often used as a benchmark for comparing different proof-assistants abilities to express correctness proofs.  

In the following sections we will show some mutations on it, along with some general explanations of the inner working of the machine. We will not dwelve too much in the implementations details, as we will rather focus on how some mutations can be expressed in MutantChick; for a complete description of the List Machine, we refer you to \cite{appel2006list}.

Regarding QuickChick, we will use custom generators manually written by Andrea Colo\cite{andrea}.

\paragraph{Mutations}
Following the work of Francesco Komauli (originally written in \emph{$\alpha-Check$}\cite{komauli}) and Andrea Colò \cite{andrea}, we ported some proposed mutations to MutantChick as shown in the following table:

\vspace{5mm}
\begin{tabularx}{\linewidth}{l|X|l}
 ID & Description & Operator \\
 \hline
 M1 & Fetching 2 times the same value &  Swap\\
 \hline
 M3 & \emph{stepFunc} does not update the environment & Custom\\ 
 \hline
 M6 & Wrong reorder of instruction in \emph{instr_seq} & Swap\\ 
\hline 
 M15 & Using constructor \emph{ty_listcons} instead of \emph{ty_list} & Substitute\\
\hline 
 M16 & Substitute \emph{ty_list} with its argument & Custom\\
\hline 
 M17 & Checks only the first instruction of a block instead of the whole block & Swap\\
\hline 
 M18 & Blocks are not checked at all & Substitute\\
 \hline
\end{tabularx}\label{tab:komauli}
\vspace{5mm}

%\vspace{5mm}
%\begin{tabularx}{\linewidth}{l|Xcc}
% ID & Description & Counterexample in & Operator \\
% \hline
% M1 & Fetching 2 times the same value &  & Swap\\
% \hline
% M3 & \emph{stepFunc} does not update the environment & Preservation & Custom\\ 
% \hline
% M6 & Wrong reorder of instruction in \emph{instr_seq} &  & Swap\\ 
%\hline 
% M15 & Using constructor \emph{ty_listcons} instead of \emph{ty_list} & TODO & Substitute\\
%\hline 
% M16 & Substitute \emph{ty_list} with its argument & Preservation & Custom\\
%\hline 
% M17 & Checks only the first instruction of a block instead of the whole block & TOD & Swap\\
%\hline 
% M18 & Blocks are not checked at all & TODO & Substitute\\
% \hline
%\end{tabularx}\label{tab:komauli}
%\vspace{5mm}

In the following sections we will examine three of these mutations, namely M6, M3 and M16.

\subsection{M6}
M6 (\emph{After a step the instructions in instr seq are wrongly reordered}) takes place in \lsti{stepFunc}, the functional version of \lsti{step}; we will test, on the resulting mutant, whether the Progress property is falsified or not.

This mutation can be implemented in a simple way using built-in operators - although a small observation has to be made: the swap has to be between the variables \lsti{i2} and \lsti{i3}, but internally Coq renames the variable \lsti{i3} to simply \lsti{i}. 

The original \lsti{stepFunc} is defined as follow (part of the definition is omitted for clarity):

\begin{lstlisting}[]
Definition stepFunc (env:store) (instruction:instr) (prog:program) := 
  match instruction with
  |instr_seq (instr_seq i1 i2) i3 =>
     ret (env, instr_seq i1 (instr_seq i2 i3))
  (* Mutate it into: ret (env, instr_seq i1 (instr_seq i3 i2)) *)
  (...)
\end{lstlisting}

To obtain this kind of mutation (bearing in mind Coq internal renamings), we write:

\begin{lstlisting}[]
Run TemplateProgram (
  Mutate <% Listmachine.stepFunc %>
         using (Swap "i" "i2") (* i in place of i3 *)
         named "M6"
).
\end{lstlisting}

We can see the resulting mutant, \lsti{M6}, has indeed exchanged the order of \lsti{i} and \lsti{i2}:

\begin{lstlisting}[]
fun (env : Listmachine.store) (instruction : Listmachine.instr) (prog : Listmachine.program),
  match instruction with
	(...)
  | Listmachine.instr_seq (Listmachine.instr_seq i1 i2) i =>
      Monad.ret (env, Listmachine.instr_seq i1 (Listmachine.instr_seq i i2))
\end{lstlisting}

We then proceed to test the Preservation property:

\vspace{5mm}
\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[frame=r]
Run TemplateProgram (
  Mutate <% Testing.preservationP %>
         using (Listmachine.stepFunc => M6)
         named "preservationP_M6"
    ).
QuickChick preservationP_M6.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[]
+ + + Passed 10000 tests (4959 discards)
\end{lstlisting}
\end{minipage}

As we can see, Quickchick was not able to find any counterexample for the preservation; this is consistent with Andrea's findings, as reported in \cite{andrea}.


\subsection{M3} \label{par:m3}
In M3 (\emph{stepFunc does not update the environment}) we again modify \lsti{stepFunc}; this time, however, we implement a custom operator (which could, if needed, be re-used in the future for similar situations).

The operator is defined as follows:

\vspace{5mm}
\begin{lstlisting}[caption=Custom operator for M3, label=M3Op]
Definition noUpdate : Operator :=
  UserDefined
   (fun tm => 
       match tm with
       | tApp (tConst "Maps.set" []) [_;_;_;_] => true
       | _ => false end)
   (fun tm => 
       match tm with
       | tApp (tConst "Maps.set" []) [_;env;_;_] => env
       | _ => tm end).
\end{lstlisting}

Here we use the desugared syntax \lsti{"Map.set"} for \lsti{env # v <- x}; as described in section \ref{op:userDefined}, a user-defined operator has two components, a \lsti{check: term -> bool} function (which, given an AST, returns \lsti{true} if the AST is suitable for mutations) and a \lsti{trans: term -> term} function (which describes how an AST should be mutated). 

The first function shown here returns \lsti{true} when it encounters the function \lsti{Maps.set}, while the second discards the function and only keeps its second argument, namely the original environment.

We apply this operator inside the \lsti{TemplateProgram} environment; we call the resulting mutant \lsti{M3}.

\begin{lstlisting}[caption=Applying noUpdate, label=M3Op]
Run TemplateProgram (
      Mutate <% Listmachine.stepFunc %>
             using noUpdate
             named "M3"
    ).
\end{lstlisting}

Then, we substitute occurrences of the old function \lsti{Listmachine.stepFunc} with the newly generated \lsti{M3}; finally, we test with QuickChick whether the preservation property still holds.

\vspace{5mm}
\noindent\begin{minipage}{.58\textwidth}
\begin{lstlisting}[caption=QuickChecking,frame=r]
Run TemplateProgram (
  Mutate <% Testing.preservationP %>
         using (Listmachine.stepFunc => M3)
         named "preservationP_M3"
    ).
    
QuickChick preservationP_M3.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.32\textwidth}
\begin{lstlisting}[caption=result]
*** Failed 
  after 19 tests and 0 shrinks (21 discards)
\end{lstlisting}
\end{minipage}

A counterexample is found, and hence \lsti{PreservationP_M3} is falsified. We omit the counterexample here because, as we have no shrinker, it is quite long.

We may also want to test the soundness of the resulting mutant, but to do so we need to do some bookkeping: as explained in section \ref{par:boilerplate}, the current implementation of MutantChick is not able to recursively mutate every Coq object in an environment which use a certain definition. In other words: the property \lsti{soundnessP} does not contain a direct reference to \lsti{stepFunc}, but rather \lsti{stepFunc} is indirectly invoked through a series of nested function calls. Hence, we need not only to mutate \lsti{stepFunc} and \lsti{soundnessP}, but also \lsti{steps} and \lsti{trySteps}.

To do this, we use an helper function called \lsti{change_occurrences} which substitutes \emph{every} occurrence of a named function with another one. 

\vspace{5mm}
\begin{lstlisting}[caption=Changing occurrences,label=changeOccurrences]
Run TemplateProgram (
   change_occurrences 
       "Testing.steps"  (* Where we apply the substitutions *)  
       "Listmachine.stepFunc" "M3"  (* stepFunc becomes M3 *)
       "steps_mut_M3" ;; (* The name of the resulting definition *)
                      
   change_occurrences "Testing.trySteps" 
    					  "Testing.steps" "steps_mut_M3" 
    					  "trySteps_mut_M3" ;;
    					  
   change_occurrences "Testing.soundnessP" 
   					  "Testing.trySteps" "trySteps_mut_M3" 
   					  "soundnessP_M3"
    ).
\end{lstlisting}

Finally, we can quickchick the resulting \lsti{soundnessP_M3}:

\vspace{5mm}
\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[frame=r]
QuickChick soundnessP_M3.
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{lstlisting}[]
*** Failed 
  after 1453 tests and 0 shrinks. (1949 discards)
\end{lstlisting}
\end{minipage}

Again, the property is falsified. This is consistent with Andrea's results obtained with manually-written mutations, as described in \cite{andrea}. 



\subsection{M16} \label{par:m16}
In mutation M16 we substitute the constructor \lsti{ty_list} with its argument.

This is an interesting mutation, because it is possible generalize the concept of \emph{substitution of a function with one of its argument} to every function of the form \lsti|func {A}: A -> A|.

There are two ways in which this is implementable. We first show the unsafe version, the one where we exclusively work on an AST level:

\vspace{5mm}
\begin{lstlisting}[caption=Custom operator for M16, label=rmMonotypeUnsafe]
Definition rm_monotype_function (func: term) : Operator :=
  UserDefined
    (fun tm => match tm with
               | tApp ident [_] => terms_eq ident func
               | _ => false end)
    (fun tm => match tm with
               | tApp ident [arg] =>
                 if terms_eq ident func
                 then arg
                 else tm
               | _ => tm
               end).
\end{lstlisting}

The first function \emph{checks} if an AST subtree represent a function call to our \lsti{func}, while the second \emph{trans}forms such subtree from a function call to its only argument. 

This method is clearly unsatisfactory, as no guarantee of typesafeness is enforced by Coq typechecker. For example, supposing we have a function \lsti|foo {A B}: A -> B|, we could freely create the operator \lsti{rm_monotype_function <\% foo \%>} despite the fact that the induced mutations would not even compile.

Hence, a safer solution is possible (at the cost of writing slightly more verbose operator definitions):

\vspace{5mm}
\begin{lstlisting}[caption=Monadic operator builder for M16, label=opHighOrder]
Definition rm_monotype_function_monadic {A} (f: A -> A) : TemplateMonad Operator :=
  quoted_f <- tmQuote f ;;
  ret (UserDefined
        (fun tm => match tm with
                   | tApp g [_] => terms_eq g quoted_f
                   | _ => false end)
        (fun tm => match tm with
                   | tApp g [arg] =>
                     if terms_eq g quoted_f
                     then arg
                     else tm
                   | _ => tm
                   end)).
\end{lstlisting}

In other words, we wrap the same exact custom definition of listing \ref{rmMonotypeUnsafe} inside the \lsti{TemplateMonad}; this time, however, the argument of \lsti{rm_monotype_function_monadic} is not a metaterm but a real Coq term, which is forced to respect some type constraint (in this case, it must have the form of \lsti{f: A -> A}). 

Using this monadic operator in the \verb+TemplateProgram+ environment requires a small adjustment to the usual \lsti{Mutate} syntax:

\vspace{5mm}
\begin{lstlisting}[caption=Monadic operator usage for M16]
Run TemplateProgram (
  op <- rm_monotype_function Listmachine.ty_list ;;
      
  Mutate <% Listmachine.typecheck_instr %>
         using op
         named "M16"
).
\end{lstlisting}

We need to "extract" the concrete operator from the monad (here we call it \lsti{op}); but then, it's business as usual.

Let's now try to validate the preservation property for this mutant:

\vspace{5mm}
\begin{lstlisting}[caption=Monadic operator usage for M16]
Run TemplateProgram (
  change_occurrences "Testing.updateEnv" 
                     "Listmachine.typecheck_instr" "M16" 
                     "updateEnv_mut_M16" ;;
  change_occurrences "Testing.preservationP" 
  					 "Testing.updateEnv" "updateEnv_mut_M16"
                     "preservationP_mut_M16"
).

QuickChick preservationP_mut_M16.
(* *** Failed after 310 tests and 0 shrinks. (200 discards) *)
\end{lstlisting}

As we can see, the property is falsified.

This example shows that it is sometimes possible to enforce type constraints also for custom-written generators, which we deem an important feature for MutantChick.

%end