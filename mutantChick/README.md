# MutantChick

## Installation

Required dependencies are:

+ Coq 8.9.1
+ Metacoq 1.0~alpha+8.9 [link](https://github.com/MetaCoq/metacoq#installation-instructions)
+ TypingFlags [link](https://github.com/SimonBoulier/TypingFlags)
+ QuickChick 1.1.0

After `clone`ing this repository, move into the `mutantChick/` folder (the one cointaining this readme) and launch:

	make -f CoqMakeFile
	
After that, files should be compiled and ready to be imported inside ProofGeneral or Coqide.


## Examples and documentation

See `demo.v` for some runnable examples, or `docs/demo.html` for human readable documentation.

