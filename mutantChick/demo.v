Require Import MetaCoq.Template.All.
Require Import MutantChick.Mutator.
Require Import MutantChick.Base.
Require Import MutantChick.utils.
Require Import MutantChick.InductiveTypes.

Require Import Arith String List Bool.
Require Import Coq.Unicode.Utf8.

Import ListNotations MonadNotation.
Open Scope list_scope.


(* Versione monadica alternativa *)
Definition rm_monotype_function {A: Type} (f: A -> A) :=
  quoted_f <- tmQuote f ;;
  ret (UserDefined
        (fun tm => match tm with
                   | tApp g [_] => terms_eq g quoted_f
                   | _ => false end)
        (fun tm => match tm with
                   | tApp g [arg] =>
                     if terms_eq g quoted_f
                     then arg
                     else tm
                   | _ => tm
                   end)).

Definition foo : list nat -> list nat :=
  @tl nat.
Definition bar : list nat ->  list nat :=
  @rev nat.

Definition example (x: list nat) :=
  foo (bar (x ++ [1;2])).

Run TemplateProgram (
      op1 <- rm_monotype_function foo ;;
      op2 <- rm_monotype_function bar ;;
      
      MultipleMutate <% example %>
                     using [op1; op2]
                     named "MUT0"
    ).

Print MUT0.
Print MUT1.


(* =========================================================== *)
(* =========================================================== *)

Inductive even: nat -> Prop :=
| ev_Z: even 0
| ev_SS: forall n, even n -> even (S (S n))
.

Compute <% ∀ n1 n2 n3, even n1 -> even n2 -> even n3 -> even (n1 + n2 - n3) %>.
Run TemplateProgram (
        Mutate <% ∀ n1 n2 n3, even n1 -> even n2 -> even n3 -> even (n1 + n2 - n3) %>
               using (DelImplications)
               named "ex"
      ).

Print ex.
Print ex0.
Print ex1.
Print ex2.
Print ex3.
Print ex4.
Print ex5.
Print ex6.
Print ex7.
Print ex8.
Print ex9.
Print ex10.
Print ex11.
Print ex12.

Run TemplateProgram (
        Mutate <% ∀ n1 n2, even n1 -> even n2 -> even (n1 + n2) %>
               using (DelImplications)
               named "ex"
      ).

(** * MutantChick demo 

MutantChick can be useful to mutate expressions, definition or inductives. In this file
we will show how this can be done.

*)

(** *** A brief introduction to operators

These are the operators currently implemented:

<<
Inductive Operator : Type :=
| Substitute {A} (f1 f2: A)
| IfThenElse
| DelImplications
| Swap (x y: Swappable)
| UserDefined (check: term -> bool) (trans: term -> list term)
| NewConstructor (newNameCons: ident) (type: term)
| SubConstructor (oldNameCons newNameCons : ident) (type : term)
| OnConstructor  (oldNameCons: ident) (o: Operator) 
| OnConstructors (o: Operator)

>>

  - <<Substitute f1 f2>> (aka. << ==> >>)
  
    Substitutes f1 with f2. 
  
    Note that f1 and f2 must be of the same type.
  
  
  - <<IfThenElse>>
  
    Exchange the order of then-else clauses.
  
  
  - <<DelImplications>>
  
    Removes implications. For example:
  
    << a -> b -> c >> can become << a -> c >> or << b -> c >> or << c >>.
  
  
  - <<Swap x y>>
  
    Swaps every occurrence of x with y and viceversa. 
  
    <<x>> and <<y>> can be either the name of a variable or a constant, 
    wrapped in << <% .. %> >> syntax.
  
  
  - <<UserDefined check trans>>
  
    This can be used to supply your own peculiar operator. At the moment this 
    is experimental.
  
*)

(** Other operators works only on inductive types, and will be explained later. 

The general syntax to mutate an object is as follow:

<<
Run TemplateProgram (
    Mutate <% object_to_mutate %>
           using (operator)
           named "new_name"
   ).
>>

If random choice of mutation is desired, the syntax is almost the same but with an added <<generating>> clause:

<<
Run TemplateProgram (
    Mutate <% object_to_mutate %>
           using (operator)
           named "new_name"
           generating (RandomMutations (seed) (probability))
   ).
>>

These methods will be better explained later on. 
*)

(* =========================================================== *)
(* =========================================================== *)
(** ** Mutating simple expressions *)

(** In the examples that follow, an inline expression is mutated.




This example transform multiplication with addition. Here we can see the
operator << ==> >> (aka. "frecciona") in action.

The newly generated expression is put inside a new definition, in this case
called "mut_to_add".
*)
Run TemplateProgram (
      Mutate <% 4 * 5 %>
             using (Init.Nat.mul ==> Init.Nat.add)
             named "mul_to_add"
    ).         
Print   mul_to_add.
Example ex_1: mul_to_add = 9. trivial. Qed.
Fail Print mut_to_add0. (* No other mutants generated! *)


(** This example randomly choose whether to apply a mutation
or not (this approach helps when dealing with states-explosion operators!).

<< RandomMutations seed probability >> specifies a seed with which the
PRNG is initiated and the probability of actually doing a mutation.
**Important**: both numbers must be in range [0, 255].
*)
Run TemplateProgram (
      Mutate <% firstn 2 [2;3;2] %>
             using (2 ==> 9)
             named "two_nine_rand"
             generating (RandomMutations 34 64)
    ).
Example ex_1_rand: two_nine_rand = [9; 3]. trivial. Qed.


(** Here a more complex substitution. Note that the two sides of the << ==> >> operator 
must be type-preserving! *)
Run TemplateProgram (
      Mutate <% firstn 2 [3;2;0] %>
                using (@firstn nat 2 ==> @map nat nat (fun x =>x*2))
                named "map_sub"
    ).
Example ex_2: map_sub = [6;4;0]. trivial. Qed.


(** Here we see a new operator, <<IfThenElse>>. It swaps the order of branches in a 
if-then-else clause. *)
Run TemplateProgram (
      Mutate <% if Nat.eqb 5 (3+2) then [0] else [1] %>
                using IfThenElse
                named "invert_clauses"
    ).
Example ex_3: invert_clauses = if Nat.eqb 5 (3+2) then [1] else [0]. trivial. Qed.


(** Here's another operator, <<Swap>>. 

Swap exchanges a constant (or a variable introduced by a lambda) with another
constant (or another variable introduced by a lambda).

Let's look at an example: here we will exchange the order of the two 
functions <<add>> and <<mul>>, so that we obtain an arithmetic expression
no longer equivalent with the original: *)

Run TemplateProgram (
      Mutate <% 3 + 4 * 5 %>
             using (Swap <% Init.Nat.mul %> <% Init.Nat.add %>)
             named "swap_add_mul"
    ).
Example ex_4_0: swap_add_mul = 27.
Proof. trivial. Qed.

(** Similarly, we will transform 

<< x || x && y >>

into

<< y || x && x >> *)

Definition bool_expr (x y: bool) :=
  x || x && y.

Run TemplateProgram (
      Mutate <% bool_expr %>
             using (Swap "x" "y")
             named "bool_expr_swap"
    ).
Example ex_4: ∃ x y, bool_expr x y ≠ bool_expr_swap x y.
Proof.  exists false, true; trivial. Qed.

(** *** MultipleMutate

In this example, we apply a number of operators to the same expression using the
<<MultipleMutate>> keyword.

This is similar to muCheck's << ==>* >>  *)
Run TemplateProgram (
      MultipleMutate
        <% firstn 2 [3;2;0] %>
        using [
          firstn 2 ==> (@skipn nat 1);
          firstn 2 ==> (List.map (fun x => x*2))
        ]
        named ["multMut"; "multMut"] 
    ).
Example ex_3_1: multMut  = [2;0]. trivial. Qed.
Example ex_3_2: multMut0 = [6;4;0]. trivial. Qed.


(** Another example of <<MultipleMutate>> with random choice of mutants. *)
Run TemplateProgram (
      MultipleMutate
        <% firstn 2 [3;2;0] %>
        using [
          firstn 2 ==> (@skipn nat 1);
          firstn 2 ==> (List.map (fun x => x*2))
        ]
        named ["multMut"; "multMut"] 
        generating (RandomMutations 4 45)
    ).
Print multMut2.
Fail multMut3.


(** The following will now work despite the list of operators appearing 
heterogenous: *)
Run TemplateProgram (
      MultipleMutate
        <% firstn 2 [3;2;0] %>
        using [
          (@firstn nat) ==> (@skipn nat);
          firstn 2 ==> (List.map (fun x => x*2))
        ]
        named ["multMut"; "multMut"] 
    ).



(* =========================================================== *)
(* =========================================================== *)
(** ** Mutation of definitions *)

(** In this example we mutate a <<simple>> definition. 

Note that we need to write << <% @simple %> >> instead of << <% simple %> >>, 
because otherwise MetaCoq will complain about implicit arguments. *)
Definition simple {A: Type} (n: nat) (l: list A) :=
  firstn n l.

Run TemplateProgram (
      Mutate <% @simple %>
             using (firstn ==> skipn)
             named "simple_mut"
    ).

(** Currently, the act of mutating a definition with implicit arguments will 
result in all these implicits becoming explicits; for example: *)

Check simple_mut.
(** <<
   simple_mut
     : ∀ A : Type, nat → list A → list A
>> *)

Check simple.
(** <<
simple
     : nat → list ?A → list ?A
where
?A : [ |- Type] >> *)

Example ex_5: forall A l n, simple_mut A l n  = @skipn A l n. trivial. Qed.



(* =========================================================== *)
(** *** Mutation of <<Fixpoint>>s *)

(** Again, with the same syntax we can mutate fixpoints (which are basically
definition containing a <<fix>>. *)
Fixpoint fact (n: nat) :=
  match n with
  | 0 => 1
  | S n' => (S n') * fact n'
  end.

Run TemplateProgram (
      Mutate <% fact %>
             using (Init.Nat.mul ==> Init.Nat.add)
             named "fact_mut"
    ).

Example fact_mut_neq : ∃ (n: nat),
    fact n ≠ fact_mut n.
exists 2; simpl; auto.
Qed.


(* =========================================================== *)
(** **** Changing the occurrences of a mutated definition 

Often, we have a Conjecture (or Definition, or Lemma...) <<Conj>> which depends on
some other definition <<Def>>. Were we to mutate <<Def>> into <<Def_Mut>>,
we would need to substitute every occurrence of <<Def>> with <<Def_Mut>> inside <<Conj>>. <<change_occurrences>> does this. 

Let's use this conjecture, <<simple_conj>>, as an example.
*)

Fixpoint my_list_equality (l1 l2: list nat) :=
  match l1, l2 with
  | [], [] => true
  | _, []  => false
  | [], _  => false
  | x::xs, y::ys => andb (Nat.eqb x y) (my_list_equality xs ys)
  end.

Conjecture simple_conj : forall (l: list nat),
    my_list_equality (simple 1 l) (firstn 1 l).
  
Run TemplateProgram (
      change_occurrences "simple_conj" "simple" "simple_mut"
    ).

Print ParameterEntry.

(** A more general method is of course using << ==> >>; note however that if
implicit arguments are involved, you may need to prefix the names of the
two functions with implicit arguments with <<@>>, as in this example: *)
Run TemplateProgram (
      Mutate <% simple_conj %>
             using (@simple ==> @simple_mut)
             named "simple2_mut"
    ).
Print simple2_mut.             

(** Currently, none of these two methods works for nested or recursive
name substitution (see at the bottom of this file). *)

(* =========================================================== *)
(* =========================================================== *)
(* =========================================================== *)

(** ** Mutation of Inductive types

Inductive types are ubiquitous in Coq. Here, we present some ways to mutate them. 

Syntax is basically the same as that for simple mutations; the same operators described
at the beginning of this file can be used; combined with these inductive-only operators:

<<
Inductive Operator : Type :=
| (...)
| NewConstructor (newNameCons: ident) (type: term)
| SubConstructor (oldNameCons newNameCons : ident) (type : term)
| OnConstructor  (oldNameCons: ident) (o: Operator) 
| OnConstructors (o: Operator)
>>

- <<NewConstructor newname type>>

  Adds a new constructor called <<newname>> and with type <<type>>.

- <<SubConstructor oldConst newConst type>>

  Remove the constructor oldConst and replace it with newConst with type <<type>>.

- <<OnConstructor (oldNameCons: ident) (o: Operator) >>

  Applies an operator <<o>> to the constructor named <<oldNameCons>>.

- <<OnConstructors (o: Operator)>>

  Applies an operator <<o>> to every constructor.

*)


(** #<hr># *)
(** *** Adding a new constructor to an inductive type *)
Inductive even: nat -> Prop :=
| ev_Z: even 0
| ev_SS: forall n, even n -> even (S (S n))
.

Run TemplateProgram (
      Mutate <% even %>
             using (NewConstructor "ev_double"
                      <% fun even_mut => forall n,
                             even_mut n -> even_mut (n + n) %>)
             named "even_mut"
    ).

Print even_mut.
(** This results in: 
<<
Inductive even_mut : nat → Prop :=
    ev_Z0 : even_mut 0
  | ev_SS0 : ∀ n : nat, even_mut n → even_mut (S (S n))
  | ev_double : ∀ n : nat, even_mut n → even_mut (n + n)
>> *)


(* begin hide *)
(* Proof of correctness *)
Lemma correct_even': forall n,
    even_mut 0 ∧
    (even_mut n -> even_mut (S (S n))) ∧
    (even_mut n -> even_mut (n + n)). 
Proof.
  intros.
  repeat split; intros; constructor; assumption.
Qed.  
(* end hide *)

(** *** Substitute a constructor in an inductive definition

When we substitute, we delete the specified constructor and 
replace it with a new one, defined by the user. 

Here we use the mutant inductive type generated above. *)

Print even.

Run TemplateProgram (
      Mutate <% even %>
             using (SubConstructor
                      "ev_Z"
                      "ev_PZ"
                      <% fun (even_mut: nat -> Prop) => even_mut (Nat.pred 1) %>)
             named "even_strange"
    ).
Print even_strange.

  (** We obtain this:

<<
Inductive even_strange : nat → Prop :=
    ev_Z1 : even_strange 0
 | ev_SS1 : ∀ n : nat, even_strange n → even_strange (S (S n)) 
 | ev_pred' : even_strange (Init.Nat.pred 1)
>>

It is clear that the naming convention here is... suboptimal. This area can
certantly be improved! *)


(* =========================================================== *)
(** ** Mapping on constructors 

Let's use this model of a simple programming language for our experiments:
*)

Inductive tm : Type :=
  | tru : tm
  | fls : tm
  | test : tm → tm → tm → tm
  | zro : tm
  | scc : tm → tm
  | prd : tm → tm
  | iszro : tm → tm.
 
Inductive nvalue : tm → Prop :=
  | nv_zro : nvalue zro
  | nv_scc : ∀t, nvalue t → nvalue (scc t).

Reserved Notation "t1 '-->' t2" (at level 40).
Inductive step : tm → tm → Prop :=
  | ST_TestTru : ∀t1 t2,
      (test tru t1 t2) --> t1
  | ST_TestFls : ∀t1 t2,
      (test fls t1 t2) --> t2
  | ST_Test : ∀t1 t1' t2 t3,
      t1 --> t1' →
      (test t1 t2 t3) --> (test t1' t2 t3)
  | ST_Scc : ∀t1 t1',
      t1 --> t1' →
      (scc t1) --> (scc t1')
  | ST_PrdZro :
      (prd zro) --> zro
  | ST_PrdScc : ∀t1,
      nvalue t1 →
      (prd (scc t1)) --> t1
  | ST_Prd : ∀t1 t1',
      t1 --> t1' →
      (prd t1) --> (prd t1')
  | ST_IszroZro :
      (iszro zro) --> tru
  | ST_IszroScc : ∀t1,
       nvalue t1 →
      (iszro (scc t1)) --> fls
  | ST_Iszro : ∀t1 t1',
      t1 --> t1' →
      (iszro t1) --> (iszro t1')
where "t1 '-->' t2" := (step t1 t2).


Module onConstructors.
(** First things first: let's apply a simple operators to _all_ constructors, such as
transforming every <<fls>> in <<tru>>. 

To do this, we use the operator <<OnConstructors>>, which maps an operator to
every constructors of an inductive type:
*)
Run TemplateProgram (
      Mutate <% step %>
             using (OnConstructors (fls ==> tru))
             named "step_mut"
    ).
(** <<
step_mut is defined
step_mut0 is defined 
>> *)
Fail Print step_mut1.
End onConstructors.


Module Swapping.
(** Similarly, let's mutate <<step>> by exchanging occurrences of variables
t1 and t2 inside the constructor <<ST_TestFls>>: *)
Run TemplateProgram (
      Mutate <% step %>
             using (OnConstructor "ST_TestFls" ("t1" ⇆ "t2"))
             named "step_mut"
    ).

(** First mutation is: 
<< ST_TestFls0 : ∀ t1 t2 : tm, step_mut (test fls t2 t1) t2 >>
*)
Print step_mut.
Example ex1_e5: ∀ t1 t2, step_mut (test fls t2 t1) t2. apply ST_TestFls. Qed.

(** Second mutation is: 
<< ST_TestFls1 : ∀ t1 t2 : tm, step_mut0 (test fls t2 t2) t1>>
*)
Print step_mut0.
Example ex1_e6: ∀ t1 t2, step_mut0 (test fls t2 t2) t1. apply ST_TestFls0. Qed.

Fail Print step_mut1.
End Swapping.

(** Nota: non posso mutare due volte la stessa definizione,
   perché i nomi dei costruttori si sovrappongono (per ora). Come soluzione
   temporanea, usare un modulo! *)

Module contenitore1.
  (** Here we swap a variable with a constant: *)
  Print step.
  Run TemplateProgram (
        Mutate <% step %>
               using (OnConstructor "ST_TestTru" ("t2" ⇆ <% tru %>))
               named "step_mut"
      ).
  Print step.                 
  Print step_mut.

  Lemma ex1: ∀ t1 t2 : tm, step_mut (test t2 t1 tru) t1.
  Proof. apply ST_TestTru. Qed.
End contenitore1.


Module contenitore12.
  (* Here we exchange two functions (ofc with same type!) *)
  Run TemplateProgram (
        Mutate <% step %>
               using (OnConstructor "ST_PrdScc" (<% scc %> ⇆ <% prd %>))
               named "step_mut"
      ).
  Print step_mut.

  Lemma ex1 : ∀ t1 : tm, nvalue t1 → step_mut (scc (prd t1)) t1.
  Proof. apply ST_PrdScc. Qed.
End contenitore12.



Module contenitore2.
  Run TemplateProgram (
        Mutate <% step %>
               using (OnConstructor "ST_Test" DelImplications)
               named "step_mut"
      ).
  Print step_mut.

  (**
<<
  | ST_Test : ∀ t1 t1' t2 t3 : tm, (test t1 t2 t3) --> (test t1' t2 t3)
>>
   *)
End contenitore2.

(** An example of <<DelImplications>>, which removes a premise in an
inductive constructor (if any). *)
Module contenitore3.
  Inductive tm : Type :=
  | tru : tm
  | fls : tm
  | test : tm → tm → tm → tm
  .
  
  Reserved Notation "t1 '-->' t2" (at level 40).
  Inductive step : tm → tm → Prop :=
  | ST_TestTru : ∀t1 t2,
      (test tru t1 t2) --> t1
  | ST_TestFls : ∀t1 t2,
      (test fls t1 t2) --> t2
  | ST_Test : ∀t1 t1' t2 t3,
      t1 --> t1' →
      (test t1 t2 t3) --> (test t1' t2 t3)
  where "t1 '-->' t2" := (step t1 t2).
  
  Run TemplateProgram (
        Mutate <% step %>
               using (OnConstructor "ST_Test" DelImplications)
               named "step_mut"
      ).

  Print step_mut.
  Example ex1: ∀ t1 t1' t2 t3 : tm, step_mut (test t1 t2 t3) (test t1' t2 t3).
  Proof. apply ST_Test0. Qed.
End contenitore3.

(** Another example of <<DelImplication>> where there is more than one
implication to remove; if that happens, every possible combination of
implications is generated (which can be a big number!).

Also, notice that if you remove an implication you may end up with some
variables binded by ∀ not used anymore. I'm not sure if the resulting 
mutation has any sense, but apparently it compiles and typechecks. *)
Module DelImplications2.
  Inductive step : tm → tm → Prop :=
  | ST_TestTru : ∀t1 t2,
      (test tru t1 t2) --> t1
  | ST_TestFls : ∀t1 t2,
      (test fls t1 t2) --> t2
  | ST_Test : ∀t1 t1' t2 t3,
      t1 --> t1' →
      t2 --> t3  →
      (test t1 t2 t3) --> (test t1' t2 t3)
  where "t1 '-->' t2" := (step t1 t2).
  
  Run TemplateProgram (
        Mutate <% step %>
               using (OnConstructor "ST_Test" DelImplications)
               named "ex_mut"
      ).
  Fail Print ex_mut5.

  Compute <% ∀ n1 n2, even n1 -> even n2 -> even (n1 + n2) %>.
  Run TemplateProgram (
        Mutate <% ∀ n, even 0 -> even n %>
               using (DelImplications)
               named "ex"
      ).
  Print ex.
  Print ex0.
End DelImplications2.


(* begin hide *)
(* =========================================================== *)
(* =========================================================== *)
(* =========================================================== *)
(* Da qui in poi è tutto provvisorio! *)


(* Questa cosa mi lascia ampiamente perplesso: perché 
   la proposizione con `step` è in Prop mentre quella con
   `step_mut` è in Set ??? *)
(* Check ∀ t2 t3 : tm, step_mut (test tru t2 t3) t2. (* ==> Set *) *)
(* Check ∀ t2 t3 : tm, step (test tru t2 t3) t2.      (* ==> Prop *) *)

(* (* Tra l'altro, le rappresentazioni AST sono praticamente identiche!  *)
(*    Da dove nasce allora questo disallineamento? *) *)
(* Compute <% ∀ t2 t3 : tm, step (test tru t2 t3) t2 %>. *)
(* Compute <% ∀ t2 t3 : tm, step_mut (test tru t2 t3) t2 %>. *)

(* Conjecture silly: ∀ t2 t3, step (test tru t2 t3) t2. *)

(* Fail Run TemplateProgram ( *)
(*       maybename1 <- tmAbout "step" ;; *)
(*       maybename2 <- tmAbout "step_mut'" ;; *)
(*       tmPrint maybename1 ;; *)
(*       tmPrint maybename2 ;; *)
(*       change_occurrences "silly" "step" "step_mut" *)
(*     ). *)

