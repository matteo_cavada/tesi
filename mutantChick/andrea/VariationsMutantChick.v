Require Import Arith Bool List ZArith.
Require Import String. Local Open Scope string. 
Import ListNotations.


From QuickChick Require Import QuickChick.
Set Warnings "-extraction-opaque-accessed,-extraction".
Set Warnings "-require-in-module".

(* Import Andrea *)
From T Require Import ArithmeticExpressions.
From T Require Import Progress.
Derive(Arbitrary,Show) for tm.

(* Import Matteo *)
Require Import MutantChick.Base.
Require Import MutantChick.Mutator.
Require Import MetaCoq.Template.All.

(* Will be used in a moment *)
Definition progressP e :=
    implication (typeable e) (progressConclusion e).

Print typeof.
  
Module mutate_typeof.
  Import MetaCoq.Template.monad_utils.MonadNotation.

  (* In soldoni: eqTy ==> not . eqTy *)
  Run TemplateProgram (
        Mutate <% typeof %>
               using (eqTy ==> (fun x y => negb (eqTy x y)))
               named "typeof_mut"
               generating (RandomMutations 51 50) (* Genera solo una mutazione *)
      ).
  Print typeof.
  Print typeof_mut.

  Run TemplateProgram (
        change_occurrences "typeable"
                           "typeof" "typeof_mut"
                           "typeable_mut" ;;
        change_occurrences "progressP"
                           "typeable" "typeable_mut"
                           "progressP_mut"
      ).
  Print typeable_mut.
  Print progressP_mut.
  Print progressP.
  
End mutate_typeof.
(* Non trova contresempi (ma dovrebbe? Probabilmente no!),
   just commented out to speed other checks *) 
QuickChick mutate_typeof.progressP_mut.


Module esercizio_pierce.
  Import MetaCoq.Template.monad_utils.MonadNotation.
  
    Run TemplateProgram (
        Mutate <% typeof %>
               using IfThenElse
               named "typeof_mut"
               generating (RandomMutations 51 50) (* Genera solo una mutazione *)
        ).
    Print typeof.
    Print typeof_mut.


    Run TemplateProgram (
          change_occurrences "typeable"
                             "typeof" "typeof_mut"
                             "typeable_mut" ;;
          change_occurrences "progressP"
                             "typeable" "typeable_mut"
                             "progressP_mut"
        ).

  Print progressP_mut.
End esercizio_pierce.

QuickChick esercizio_pierce.progressP_mut.

(* =========================================================== *)


Module tbool_to_tnat.
  Import MetaCoq.Template.monad_utils.MonadNotation.

  Run TemplateProgram (
        Mutate <% typeof %>
               using (TBool ==> TNat)
               named "typeof_mut_bn"
      ).

  Run TemplateProgram (
        Mutate <% typeable %> using (typeof ==> typeof_mut_bn) named "typeable_mut_bn" ;;
        Mutate <% typeable %> using (typeof ==> typeof_mut_bn0) named "typeable_mut_bn0" ;;
        Mutate <% typeable %> using (typeof ==> typeof_mut_bn1) named "typeable_mut_bn1" ;;
        Mutate <% typeable %> using (typeof ==> typeof_mut_bn2) named "typeable_mut_bn2"
      ).
     
 Definition progressP e :=
    implication (typeable e) (progressConclusion e).

 Run TemplateProgram (
        Mutate <% progressP %> using (typeable ==> typeable_mut_bn) named "progressP_mut_bn" ;;
        Mutate <% progressP %> using (typeable ==> typeable_mut_bn0) named "progressP_mut_bn0" ;;
        Mutate <% progressP %> using (typeable ==> typeable_mut_bn1) named "progressP_mut_bn1" ;;
        Mutate <% progressP %> using (typeable ==> typeable_mut_bn2) named "progressP_mut_bn2"
      ).
End tbool_to_tnat.

(* too slow *)
(* Extract Constant defNumDiscards => "(defNumTests)". *)
(* QuickChick tbool_to_tnat.progressP_mut_bn. *)
(* QuickChick tbool_to_tnat.progressP_mut_bn0. *)
(* QuickChick tbool_to_tnat.progressP_mut_bn1. (* Molti discard ma trova contresempio *) *)

(* (* Il seguente NON trova contresempi; la mutazione consiste nel *)
(* (tiszero e) che ritorna TNat invece che TBool *) *)
(* QuickChick tbool_to_tnat.progressP_mut_bn2. *)

(* =========================================================== *)


Module mut_eval1.

  Print eval1Monad.
  Run TemplateProgram (
        Mutate <% eval1Monad %>
               using (tzero ==> (tsucc tfalse))
               named "eval1Monad_mut"
               generating (RandomMutations 40 50)
      ).  
  Print eval1Monad.
  Print eval1Monad_mut.


  Run TemplateProgram (
        Mutate <% canStep %>
               using (eval1Monad ==> eval1Monad_mut)
               named "canStep_mut"
      ).
  Print canStep.
  Print canStep_mut.

  (* shortcut *)      
  Definition progressConclusion_mut e :=
    isval e || canStep_mut e.

  Definition progressP2_mut :=
    forAll arbitrary (
             fun tau =>
               forAll (gen_term tau)
                      (fun t => 
                         (progressConclusion_mut t))).
End mut_eval1.

(* Print mut_eval1.eval1Monad_mut. *)
QuickChick mut_eval1.progressP2_mut. (* No controesempi *)

(* =========================================================== *)
(* =========================================================== *)

(* Questo era il codice che mandava in crash la macchina, ora
funziona abbastanza tranquillamente! *)
Run TemplateProgram (
        Mutate <% eval1Monad %>
               using (tsucc ==> tpred)
               named "eval1Monad_mut"
    ).

Print eval1Monad.
Print eval1Monad_mut.

(* =========================================================== *)

