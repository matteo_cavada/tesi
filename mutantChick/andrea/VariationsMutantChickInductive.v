Require Import Arith Bool List ZArith.
Require Import String. Local Open Scope string. 
Import ListNotations.

From QuickChick Require Import QuickChick.
Set Warnings "-extraction-opaque-accessed,-extraction".
Set Warnings "-require-in-module".

(* Import Andrea *)
From T Require Import ArithmeticExpressions.
From T Require Import Progress.
Derive(Arbitrary,Show) for tm.

(* Import Matteo *)
Require Import MutantChick.Base.
Require Import MutantChick.InductiveTypes.
Require Import MutantChick.Mutator.
Require Import MetaCoq.Template.All.
Import MetaCoq.Template.monad_utils.MonadNotation.


Print has_type.

(* =========================================================== *)
(* ** VARIATION 1 **

 Aggiungere il costruttore SccBool:   
     | T_SccBool : forall t1,
          has_type t1 TBool ->
          has_type (tsucc t1) TBool *)
Module scc_bool.

  Run TemplateProgram (
        Mutate <% has_type %>
               using (NewConstructor "T_SccBool"
                                     <% fun has_type_mut =>
                                          forall t1, has_type_mut t1 TBool ->
                                                     has_type_mut (tsucc t1) TBool %>)
               named "has_type_mut"
      ).

  Derive ArbitrarySizedSuchThat for (fun t => has_type_mut t tau).

  (* Problema: non riesco a generare automaticamente progressST, perché?
     Probabilmente ha a che fare con i tipi impliciti e le typeclass... 
     Non escluderei però un errore dettato dalla mancanza di un qualche
     elemento sintattico in mutlisub. 
     Qui sotto il codice per investigare: *)
  (* Run TemplateProgram ( *)
  (*       change_occurrences "progressST" *)
  (*                          "has_type" "has_type_mut" *)
  (*                          "progressST_mut" *)
  (*     ). *)

  Definition progressST :=
    forAll arbitrary (fun tau : typ =>
                        forAll (genST (fun t => has_type_mut t tau))
                               (fun mt =>
                                  match mt with
                                  | Some t => progressConclusion  t
                                  | None => false
                                  end)).

End scc_bool.
QuickChick scc_bool.progressST. (* ==> CONTROESEMPIO *)


(* =========================================================== *)
(* ** VARIATION 2 ** 

Scambio TBool con TNat in T_Iszro, rendendolo:
  | T_Iszro : forall t1,
       has_type t1 TBool ->
       has_type (tiszero t1) TNat *)

Module tnat_tbool_swap.
  Require Import Unicode.Utf8.

  Run TemplateProgram (
        Mutate <% has_type %>
               using (OnConstructors (<% TNat %> ⇆ <% TBool %>))
               named "has_type_mut"
      ).

  Print has_type.
  Print has_type_mut.

  Derive ArbitrarySizedSuchThat for (fun t => has_type_mut t tau).

  Definition progressST :=
    forAll arbitrary (fun tau : typ =>
                        forAll (genST (fun t => has_type_mut t tau))
                               (fun mt =>
                                  match mt with
                                  | Some t => progressConclusion  t
                                  | None => false
                                  end)).
    
End tnat_tbool_swap.
QuickChick tnat_tbool_swap.progressST. (* ==> CONTROESEMPIO *)
(* =========================================================== *)

(* ** SANITY CHECK ** 

Se tolgo e aggiungo lo stesso costruttore, mi aspetto che
quickchick affermi che è tutto OK. È cosi? *)
Print has_type.

Module sanity_check.

  Print SubConstructor.
  Run TemplateProgram (
        Mutate <% has_type %>
               using (SubConstructor "T_Iszro" "T_Iszro"
                                     <% fun has_type_mut => forall t1,
                                            has_type_mut t1 TNat -> has_type_mut (tiszero t1) TBool %>)
               named "has_type_mut"
      ).

  Derive ArbitrarySizedSuchThat for (fun t => has_type_mut t tau).

  Definition progressST :=
    forAll arbitrary (fun tau : typ =>
                        forAll (genST (fun t => has_type_mut t tau))
                               (fun mt =>
                                  match mt with
                                  | Some t => progressConclusion  t
                                  | None => false
                                  end)).
End sanity_check.
QuickChick sanity_check.progressST. (* ==> OK *)


