Require Import Arith Bool List ZArith. Import ListNotations.
Require Import Coq.Lists.List.
From QuickChick Require Import QuickChick.
Import QcNotation. (* Suppress some annoying warnings: *)
Set Warnings "-extraction-opaque-accessed,-extraction". 
 Require Import String. Local Open Scope string. 

 Require Export ExtLib.Structures.Monads.
Import MonadNotation.
Local Open Scope monad_scope.

From T Require Import ArithmeticExpressions.
From T Require Import Progress.

Derive(Arbitrary,Show) for tm.

(*! Section VARIATIONS *)

Module VariationTypeof.

Fixpoint typeof t :=
match t with
| ttrue => ret TBool
| tfalse => ret TBool
|  tzero => ret TNat
| tsucc(ttrue) => ret TBool (* bug *)
| tsucc(tfalse) => ret TBool (*bug *)
| tsucc(t1) =>
    t' <- typeof t1 ;;
    if eqTy t' TNat then ret TNat
    else None
| tpred(t1) =>
    t' <- typeof t1 ;;
    if eqTy t' TNat then ret TNat
    else None
| tiszero(t1) =>
    t' <- typeof t1 ;;
    if eqTy t' TNat then ret TBool
    else None
| tif t1 t2 t3 =>
    t' <- typeof t1 ;;
    if eqTy t' TBool then
      t' <- typeof t2 ;;
      t'' <- typeof t3 ;;
      if eqTy t' t'' then ret t'
      else None
    else None
end.

Definition typeable e :=
  match typeof e with
  |Some _ => true
  |_ => false
  end.

Definition progressP1 e :=
   (typeable e) ==> (progressConclusion e).

End VariationTypeof.

(*! QuickChick VariationTypeof.progressP1. *)
(*
QuickChecking VariationTypeof.progressP1
tsucc tfalse
*** Failed after 13 tests and 0 shrinks. (6 discards)
*)

Module VariationSccBool.

Inductive has_type : tm -> typ -> Prop :=
  | T_Tru :
        has_type ttrue TBool
  | T_Fls :
       has_type tfalse TBool
   | T_Test : forall t1 t2 t3 T,
       has_type t1 TBool ->
       has_type t2 T ->
       has_type t3 T ->
       has_type (tif t1 t2 t3) T
  | T_Zro :
       has_type  tzero TNat
  | T_Scc : forall t1,
        has_type t1 TNat ->
        has_type (tsucc t1) TNat
  | T_SccBool : forall t1,
          has_type t1 TBool ->
          has_type (tsucc t1) TBool
  | T_Prd : forall t1,
       has_type t1 TNat ->
       has_type (tpred t1 ) TNat
  | T_Iszro : forall t1,
       has_type t1 TNat ->
       has_type (tiszero t1) TBool.

Definition has_type1 tau t := has_type t tau.

Derive ArbitrarySizedSuchThat for (fun t => has_type t tau).
(*GenSizedSuchThathas_type*)

Definition progressST :=
forAll arbitrary (fun tau : typ =>
                     forAll (genST (fun t => has_type t tau))
                            (fun mt =>
                               match mt with
                               | Some t => progressConclusion  t
                               | None => false
                               end)).

End VariationSccBool.

(*! QuickChick VariationSccBool.progressST. *)
(*QuickChecking VariationSccBool.progressST
TBool
Some tsucc ttrue
*** Failed after 11 tests and 0 shrinks. (0 discards)
*)

Module VariationPredBool.

Inductive has_type : tm -> typ -> Prop :=
  | T_Tru :
        has_type ttrue TBool
  | T_Fls :
       has_type tfalse TBool
   | T_Test : forall t1 t2 t3 T,
       has_type t1 TBool ->
       has_type t2 T ->
       has_type t3 T ->
       has_type (tif  t1 t2 t3) T 
  | T_Zro :
       has_type tzero TNat 
  | T_Scc : forall t1,
        has_type t1 TNat ->
        has_type (tsucc t1) TNat
  | T_Prd : forall t1,
       has_type t1 TNat ->
       has_type (tpred t1 ) TNat
  | T_Funny5 :
       has_type (tpred tzero) TBool
  | T_Iszro : forall t1,
       has_type t1 TNat ->
       has_type (tiszero t1) TBool.

Definition has_type1 tau t := has_type t tau.

Derive ArbitrarySizedSuchThat for (fun t => has_type t tau).

Definition preservationST :=
  forAll arbitrary (fun tau : typ =>
                     forAll (genST (fun t => has_type t tau))
                            (fun mt =>
                              match canStepOption mt with
                              |(true, Some t')=>
                                 preservation2 t' tau
                              |_ => true
                              end
                            )).

End VariationPredBool.
(*! QuickCheck VariationPredBool.preservationST. *)
(* QuickChecking VariationPredBool.preservationST
TBool
Some tif tfalse (tif ttrue (tpred tzero) tfalse) ttrue
*** Failed after 7 tests and 0 shrinks. (6 discards)
*)


(*! Section Mutants*)
(* eval1Monad - stepFM bug stepFM1 --> equivalent mutants*)
(* eval1Monad - stepFM bug stepFM2 --> equivalent mutants*)

Fixpoint stepFM t  :=
   match t with
      (*! *)
      | tif ttrue t2 t3 => ret t2
      (*!! stepFM1 *)
      (*!
      | tif ttrue t3 t2 => ret t3
      *)
      | tif tfalse t2 t3 => ret t3
      (*! *)
      | tif t1 t2 t3 =>
           t1' <- stepFM t1 ;;
           ret (tif t1' t2 t3)
      (*!! stepFM2 *)
      (*!
      | tif t1 t2 t3 =>
           t' <- stepFM t1 ;;
           ret (tif t' t2 t3)
      *)
      |tsucc(t1) =>
           t1' <- stepFM t1 ;;
           ret (tsucc(t1'))
      |tpred  tzero => ret  tzero
      |tpred(tsucc(nv1)) =>
          if (isnumericval nv1) then ret nv1
          else
            t1' <- stepFM nv1 ;;
            ret (tpred(tsucc(t1')))
      |tpred(t1) =>
           t1' <- stepFM t1 ;;
           ret (tpred(t1'))
      |tiszero  tzero => ret ttrue
      |tiszero(tsucc(nv1)) =>
          if (isnumericval nv1) then ret tfalse
          else
           t1' <- stepFM nv1 ;;
           ret (tiszero(tsucc( t1')))
      | tiszero(t1)  =>
           t1' <- stepFM t1 ;;
           ret (tiszero( t1'))
      |_ =>  None
  end.

Definition step_stepM_equivalence1 (e:tm) :=
  forAll arbitrary (
         fun tau =>
           forAll (wellTypedTermThatCanStepGen tau)
           (fun t =>
             (t<>None?)==>
              match t with
              |None => true
              |Some t' => (stepFM e = eval1Monad e)?
              end
           )).

(*! QuickCheck step_stepM_equivalence1. *)

Definition canStepM e :=
    match stepFM e with
    | Some _ => true
    | None => false
    end.

Definition wellTypedTermThatCanStepGenM T :  G (option tm) :=
  suchThatMaybe (gen_term T) canStepM.

Definition step_stepM_equivalence2 (e:tm) :=
  forAll arbitrary (
         fun tau =>
           forAll (wellTypedTermThatCanStepGenM tau)
           (fun t =>
             (t<>None?)==>
              match t with
              |None => true
              |Some t' => (stepFM e = eval1Monad e)?
              end
           )).

(*! QuickCheck step_stepM_equivalence2. *)

