Require Import Arith Bool List ZArith. Import ListNotations.
Require Import Coq.Lists.List.
From QuickChick Require Import QuickChick.
Import QcNotation. (* Suppress some annoying warnings: *)
Set Warnings "-extraction-opaque-accessed,-extraction". 
 Require Import String. Local Open Scope string. 
Import MonadNotation.

From T Require Import ArithmeticExpressions.

Extract Constant defNumTests  => "500". 

Derive(Arbitrary,Show) for tm.

(*---------------------------------------------*)
(*! Section TEST_PROGRESS_SCARTANDO_NON_TIPATI *)

Definition progressP1 e :=
   (typeable e) ==> (progressConclusion e).

(*! QuickCheck progressP1. *)
(*
QuickChecking progressP1
+++ Passed 500 tests (490 discards)
*)

(*---------------------------------------*)
(*! Section TEST_PROGRESS_CON_GENERATORI *)

Derive (Arbitrary,Show) for typ.

Module DoNotation.
Notation "'do!' X <- A ; B" :=
  (bindGen A (fun X => B))
    (at level 200, X ident, A at level 100, B at level 200).
End DoNotation.
Import DoNotation.

Fixpoint gen_term_size (n:nat) (t:typ) : G tm :=
  match n with
    | 0 =>
      match t with
      |TNat => returnGen  tzero
      |TBool => oneOf [returnGen ttrue; returnGen tfalse]
      end
    | S n' =>
        do! m <- choose (0, n');
        match t with
        | TNat =>
          oneOf [returnGen  tzero;
                 liftGen tsucc (gen_term_size (n'-m) TNat) ;
                 liftGen tpred (gen_term_size (n'-m) TNat) ;
                 liftGen3 tif (gen_term_size (n'-m) TBool)
                               (gen_term_size (n'-m) TNat)
                               (gen_term_size (n'-m) TNat)]
        | TBool =>
          oneOf [returnGen ttrue; returnGen tfalse;
                liftGen tiszero (gen_term_size (n'-m) TNat);
                liftGen3 tif (gen_term_size (n'-m) TBool)
                               (gen_term_size (n'-m) TBool)
                               (gen_term_size (n'-m) TBool)]
        end
    end.

Definition gen_term (tau : typ) :=
  sized (fun s => gen_term_size s tau ).

Fixpoint term_size (t : tm) : nat :=
  match t with
    | ttrue | tfalse |  tzero => 0
    | tsucc t' | tpred t' | tiszero t' =>
      1 + (term_size t')
    | tif t1 t2 t3 =>
      1 + (term_size t1 + term_size t2 + term_size t3)
  end.

Definition progressP2Collect :=
   forAll arbitrary (
               fun tau =>
                forAll (gen_term tau)
                (fun t =>
                  (collect (append "size " (show (term_size t)))
                (progressConclusion t)))).

(*! QuickCheck progressP2Collect. *)
(*
QuickChecking progressP2Collect
249 : "size 0"
128 : "size 1"
71 : "size 2"
26 : "size 3"
10 : "size 4"
8 : "size 5"
3 : "size 6"
2 : "size 7"
1 : "size 8"
1 : "size 14"
1 : "size 10"
+++ Passed 500 tests (0 discards)
*)

(*---------------------------------------*)
(*! Section TEST_PROGRESS_CON_HAS_TYPE   *)

Derive ArbitrarySizedSuchThat for (fun t => has_type t tau).
(*GenSizedSuchThathas_type*)

Definition progressST :=
  forAll arbitrary (fun tau : typ =>
                       forAll (genST (fun t => has_type t tau))
                              (fun mt =>
                                 match mt with
                                 | Some t => progressConclusion  t
                                 | None => false
                                 end)).
(*! QuickCheck progressST. *)
(* QuickChecking progressST
+++ Passed 500 tests (0 discards)
*)

(*-------------------------------------------------------
when a well-typed term takes a step,
the result is also a well-typed term.
*)
(*! Section TEST_PRESERVATION_ST *)

Instance eq_dec_ty (y1 y2 :  typ) : Dec (y1 = y2).
Proof. dec_eq. Defined.

Instance eq_dec_option_tm (y1 y2 : option tm) : Dec (y1 = y2).
Proof. dec_eq. Defined.

Instance eq_dec_option_tp (y1 y2 : option typ) : Dec (y1 = y2).
Proof. dec_eq. Defined.

Definition canStepOption (t:option tm) : (bool * (option tm)):=
  match t with 
  |Some t => 
      match eval1Monad t with
      |Some t' => (true,Some t')
      |None => (false,None)
      end
  |None => (false,None)
  end.

Definition preservationST :=
  forAll arbitrary (fun tau : typ =>
                     forAll (genST (fun t => has_type t tau))
                            (fun mt =>
(*for efficency use ((fst (canStepOption mt)) = true ?)==>*)
                              match canStepOption mt with
                              |(true, Some t')=>
                                 preservation2 t' tau
                              |_ => true
                              end
                            )).

(*! QuickCheck preservationST. *)
(* QuickChecking preservationST
+++ Passed 500 tests (424 discards) *)

(*! Section TEST_PRESERVATION_SUCHTHATMAYBE *)

Definition wellTypedTermThatCanStepGen T :  G (option tm) :=
  suchThatMaybe (gen_term T) canStep.

Definition someOrNone (t:option tm) :=
  match t with
  |None => "None"
  |Some _ => "Some _"
  end.


Definition preservationP2Collect :=
   forAll arbitrary (
               fun tau =>
                 forAll (wellTypedTermThatCanStepGen tau)
                 (fun t =>
                    collect (someOrNone t) (

                    match t with
                    |None => true
                    |Some t' => preservation t' tau
                    end
                    )
                 )).
(*! QuickChick preservationP2Collect. *)
(*QuickChecking preservationP2Collect
391 : "Some _"
109 : "None"
+++ Passed 500 tests (0 discards)*)


(*! Section TEST_SUBJECT_EXPANSION *)
(*t --> t' and ⊢ t' ∈ T ,then ⊢ t ∈ T*)

Definition subject_expansion t :=
  match eval1Monad t with
  |None => true (*does not happen if t canStep*)
  |Some t' =>
    let T := typeof t' in
     match T with
     |Some _ => (T = typeof t)?
     |None => true (*since the hypothesis that t'is typeable
                     is false, than the property is true*)
     end
  end.
  
(*----SUBJECT_EXPANSION SUCHTHATMAYBE----*)
Definition canStepAndResultIsTypeable e :=
    match eval1Monad e with
    | Some e' => typeable e'
    | None => false
    end.

Definition termsThatCanStepAndResultIsTypeable : G (option tm) :=
  suchThatMaybe arbitrary canStepAndResultIsTypeable.

Definition subject_expansionP1 :=
    forAllShrink  (termsThatCanStepAndResultIsTypeable)
      shrink
     (fun t =>
        collect (someOrNone t) (
        (t <> None)? ==>
        match t with
        |None => true
        |Some t' =>
          subject_expansion t'
        end
        )
     ).
QuickCheck subject_expansionP1.
(*QuickChecking subject_expansionP1
tif (tsucc tfalse) (tiszero tfalse) tzero
Some tif tfalse (tpred tfalse) ttrue
18 : (Discarded) "None"
1 : "Some _"
*** Failed after 2 tests and 1 shrinks. (18 discards)
*)

(*----SUBJECT_EXPANSION_Derive ArbitrarySizedSuchThat----*)
Instance dec_nvalue t : Dec (nvalue t).
Proof.
dec_eq.
induction t; 
try solve[right => contra; inversion contra].
- left; constructor.
- destruct IHt as [L | R]; [left; constructor | right => contra; inversion contra]; eauto.
Defined.

Derive ArbitrarySizedSuchThat for (fun t1 : tm => nvalue t1).

Derive ArbitrarySizedSuchThat for (fun t => step t t1).

(* NO SHRINK *)
Definition subject_expansionP  :=
  forAll arbitrary  (fun t1 : tm =>
      forAll (genST (fun t => step t t1))(
          fun t' => 
               match t' with
               |Some t'' => subject_expansion t''
               |_ => true 
               end
       )).
(*! QuickCheck subject_expansionP. *)
(* QuickChecking subject_expansionP
tzero
Some tif tfalse (tif (tiszero (tiszero tfalse)) (tif (tif ttrue (tpred ttrue) (tsucc ttrue)) (tsucc ttrue) tfalse) (tiszero ttrue)) tzero
*** Failed after 5 tests and 0 shrinks. (0 discards)
 *)

(**)
(* Using PBT to detect non-equivalent mutants
verbose encoding w/out QC sections *)

Fixpoint stepFM1 t  :=
   match t with
 (*bug *)| tif ttrue t2 t3 => ret t3
      | tif tfalse t2 t3 => ret t3
      | tif t1 t2 t3 =>
           t1' <- stepFM1 t1 ;;
           ret (tif t1' t2 t3)
      |tsucc(t1) =>
           t1' <- stepFM1 t1 ;;
           ret (tsucc(t1'))
      |tpred  tzero => ret  tzero
      |tpred(tsucc(nv1)) =>
          if (isnumericval nv1) then ret nv1
          else
            t1' <- stepFM1 nv1 ;;
            ret (tpred(tsucc(t1')))
      |tpred(t1) =>
           t1' <- stepFM1 t1 ;;
           ret (tpred(t1'))
      |tiszero  tzero => ret ttrue
      |tiszero(tsucc(nv1)) =>
          if (isnumericval nv1) then ret tfalse
          else
           t1' <- stepFM1 nv1 ;;
           ret (tiszero(tsucc( t1')))
      | tiszero(t1)  =>
           t1' <- stepFM1 t1 ;;
           ret (tiszero( t1'))
      |_ =>  None
  end.



Fixpoint stepFM t  :=
   match t with
      | tif ttrue t2 t3 => ret t2
      
      | tif tfalse t2 t3 => ret t3
     
      | tif t1 t2 t3 =>
           t2' <- stepFM t2 ;; (* bug *)
           ret (tif t1 t2' t3)
      |tsucc(t1) =>
           t1' <- stepFM t1 ;;
           ret (tsucc(t1'))
      |tpred  tzero => ret  tzero
      |tpred(tsucc(nv1)) =>
          if (isnumericval nv1) then ret nv1
          else
            t1' <- stepFM nv1 ;;
            ret (tpred(tsucc(t1')))
      |tpred(t1) =>
           t1' <- stepFM t1 ;;
           ret (tpred(t1'))
      |tiszero  tzero => ret ttrue
      |tiszero(tsucc(nv1)) =>
          if (isnumericval nv1) then ret tfalse
          else
           t1' <- stepFM nv1 ;;
           ret (tiszero(tsucc( t1')))
      | tiszero(t1)  =>
           t1' <- stepFM t1 ;;
           ret (tiszero( t1'))
      |_ =>  None
  end.

Definition step_stepM_equivalence1 (e:tm) :=
  forAll arbitrary (
         fun tau =>
         forAll (gen_term tau)
           (fun t =>
             
              (stepFM1 t = eval1Monad t)?
              
           )).

QuickCheck step_stepM_equivalence1.

(* untyped *)
Definition step_stepM_equivalence1u (e:tm) :=
  forAll arbitrary (
         fun t =>
             
              (stepFM1 t = eval1Monad t)?
              
           ).

QuickCheck step_stepM_equivalence1u.

Definition step_stepM_equivalence (e:tm) :=
  forAll arbitrary (
         fun tau =>
         forAll (gen_term tau)
           (fun t =>
             
              (stepFM t = eval1Monad t)?
              
           )).

(* QuickCheck step_stepM_equivalence. *)






















