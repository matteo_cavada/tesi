Require Import Arith Bool List ZArith.
Require Export ExtLib.Structures.Monads.
Import MonadNotation.
(* Local Open Scope monad_scope. *)
From QuickChick Require Import QuickChick.


Inductive tm : Type :=
  | ttrue : tm
  | tfalse : tm
  | tif : tm -> tm -> tm -> tm
  | tzero : tm
  | tsucc : tm -> tm
  | tpred : tm -> tm
  | tiszero : tm -> tm.

Inductive typ : Type :=
  |TBool : typ
  |TNat : typ.

Fixpoint isnumericval t :=
    match t with
         tzero => true
        | tsucc(t1) => isnumericval t1
        | _ => false
    end.

Fixpoint isval t :=
    match t with
        | ttrue  => true
        | tfalse => true
        | _ => if isnumericval t then true else false
    end.


Instance optionMonad : Monad option :=
  {
    ret T x :=
      Some x ;
    bind T U m f :=
      match m with
        None => None
      | Some x => f x
      end
  }.

(* rewritten to help mutation*)
Definition eval1Monad := 
fix eval1Monad (t : tm) : option tm :=
  match t with
  | tif ttrue t2 _ => ret t2
  | tif tfalse _ t3 => ret t3
  | tif (tif _ _ _ as t1) t2 t3 | tif 
    (tzero as t1) t2 t3 | tif (tsucc _ as t1) t2 t3 |
    tif (tpred _ as t1) t2 t3 | tif 
    (tiszero _ as t1) t2 t3 =>
      t1' <- eval1Monad t1;; ret (tif t1' t2 t3)
  | ttrue | tfalse | tzero => None
  | tsucc t1 => t1' <- eval1Monad t1;; ret (tsucc t1')
  | tpred tzero => ret tzero
  | tpred (tsucc nv1) =>
      if isnumericval nv1
      then ret nv1
      else
       t1' <- eval1Monad nv1;; ret (tpred (tsucc t1'))
  | tpred (ttrue as t1) | tpred (tfalse as t1) | tpred
    (tif _ _ _ as t1) | tpred (tpred _ as t1) | tpred
    (tiszero _ as t1) =>
      t1' <- eval1Monad t1;; ret (tpred t1')
  | tiszero tzero => ret ttrue
  | tiszero (tsucc nv1) =>
      if isnumericval nv1
      then ret tfalse
      else
       t1' <- eval1Monad nv1;;
       ret (tiszero (tsucc t1'))
  | tiszero(t1)  => t1' <- eval1Monad t1;; ret (tiszero t1')
  end.
(*
Fixpoint eval1Monad t  :=
   match t with
      | tif ttrue t2 t3 => ret t2
      | tif tfalse t2 t3 => ret t3
      | tif t1 t2 t3 =>
           t1' <- eval1Monad t1 ;;
           ret (tif t1' t2 t3)
      |tsucc(t1) =>
           t1' <- eval1Monad t1 ;;
           ret (tsucc(t1'))
      |tpred  tzero => ret  tzero
      |tpred(tsucc(nv1)) =>
          if (isnumericval nv1) then ret nv1
          else
            t1' <- eval1Monad nv1 ;;
            ret (tpred(tsucc(t1')))
      |tpred(t1) =>
           t1' <- eval1Monad t1 ;;
           ret (tpred(t1'))
      |tiszero  tzero => ret ttrue
      |tiszero(tsucc(nv1)) =>
          if (isnumericval nv1) then ret tfalse
          else
           t1' <- eval1Monad nv1 ;;
           ret (tiszero(tsucc( t1')))
      | tiszero(t1)  =>
           t1' <- eval1Monad t1 ;;
           ret (tiszero( t1'))
      |_ =>  None
  end.
*)
Definition canStep e :=
    match eval1Monad e with
    | Some _ => true
    | None => false
    end.

Definition eqTy t1 t2 :=
  match t1,t2 with
  |TBool,TBool => true
  |TNat,TNat => true
  |_,_ => false
  end.

Fixpoint typeof t :=
  match t with
  | ttrue => ret TBool
  | tfalse => ret TBool
  |  tzero => ret TNat
  | tsucc(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TNat
      else None
  | tpred(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TNat
      else None
  | tiszero(t1) =>
      t' <- typeof t1 ;;
      if eqTy t' TNat then ret TBool
      else None
  | tif t1 t2 t3 =>
      t' <- typeof t1 ;;
      if eqTy t' TBool then
        t' <- typeof t2 ;;
        t'' <- typeof t3 ;;
        if eqTy t' t'' then ret t'
        else None
      else None
  end.

  
Definition typeable e :=
    match typeof e with
    |Some _ => true
    |_ => false
    end.

Definition progressConclusion e :=
   isval e || canStep e.


Instance eq_dec_ty (y1 y2 :  typ) : Dec (y1 = y2).
Proof. dec_eq. Defined.

Definition preservation t tau :=
   match eval1Monad t with
   |None => true 
   |Some t' =>
     match typeof t' with
     |Some tt' => (tt' = tau)?
     |None => false
     end
    end.

Definition preservation2 t' tau :=
    match typeof t' with
    |Some tt' => (tt' = tau ?)
    |None => false
    end.


(*--------------------------------------------*)

Inductive has_type : tm -> typ -> Prop :=
  | T_Tru :
        has_type ttrue TBool
  | T_Fls :
       has_type tfalse TBool
   | T_Test : forall t1 t2 t3 T,
       has_type t1 TBool ->
       has_type t2 T ->
       has_type t3 T ->
       has_type (tif  t1 t2 t3) T 
  | T_Zro :
       has_type tzero TNat
  | T_Scc : forall t1,
        has_type t1 TNat ->
        has_type (tsucc t1) TNat
  | T_Prd : forall t1,
       has_type t1 TNat ->
       has_type (tpred t1 ) TNat
  | T_Iszro : forall t1,
       has_type t1 TNat ->
       has_type (tiszero t1) TBool.

Definition has_type1 tau t := has_type t tau.

Inductive nvalue : tm -> Prop :=
  | nv_zero : nvalue tzero
  | nv_succ : forall t, nvalue t -> nvalue (tsucc t).


Reserved Notation "t1 '===>' t2" (at level 40).


Inductive step : tm -> tm -> Prop :=
  | ST_IfTrue : forall t1 t2,
      (tif ttrue t1 t2) ===> t1
  | ST_IfFalse : forall t1 t2,
      (tif tfalse t1 t2) ===> t2
  | ST_If : forall t1 t1' t2 t3,
      t1 ===> t1' ->
      (tif t1 t2 t3) ===> (tif t1' t2 t3)
  | ST_Succ : forall t1 t1',
      t1 ===> t1' ->
      (tsucc t1) ===> (tsucc t1')
  | ST_PredZero :
      (tpred tzero) ===> tzero
  | ST_PredSucc : forall t1,
      nvalue t1 ->
      (tpred (tsucc t1)) ===> t1
  | ST_Pred : forall t1 t1',
      t1 ===> t1' ->
      (tpred t1) ===> (tpred t1')
  | ST_IszeroZero :
      (tiszero tzero) ===> ttrue
  | ST_IszeroSucc : forall t1,
       nvalue t1 ->
      (tiszero (tsucc t1)) ===> tfalse
  | ST_Iszero : forall t1 t1',
      t1 ===> t1' ->
      (tiszero t1) ===> (tiszero t1')

where "t1 '===>' t2" := (step t1 t2).

Hint Constructors step.





