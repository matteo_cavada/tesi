(* Pseudo Random Number Generator *)

Require Import List Arith String Bool Nat Program.
Import ListNotations.
From Coq Require Recdef. (* Needed for Function *)

(* A number is represented in binary notation using a list of bool.
   This function returns a new pseudo-random number given a seed. 
*)

(* Reverse the list before calling this *)
Fixpoint toNat (l: list bool) :=
  match l with
  | [] => 0
  | [x] => if x then 1 else 0
  | x::xs => (if x then 1 else 0) + 2 * (toNat xs)
  end.



(* Helper function, converts a nat into a list bool *)
Function fromNatRec (n: nat) {measure S n} : list bool :=
  match n with
  | 0  => [false]
  | 1  => [true]
  | _ => (negb (eqb (n mod 2) 0)) :: (fromNatRec ((div n 2)))
  end.
Proof.
  intros.
  apply lt_n_S.
  apply Nat.div_lt.
  - apply Nat.lt_0_succ.
  - auto.
Defined.

Definition fromNat (n: nat) : list bool :=
  let listBool := fromNatRec n in
  if List.length listBool <? 7
  then listBool ++ (map (fun _ => false) (seq 0 (7 - List.length listBool)))
  else listBool.


(* :-) *)
(* From QuickChick Require Import QuickChick. *)
(* QuickChick (forAll (choose (0, 256)) (fun x => toNat (fromNat x) =? x)) . *)


(* =========================================================== *)

(* Generatore vero e proprio di numeri casuali *)
Definition genNew (l: list bool) :=
  let newBit := (xorb (xorb (nth 0 l false) (nth 1 l true)) (nth 6 l false)) in
  newBit :: (firstn 6 l).

Definition genNewN (seed: nat) : nat :=
  toNat (rev (genNew (fromNat seed))).

Fixpoint generate (l: list bool) (n: nat) : list (list bool) :=
  match n with
  | 0 => []
  | S n' => (rev (genNew l)) :: generate (genNew l) n'
  end.

Definition generateN (l: list bool) (n: nat) : list nat :=
  map toNat (generate l n).


Definition generateStates (seed n: nat) (threshold: nat) : list (option nat) :=
  let states := generate (fromNat seed) n in
  map (fun s => if (toNat s) <? threshold then Some (toNat s) else None) states.

Definition generateState (seed threshold: nat) : option nat :=
  match generateStates seed 1 threshold with
  | [] => None (* Non può mai accadere; forzarlo a livello di typesystem? *)
  | x::_ => x
  end.

Definition generateBooleans (seed n: nat) (threshold: nat) : list bool :=
  map (fun x => match x with | None => false | Some _ => true end)
      (generateStates seed n threshold).
(* QuickChick (forAll (choose (0, 256)) *)
(*                    (fun x => Nat.eqb (List.length (filter *)
(*                                             (fun t => match t with *)
(*                                                       | None => true *)
(*                                                       | _ => false *)
(*                                                       end) *)
(*                                             (generateStates x 20 999))) 0)). *)


Check List.fold_left.

Fixpoint memory_map {A T1 T2}
         (f: T1 -> T2) (f_acc: A -> A) (acc: A) (l: list T1) : list T2 :=
  match l with
  | [] => []
  | x::xs => f x :: (memory_map f f_acc (f_acc acc) xs)
  end.

