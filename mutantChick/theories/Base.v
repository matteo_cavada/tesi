(* Esperimenti con Coq e MetaCoq *)
Require Import List Arith String Bool Nat.
Require Import MetaCoq.Template.All.
Require Import Unicode.Utf8.

Import ListNotations MonadNotation.
Local Open Scope string_scope.
Local Open Scope list_scope.

Require Import MutantChick.utils.


(* =========================================================== *)
(* =========================================================== *)
(* Sostituzione vera e propria *)

(* Applicazione di funzioni su termini di un albero sintattico. 
   Tiene conto della possibilità che, nell'albero che si vuole
   mutare, sia possibile effettuare più mutazioni contemporaneamente.
   Questa funzione ritorna tanti alberi sinattici quante sono le
   possibili mutazioni.
 *)

Require Import MutantChick.PRNG.
From Coq Require Import Program Arith.

From TypingFlags Require Import Loader.
Unset Guard Checking.

Inductive MutationNumber : nat -> nat -> Type :=
| AllMutations: MutationNumber 0 999 
| RandomMutations (seed threshold: nat) : MutationNumber seed threshold
| MaxNMutations (n seed threshold: nat) : MutationNumber seed threshold
.

Compute <% let x := 0 in 1 + x %>.
Fixpoint genericFind
         (check : term -> bool)
         (idxs: list nat) (tm: term) {struct tm}
  : list (list nat) :=

  let if_found := if check tm
                  then [idxs]
                  else [] in
  
  let recurse :=       
      match tm with
      | tApp
          (tConstruct {| inductive_mind := "Coq.Init.Datatypes.nat"; inductive_ind := 0 |} 1 [])
           rest => []
      | tProd _ argSx argDx =>
        (genericFind check (idxs ++ [2]) argSx) ++
        (genericFind check (idxs ++ [3]) argDx)                                         
      | tApp type args =>
        (genericFind check (idxs ++ [1]) type) ++
        (List.concat (mapi (fun i a => genericFind check (idxs ++ [2; (i+1)]) a) args))
      | tLambda _ type body =>
        (genericFind check (idxs ++ [2]) type) ++
        (genericFind check (idxs ++ [3]) body)

      | tCase typ obj out casi =>
        (genericFind check (idxs ++ [2]) obj) ++
        (genericFind check (idxs ++ [3]) out) ++
        (List.concat (mapi (fun i '(nargs, caso) =>
                              genericFind check (idxs ++ [4; (i+1); 1]) caso) casi))
        
      | tFix [x] _ =>
        (genericFind check (idxs ++ [1; 1; 1]) (x.(dtype))) ++
        (genericFind check (idxs ++ [1; 1; 2]) (x.(dbody)))

      | tLetIn _ tmDef _ body =>
        (genericFind check (idxs ++ [2]) tmDef) ++
        (genericFind check (idxs ++ [4]) body)
                                                
      | _ => []
      end in

  if_found ++ recurse.

 
(* 
   Applica una funzione all'i-esimo elemento di una lista. 
   Se ∄ l'i-esimo elemento, non fare nulla.
*)
Fixpoint change_nth {A} (i: nat) (l: list A) (f: A -> A) : list A :=
  match i, l with
  | 0, [] => []
  | 0, x::xs => f x :: xs
  | S i', [] => []
  | S i', x::xs => x :: change_nth i' xs f
  end.

Inductive ASTERROR := AstError.

(* 
   Data una coordinata, sostituisce il termine in _quella_ coordinata
   con il termine `new`.
 *)
Fixpoint subAt
         (trans : term -> term)
         (idx: list nat) (tm: term) {struct tm}
  : term :=
  match idx, tm with
  | [], _ => trans tm
                  
  | 2::idxs, tProd n argSx argDx =>
    tProd n (subAt trans idxs argSx) argDx
  | 3::idxs, tProd n argSx argDx =>
    tProd n argSx (subAt trans idxs argDx)
  | 1::idxs, tApp type body =>
    tApp (subAt trans idxs type) body
  | 2::i::idxs, tApp type args =>
    match List.nth_error args (i-1) with
    | None   => tRel 999
    | Some t => tApp type (change_nth (i-1) args (fun _ => subAt trans idxs t))
    end
  | 2::idxs, tLambda n type body =>
    tLambda n (subAt trans idxs type) body
  | 3::idxs, tLambda n type body =>
    tLambda n type (subAt trans idxs body)
        
  | 2::idxs, tCase typ obj out casi =>
    tCase typ (subAt trans idxs obj) out casi
  | 3::idxs, tCase typ obj out casi =>
    tCase typ obj (subAt trans idxs out) casi
  | 4::i::1::idxs, tCase typ obj out casi =>
    match List.nth_error casi (i-1) with
    | None   => tRel 999
    | Some t => tCase typ obj out
                      (change_nth (i-1) casi
                                  (fun '(nargs, caso) => (nargs, subAt trans idxs caso)))
    end
      
  | 1::1::1::idxs, tFix [x] ind =>
    tFix [map_def (subAt trans idxs) id x] ind
  | 1::1::2::idxs, tFix [x] ind =>
    tFix [map_def id (subAt trans idxs) x] ind

  | 2::idxs, tLetIn name tmDef type body =>
    tLetIn name (subAt trans idxs tmDef) type body
  | 4::idxs, tLetIn name tmDef type body =>
    tLetIn name tmDef type (subAt trans idxs body)

  | _, _ => <% AstError %>
  end.
Set Guard Checking.




Fixpoint keep_choosen (idxs: list (list nat)) (bools: list bool) : list (list nat) :=
  match idxs, bools with
  | [], _ => []
  | _::_, [] => [] (* Non dovrebbe mai succedere *)
  | x::xs, b::bs => if b then x :: (keep_choosen xs bs)
                    else (keep_choosen xs bs)
  end.

Fixpoint multisub (check: term -> bool) (trans: term -> term)
         (seed prob maxNumMutations: nat) (tm: term) {struct tm} : list term :=

  let idxs := genericFind check [] tm in
  let randomNumbers := generateBooleans seed (List.length idxs) prob in
                 
  map (fun idx => subAt trans idx tm) (keep_choosen idxs randomNumbers).
