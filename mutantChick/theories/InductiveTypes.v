Require Import MetaCoq.Template.All.
Require Import List String.
Import ListNotations MonadNotation.
Require Import Unicode.Utf8.
Require Import MutantChick.utils.
Require Import MutantChick.Base.


(* **************************************************** *)
(* The two definition, add_ctor and add_constructor     *)
(* were originally declared as examples in the Metacoq  *)
(* repository. Here is the original file:               *)
(*https://github.com/MetaCoq/metacoq/blob/7647d8cfdf28f633a0495acec6c4c76cc4852824/examples/add_constructor.v*)
(* **************************************************** *)


(* ** Public API

This documentation file shows some important functions 
 *) 

Class TslIdent := { tsl_ident : ident -> ident }.

Instance prime_tsl_ident : TslIdent
  := {| tsl_ident := fun id => (id ++ "'")%string |}.

Fixpoint try_remove_n_lambdas (n : nat) (t : term) {struct n} : term :=
  match n, t with
  | 0, _ => t
  | S n, tLambda _ _ t => try_remove_n_lambdas n t
  | S n, _ => t
  end.


Definition get_constructors_names_quoted (decl: mutual_inductive_body) :=
  match (ind_bodies decl) with
  | body::_ => ret (map (fun t => fst (fst t)) (ind_ctors body))
  | _ => ret []
  end.
Definition get_constructors_names (tm : Ast.term) : TemplateMonad (list ident) :=
  match tm with
  | tInd ind0 _ =>
    decl <- tmQuoteInductive (inductive_mind ind0) ;;
    get_constructors_names_quoted decl
  | _ => ret []
  end.



(** [add_ctor] adds a constructor to a [mutual_inductive_body]
 (that is a reified declaration of an inductive). *)
Polymorphic Definition add_ctor (mind : mutual_inductive_body)
            (ind0 : inductive) (newName idc : ident) (newNames : list ident) (ctor : term)
  : mutual_inductive_body
  := let i0 := inductive_ind ind0 in
     {| ind_finite := mind.(ind_finite);
        ind_npars := mind.(ind_npars) ;
        ind_universes := mind.(ind_universes) ;
        ind_params := mind.(ind_params);
        ind_bodies :=
          mapi (fun (i : nat) (ind : one_inductive_body) =>
                  {| ind_name := newName ;
                     ind_type  := ind.(ind_type) ;
                     ind_kelim := ind.(ind_kelim) ;
                     ind_ctors :=
                       let ctors :=
                           map (fun '(id, t, k, new) => (new, t, k))
                               (combine ind.(ind_ctors) newNames)
                       in
                       if Nat.eqb i i0 then
                         let n := #|ind_bodies mind| in
                         let typ := try_remove_n_lambdas n ctor in
                         ctors ++ [(idc, typ, 0)]  (* fixme 0 *)
                       else ctors;
                     ind_projs := ind.(ind_projs) |})
               mind.(ind_bodies)
     |}.

Fixpoint update_names (names: list ident) :=
  monad_map (fun n => tmFreshName n) names.

(** [add_constructor] is the user-interface for adding a constructor. *)
Polymorphic Definition add_constructor (tm : Ast.term)
            (newName idc : ident) (type : Ast.term)
  : TemplateMonad unit
  := match tm with
     | tInd ind0 _ =>
       decl <- tmQuoteInductive (inductive_mind ind0) ;;

       newNamesRaw <- get_constructors_names tm ;;
       newNames <- tmEval lazy newNamesRaw ;;
       updateNamesRaw <- update_names newNames ;;
       updateNames <- tmEval lazy updateNamesRaw ;;

       let ind' := add_ctor decl ind0 newName idc updateNames type in
       tmMkInductive' ind'
                      
     | _ => tmPrint tm ;; tmFail " is not an inductive"
     end.


(** =========================================================== *)

(** [add_constructor] is the user-interface for adding a constructor. *)
Polymorphic Definition delete_ctor (mind: mutual_inductive_body) (idc : ident)
            (shift: bool)
  : mutual_inductive_body
  := {|
      ind_finite := mind.(ind_finite);
      ind_npars  := mind.(ind_npars) ;
      ind_universes := mind.(ind_universes) ;
      ind_params := mind.(ind_params);
      ind_bodies :=
        map (fun (ind : one_inductive_body) =>
               {|
                 ind_name  := if shift then tsl_ident (ind.(ind_name)) else (ind.(ind_name)) ;
                 ind_type  := ind.(ind_type) ;
                 ind_kelim := ind.(ind_kelim) ;
                 ind_projs := ind.(ind_projs) ; 
                 ind_ctors :=
                   map (fun '(id, t, k) => (if shift then tsl_ident id else id, t, k))
                       (filter (fun '(id , t , k) => negb (id =? idc)) ind.(ind_ctors))
               |}) 
            mind.(ind_bodies)
    |}.


(* =========================================================== *)

(** Substitute a constructor with a new version of it. *)
Polymorphic Definition substitute_constructor (tm : Ast.term)
            (newNameInd oldNameCons newNameCons : ident) (type : Ast.term)
  : TemplateMonad unit
  := match tm with
     | tInd nomeIndOriginale _ =>
         decl <- tmQuoteInductive (inductive_mind nomeIndOriginale) ;;

         newNameCons' <- tmFreshName newNameCons ;;
         newNameInd'  <- tmFreshName newNameInd ;;
         
         let deletedInd := delete_ctor decl oldNameCons false in

         new_names_raw <- get_constructors_names_quoted deletedInd ;;
         new_names     <- tmEval all new_names_raw ;;
         updateNamesRaw <- update_names new_names ;;
         updateNames    <- tmEval all updateNamesRaw ;;

         let newInd     := add_ctor
                             deletedInd
                             nomeIndOriginale
                             newNameInd' newNameCons' updateNames
                             type in
         tmMkInductive' newInd
                        
     | _ => tmPrint tm ;; tmFail " is not an inductive"
     end.

(* =========================================================== *)
(* Creare multipli tipi induttivi. Questa sezione è necessaria per
   evitare di incorrere in errori tipo "XXX already defined" ecc. 
   Sembra complicata, ma per lo più si tratta di plumbing. *)


(* 
   Ricorsivamente:
   - elimina il costruttore chiamato `oldNameCons` dai 
     costruttori del tipo induttivo `decl`;
   - aggiunge un nuovo costruttore, `newNameCons`, che 
     ha come corpo il primo elemento della lista `types`;
   - chiama ricorsivamente sè stessa con i nuovi nomi
 *)

Polymorphic Fixpoint substitute_constructor_multiple_times_Rec
            originalInd0   decl
            (newNameInd oldNameCons newNameCons : ident)
            (constr_names: list ident)
            (types : list Ast.term)
  : TemplateMonad unit :=
  match types with
  | type::xs =>
    newNameCons' <- tmFreshName newNameCons ;;
    newNameInd'  <- tmFreshName newNameInd ;;
    
    (* tmPrint newNameCons' ;; *)
    (* tmPrint newNameInd' ;; *)
    (* let deletedInd := delete_ctor decl oldNameCons false in *)

    new_constr_names_raw <- update_names constr_names ;;
    new_constr_names     <- tmEval lazy new_constr_names_raw ;;

    let newInd := add_ctor
                    decl
                    originalInd0
                    newNameInd' newNameCons' new_constr_names
                    type in
    tmMkInductive' newInd ;;

    (* substitute_constructor_multiple_times_Rec originalInd0 newInd *)
    substitute_constructor_multiple_times_Rec originalInd0 decl (* decl? *)
                                          newNameInd'
                                          newNameCons'
                                          newNameCons'
                                          new_constr_names
                                          xs
  | [] => tmPrint "Ok!"
  end.                     


Polymorphic Definition substitute_constructor_multiple_times
            (tmOriginale : Ast.term)
            (newNameInd oldNameCons newNameCons : ident)
            (types : list Ast.term)
  : TemplateMonad unit :=
  match tmOriginale with
  | tInd nomeIndOriginale _ =>
    decl <- tmQuoteInductive (inductive_mind nomeIndOriginale) ;;

    let deletedInd := delete_ctor decl oldNameCons false in
    
    new_constr_names_raw <- get_constructors_names_quoted deletedInd ;;
    new_constr_names     <- tmEval lazy new_constr_names_raw ;;
    
    substitute_constructor_multiple_times_Rec
          nomeIndOriginale deletedInd            (* a cosa serve nomeIndOriginale? *)
          newNameInd oldNameCons newNameCons
          new_constr_names
          types
  | _ => tmPrint tmOriginale ;; tmFail " is not an inductive"
  end.




(* =========================================================== *)
(* =========================================================== *)
(* Implementazione di un operatore di SWAP, che prende un 
   costruttore di un tipo induttivo e scambia l'ordine di due
   variabili scelte dall'utente. *)


(*
  Ritorna una lista contenente tutte le variabili (e relativo
  tipo) che appaiono in un termine. 
*)
Fixpoint prod_vars (tm: term) : list (string × term) :=
  match tm with
  | tProd (nNamed name) type body =>
    (name, type) :: prod_vars body
  | tApp _ body => List.concat (map prod_vars body)
  | _ => []
  end.
Definition prod_vars_monad (tm: term) :=
  ret (rev (prod_vars tm)).


(* 
   Ritorna il corpo del costruttore chiamato `idc`.
 *)
Definition get_body_constructor (tm: term) (idc : ident) : TemplateMonad term :=
   match tm with
   | tInd ind0 _ =>
     decl <- tmQuoteInductive (inductive_mind ind0) ;;
     match ind_bodies decl with
     | [] => tmFail "Lista vuota? Errore imprevisto!"
     | x::_ =>
       match List.find (fun '(name, b, nargs) => name =? idc) (ind_ctors x) with
       | None => tmFail ("Costruttore non trovato con il nome: " ++ idc)
       | Some (_, b, _) => ret b
       end
     end
   | _ => tmFail "Not a inductive term"
   end.



From TypingFlags Require Import Loader.
Unset Guard Checking. (* Disabilito termination checker *)

(*
  Ritorna una lista di "coordinate" che identificano, all'interno di
  `tm`, le posizioni delle variabili chiamate `name`. 

  (questa è una funzione helper, dovrai invocare la funzione più sotto)   
*)
Fixpoint lambda_var_occur_Rec (name: string) (tm: term)
         (lvl: option nat) (idxs: list nat) {struct tm}
  : list (list nat) :=
  
  match tm with
  | tRel n => match lvl with
      | Some m => if Nat.eqb n m
                  then [idxs]
                  else []
      | None   => []
      end
                
  | tProd (nNamed nm) type body =>
    if nm =? name
    then lambda_var_occur_Rec name body (Some 0) (idxs ++ [3])
    else lambda_var_occur_Rec name body (lvl >>= (fun x => Some (x+1))) (idxs ++ [3])

  | tLetIn (nNamed nm) tmDef type body => (* e se una lambda apparisse in tmDef? *)
    if nm =? name
    then lambda_var_occur_Rec name body (Some 0) (idxs ++ [4])
    else lambda_var_occur_Rec name body (lvl >>= (fun x => Some (x+1))) (idxs ++ [4])

                              
  | tProd nAnon argSx argDx =>
    (lambda_var_occur_Rec name argSx (lvl >>= (fun x => Some (x+0))) (idxs ++ [2])) ++
    (lambda_var_occur_Rec name argDx (lvl >>= (fun x => Some (x+1))) (idxs ++ [3]))
                         
  | tLambda named type body =>
    lambda_var_occur_Rec name (tProd named type body) lvl idxs
                                               
  | tApp type args =>
    (lambda_var_occur_Rec name type lvl (idxs ++ [1])) ++
    (List.concat (mapi (fun i a => lambda_var_occur_Rec name a lvl (idxs ++ [2; (i+1)])) args))

  | tCase typ obj out casi =>
    (lambda_var_occur_Rec name obj (lvl >>= (fun x => Some (x+0))) (idxs ++ [2])) ++
    (lambda_var_occur_Rec name out (lvl >>= (fun x => Some (x+0))) (idxs ++ [3])) ++
    (List.concat (mapi (fun i '(nargs, caso) =>
                          lambda_var_occur_Rec name
                                               caso
                                               lvl
                                               (idxs ++ [4; (i+1); 1])) casi))

  | tFix [x] _ =>
        (lambda_var_occur_Rec name (dtype x) lvl (idxs ++ [1; 1; 1])) ++
        (lambda_var_occur_Rec name (dbody x) lvl (idxs ++ [1; 1; 2]))
        
  | _ => []
  end.
Fixpoint lambda_var_occur (name: string) (tm: term) : list (list nat) :=
  lambda_var_occur_Rec name tm None [].
 

Fixpoint genericFind
         (check : term -> bool)
         (idxs: list nat) (tm: term) {struct tm}
  : list (list nat) :=

  let if_found := if check tm
                  then [idxs]
                  else [] in
  
  let recurse :=       
      match tm with
      | tProd _ argSx argDx =>
        (genericFind check (idxs ++ [2]) argSx) ++
        (genericFind check (idxs ++ [3]) argDx)
                                                
      | tApp type args =>
        (genericFind check (idxs ++ [1]) type) ++
        (List.concat (mapi (fun i a => genericFind check (idxs ++ [2; (i+1)]) a) args))
      | tLambda _ type body =>
        (genericFind check (idxs ++ [2]) type) ++
        (genericFind check (idxs ++ [3]) body)

      | tCase typ obj out casi =>
        (genericFind check (idxs ++ [2]) obj) ++
        (genericFind check (idxs ++ [3]) out) ++
        (List.concat (mapi (fun i '(nargs, caso) =>
                              genericFind check (idxs ++ [4; (i+1); 1]) caso) casi))
        
      | tFix [x] _ =>
        (genericFind check (idxs ++ [1; 1; 1]) (x.(dtype))) ++
        (genericFind check (idxs ++ [1; 1; 2]) (x.(dbody)))

      | tLetIn _ tmDef _ body =>
        (genericFind check (idxs ++ [2]) tmDef) ++
        (genericFind check (idxs ++ [4]) body)           

      | _ => []
      end in

  if_found ++ recurse.


(* 
   Applica una funzione all'i-esimo elemento di una lista. 
   Se ∄ l'i-esimo elemento, non fare nulla.
*)
Fixpoint change_nth {A} (i: nat) (l: list A) (f: A -> A) : list A :=
  match i, l with
  | 0, [] => []
  | 0, x::xs => f x :: xs
  | S i', [] => []
  | S i', x::xs => x :: change_nth i' xs f
  end.

Inductive ASTERROR := AstError.

(* 
   Data una coordinata, sostituisce il termine in _quella_ coordinata
   con il termine `new`.
 *)
Fixpoint subAt
         (trans : term -> term)
         (idx: list nat) (tm: term) {struct tm}
  : term :=
  match idx, tm with
  | [], _ => trans tm
                  
  | 2::idxs, tProd n argSx argDx =>
    tProd n (subAt trans idxs argSx) argDx
  | 3::idxs, tProd n argSx argDx =>
    tProd n argSx (subAt trans idxs argDx)
  | 1::idxs, tApp type body =>
    tApp (subAt trans idxs type) body
  | 2::i::idxs, tApp type args =>
    match List.nth_error args (i-1) with
    | None   => tRel 999
    | Some t => tApp type (change_nth (i-1) args (fun _ => subAt trans idxs t))
    end
  | 2::idxs, tLambda n type body =>
    tLambda n (subAt trans idxs type) body
  | 3::idxs, tLambda n type body =>
    tLambda n type (subAt trans idxs body)
        
  | 2::idxs, tCase typ obj out casi =>
    tCase typ (subAt trans idxs obj) out casi
  | 3::idxs, tCase typ obj out casi =>
    tCase typ obj (subAt trans idxs out) casi
  | 4::i::1::idxs, tCase typ obj out casi =>
    match List.nth_error casi (i-1) with
    | None   => tRel 999
    | Some t => tCase typ obj out
                      (change_nth (i-1) casi
                                  (fun '(nargs, caso) => (nargs, subAt trans idxs caso)))
    end
      
  | 1::1::1::idxs, tFix [x] ind =>
    tFix [map_def (subAt trans idxs) id x] ind
  | 1::1::2::idxs, tFix [x] ind =>
    tFix [map_def id (subAt trans idxs) x] ind
         
  | _, _ => <% AstError %>
  end.
  

Module esempi_coordinate.
  Definition ex1 :=
    tProd (nNamed "t1") (tInd {| inductive_mind := "Top.tm"; inductive_ind := 0 |} [])
	  (tProd (nNamed "t2") (tInd {| inductive_mind := "Top.tm"; inductive_ind := 0 |} [])
	         (tApp (tRel 2)
		       [tApp (tConstruct {| inductive_mind := "Top.tm"; inductive_ind := 0 |} 2 [])
			     [tConstruct {| inductive_mind := "Top.tm"; inductive_ind := 0 |} 0 [];
			     tRel 1; tRel 0]; tRel 1])).

  Definition ex2 :=
    tProd (nNamed "t1") (tInd {| inductive_mind := "Top.tm"; inductive_ind := 0 |} [])
          (tApp (tConstruct {| inductive_mind := "Top.tm"; inductive_ind := 0 |} 2 []) [tRel 1; tRel 0]).

  Example lambda_v1 : lambda_var_occur "t1" ex1 = [[3; 3; 2; 1; 2; 2]; [3; 3; 2; 2]].
  Proof. trivial. Qed.
  Example lambda_v2 : lambda_var_occur "t1" ex2 = [[3;2;2]].
  Proof. trivial. Qed.

End esempi_coordinate.


(* 
   Data una coordinata e il nome di una variabile, ritorna l'indice di
   DeBrujin della variabile presso quella coordinata.
 *)

Unset Guard Checking. (* Disabilito termination checker *)
Fixpoint get_brujin (var: string) (idx: list nat) (dbj: option nat) (tm : term)
  {struct tm} : option term :=
  match idx, tm with
  | [], _ => match dbj with
             | Some n => Some (tRel n)
             | None   => None
             end
               
  | 2::xs, tProd (nNamed name) type _ =>
    if name =? var
    then get_brujin var xs (Some 0) type
    else get_brujin var xs (dbj >>= (fun x => Some (x+1))) type
                    
  | 3::xs, tProd (nNamed name) _ body =>
    if name =? var
    then get_brujin var xs (Some 0) body
    else get_brujin var xs (dbj >>= (fun x => Some (x+1))) body

  (* Per le implicazioni logiche → *)
  | 2::xs, tProd nAnon sx _ =>
    get_brujin var xs dbj sx  
  | 3::xs, tProd nAnon _ dx =>
    get_brujin var xs (dbj >>= (fun x => Some (x+1))) dx 
                    
  | 1::xs, tApp type _ =>    get_brujin var xs dbj type
  | 2::y::xs, tApp _ args => get_brujin var xs dbj (List.nth (y-1) args (tRel 999))

  | _, tLambda n type body =>
    get_brujin var idx dbj (tProd n type body)

        
  | 2::idxs, tCase typ obj out casi =>
    get_brujin var idxs dbj obj
  | 3::idxs, tCase typ obj out casi =>
    get_brujin var idxs dbj out
  | 4::i::1::idxs, tCase typ obj out casi =>
    match List.nth_error casi (i-1) with
    | None   => None
    | Some t => get_brujin var idxs dbj (snd t)
    end
      
  | 1::1::1::idxs, tFix [x] ind =>
    get_brujin var idxs dbj (dtype x) (* Oss: qui forse è più complicato il discorso? *)
  | 1::1::2::idxs, tFix [x] ind =>
    get_brujin var idxs dbj (dbody x) (* Idem? *)
         
  | _, _ => None
  end.

Set Guard Checking.

(* =========================================================== *)

Inductive Swappable :=
| Var : string -> Swappable
| Other : term -> Swappable
.

Coercion str_to_swappable_var (s: string) := Var s.
Coercion term_to_swappable_other (tm: term) := Other tm.

Definition obj_coordinates (obj: Swappable) (tm: term) :=
  match obj with
      | Var s   => lambda_var_occur s tm
      | Other t => genericFind (terms_eq t) [] tm
  end.

Check obj_coordinates.
(* 
   Dato un termine `tm` di tipo (ie. contenente solo tProd, tApp, tSort e tRel)
   e il nome di due variabili, genera tutti i possibili termini in
   cui le due variabili possono essere scambiate. 
*)
Definition swap_objects (obj1 obj2: Swappable) (tm: term) : list term :=
  let coord_obj1 := obj_coordinates obj1 tm in   
  let coord_obj2 := obj_coordinates obj2 tm in
  let combos := combinations coord_obj1 coord_obj2 in
  
  filter
    (fun t => negb (terms_eq t <% AstError %>))
    (map
       (fun '(i1, i2) =>
          let newTm1M :=
              match obj1 with
              | Other t => Some t
              | Var s1  => get_brujin s1 i2 None tm
              end in
          
          let newTm2M :=
              match obj2 with
              | Other t => Some t
              | Var s2  => get_brujin s2 i1 None tm
              end in

          match newTm1M, newTm2M with
          | None, _ => <% AstError %>
          | _, None => <% AstError %>
          | Some newTm1, Some newTm2 =>
            let temp := subAt (fun _ => newTm2) i1 tm in
            subAt (fun _ => newTm1) i2 temp
          end
       )
    combos).  






(* Definition swap_objects (obj1 obj2: Swappable) (tm: term) : list term := *)
(*   let coord_obj1 := obj_coordinates obj1 tm in    *)
(*   let coord_obj2 := obj_coordinates obj2 tm in *)
(*   let combos := combinations coord_obj1 coord_obj2 in *)
  
(*   map *)
(*     (fun '(i1, i2) => *)
(*        let newTm1 := *)
(*            match obj1 with *)
(*            | Other t => t *)
(*            | Var s1  => *)
(*              match get_brujin s1 i2 None tm with *)
(*                 | Some trel => trel *)
(*                 | None => <% AstError %> *)
(*              end *)
(*            end in *)
       
(*        let newTm2 := *)
(*            match obj2 with *)
(*            | Other t => t *)
(*            | Var s2  => *)
(*              match get_brujin s2 i1 None tm with *)
(*              | Some trel => trel *)
(*              | None => <% AstError %> *)
(*              end *)
(*            end in *)


(*        let temp := subAt (fun _ => newTm2) i1 tm in *)
(*        subAt (fun _ => newTm1) i2 temp *)
(*     ) *)
(*   combos.   *)
  
        
        
        
Module SanityChecks.
  Inductive gatto := Micio | Gattino.
  
  Inductive miao : gatto -> Prop :=
  | mew : ∀ (t1 t2: gatto),
      miao t1 -> miao t2 -> miao t1.

  Definition ex1 := <% ∀ t1 t2, miao t1 -> miao t2 %>.
  Example ex1_e1: lambda_var_occur "t1" ex1 = [[3;3;2;2;1]]. trivial. Qed.
  Example ex1_e2: lambda_var_occur "t2" ex1 = [[3;3;3;2;1]]. trivial. Qed.

  Example ex1_e3: get_brujin "t1" [3;3;2;2;1] None ex1 = Some (tRel 1). trivial. Qed.
  Example ex1_e4: get_brujin "t2" [3;3;3;2;1] None ex1 = Some (tRel 1). trivial. Qed.

  Definition ex2 := <% ∀ t1 t2, miao Micio -> miao t1 -> miao t2 %>.
  Run TemplateProgram (
        let ll := swap_objects (Var "t1") (Other <% Micio %>) ex2 in
        l <- tmEval all ll ;;
        match l with
        | x::_ => tmMkDefinition "var_const_swap" x
        | _    => tmFail "lista vuota"
        end
      ).
  Example ex2_e1: var_const_swap = ∀ t1 t2, miao t1 -> miao Micio -> miao t2.
  Proof. trivial. Qed.

End SanityChecks.


(* =========================================================== *)
(* =========================================================== *)
(* Rimuovere implicazioni *)

(* Un'implicazione viene interpretata da Metacoq come una sorta di 
   lambda anonima (un tProd, per essere più precisi), in virtù del
   fatto che:
       A -> B ≡ ∀ _: A, B
   rimuovere un'implicazione A → B significa quindi "abbassare" di 1 ogni
   indice di DeBrujin del termine B. Questa funzione svolge questo compito *)
Fixpoint shift_down_debrujin (t : term) :=
  match t with
  | tRel i => tRel (i - 1)
  | tEvar ev args => tEvar ev (List.map shift_down_debrujin args)
  | tLambda na T M => tLambda na (shift_down_debrujin T) (shift_down_debrujin M)
  | tApp u v => tApp (shift_down_debrujin u) (List.map shift_down_debrujin v)
  | tProd na A B => tProd na (shift_down_debrujin A) (shift_down_debrujin B)
  | tCast c kind t => tCast (shift_down_debrujin c) kind (shift_down_debrujin t)
  | tLetIn na b t b' => tLetIn na (shift_down_debrujin b) (shift_down_debrujin t) (shift_down_debrujin b')
  | tCase ind p c brs =>
    let brs' := List.map (on_snd shift_down_debrujin) brs in
    tCase ind (shift_down_debrujin p) (shift_down_debrujin c) brs'
  | tProj p c => tProj p (shift_down_debrujin c)
  | tFix mfix idx =>
    let mfix' := List.map (map_def shift_down_debrujin shift_down_debrujin) mfix in
    tFix mfix' idx
  | x => x
  end.

(* Questo effettua la rimozione del tProd anonimo. *)
Fixpoint remove_implications (tm: term) : list term :=
  match tm with
  | tProd (nNamed s) type body =>
    tm :: map (tProd (nNamed s) type) (tl (remove_implications body))       
  | tProd nAnon argSx argDx =>
    let muts_argDx := remove_implications argDx in
    tm :: map (tProd nAnon argSx) (tl muts_argDx) ++
       map shift_down_debrujin muts_argDx ++
      [argSx]
  | tApp type args =>
    let args_muts := bind_lst (map remove_implications args) in
    tm :: map (tApp type) (tl args_muts)
  | _ => [tm]
  end.


(* =========================================================== *)
(* =========================================================== *)
(* =========================================================== *)
(* Infine, l'interfaccia generica per ogni tipo di mutazione 
   per tipi induttivi *)


(* Questa funzione applica un operatore induttivo ad un costruttore. *)
Definition generic_map {seed thr: nat}
           (ast_inductive: term)
           (newNameInd oldNameCons newNameCons : ident)
           (foo: term -> TemplateMonad (list term)) (mode: MutationNumber seed thr)
  : TemplateMonad unit :=

  match ast_inductive with
  | tInd _ _ =>
    body      <- get_body_constructor ast_inductive oldNameCons ;;
    mutations <- foo body ;;

    match mutations with
    | []   => tmFail "Non era possibile nessuna mutazione! Aborting."
    | _::_ => substitute_constructor_multiple_times
                ast_inductive
                newNameInd oldNameCons newNameCons
                mutations
    end
  | _ => tmFail "Not an inductive type!"
  end.



(* =========================================================== *)
(* Questa funzione applica un operatore induttivo a tutti i costruttori. *)

Definition generic_map_all_constructors {seed thr: nat}
           (ast_inductive: term) (newNameInd : ident)
           (foo: term -> TemplateMonad (list term))
           (mode: MutationNumber seed thr)
  :=

  constructors_names_raw <- get_constructors_names ast_inductive ;;
  constructors_names <- tmEval all constructors_names_raw ;;

  monad_map (fun oldNameCons =>
      match ast_inductive with
      | tInd _ _ =>
        body      <- get_body_constructor ast_inductive oldNameCons ;;
        mutations <- foo body ;;

        
        newNameCons' <- tmEval all (oldNameCons ++ "_MUT")%string ;;
        newNameCons  <- tmFreshName newNameCons' ;;

        match mutations with
        | []   => tmMsg ("Non era possibile nessuna mutazione per " ++ oldNameCons ++ ".")
        | _::_ => substitute_constructor_multiple_times
                    ast_inductive
                    newNameInd oldNameCons newNameCons'
                    mutations
        end
      | _ => tmFail "Not an inductive type!"
      end
    ) constructors_names.

