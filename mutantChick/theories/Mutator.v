(* Esperimenti con Coq e MetaCoq *)
Require Import List Arith String Bool Nat.
Require Import MetaCoq.Template.All.
Require Import Unicode.Utf8.

Import ListNotations MonadNotation.
Local Open Scope string_scope.
Local Open Scope list_scope.

Require Import MutantChick.utils.
Require Import MutantChick.Base.
Require Import MutantChick.InductiveTypes.

(** This Inductive contains _all_ the operators, both for inductives and non-inductives. *)
Inductive Operator : Type :=
| Substitute {A} (f1 f2: A)
| IfThenElse
| DelImplications
| Swap (x y: Swappable)
| NewConstructor (newNameCons: ident) (type: term)
| SubConstructor (oldNameCons newNameCons : ident) (type : term)
| OnConstructor  (oldNameCons: ident) (o: Operator) 
| OnConstructors (o: Operator)
| UserDefined (check: term -> bool) (trans: term -> term)
.

Notation "F1 ==> F2" :=
  (Substitute F1 F2) (at level 10).
Notation "v1 ⇆ v2" :=
  (Swap v1 v2) (at level 10).


(* =========================================================== *)
(* Effettua una sostituzione via operatore *)

Definition monadic_sub {seed thr: nat}
           (mode: MutationNumber seed thr) (op: Operator) (tm_ast: term)
  : TemplateMonad (list term) :=

  let maxNumOfMutations :=
      match mode with
      | MaxNMutations n _ _ => n
      | _ => 0
      end in
        
  match op with
  | @Substitute A f1 f2 =>
        f1_ast <- tmQuote f1 ;;
        f2_ast <- tmQuote f2 ;;
        ret ((multisub
              (fun tm => (sub_tApp f1_ast f2_ast tm) |b> (terms_eq f1_ast tm))
              (fun tm => (sub_tApp f1_ast f2_ast tm  |> f2_ast))
              seed
              thr
              maxNumOfMutations
              tm_ast))

  | IfThenElse =>
    ret (multisub is_tCase invert_if_clauses seed thr maxNumOfMutations tm_ast)
  | DelImplications =>
    ret (tl (remove_implications tm_ast))
  | Swap x1 x2 =>
    ret (swap_objects x1 x2 tm_ast)
  | UserDefined check trans =>
    ret (multisub check trans seed thr maxNumOfMutations tm_ast)
  | _ =>
    tmFail "ERRORE: operatore non ancora implementato (funzione monadic_sub)" 
  end.


(* =========================================================== *)
(* Next step: mutare definizioni. *)

(* Ritorna il corpo di una definizione (SOLO il corpo!) *)
Definition monadic_return_def_body (name: ident) : TemplateMonad (option term):=
  x <- tmQuoteConstant name false ;;
        
  match x with
  | DefinitionEntry y =>
    ret (Some y.(definition_entry_body))
  | ParameterEntry y =>
    ret (Some y.(parameter_entry_type))
  end.


       
(* =========================================================== *)
(* Effettua una sostituzione via operatore e la "mette" dentro
una nuova definizione *)

From TypingFlags Require Import Loader.
Unset Guard Checking.

Fixpoint generate_mutant {seed thr: nat}
         (mode: MutationNumber seed thr) (name: ident) (op: Operator)
         (tm_ast: term) {struct tm_ast} : TemplateMonad unit:=
  match tm_ast, op with

  | tConst nameDef _, _ =>
    tm_body <- monadic_return_def_body nameDef ;;
    (* tmPrint tm_body ;; *)
    match tm_body with
    | Some body => generate_mutant mode name op body
    | None      => tmFail ("ERRORE: impossibile estrarre corpo della funzione " ++ nameDef)
    end
      
  | _ , (NewConstructor newNameCons newType) =>
    add_constructor
      tm_ast
      name newNameCons
      newType

  | _ , (SubConstructor oldNameCons newNameCons newType) =>
    substitute_constructor
      tm_ast
      name oldNameCons newNameCons
      newType

  | _ , (OnConstructor constrName op') =>
    generic_map
      tm_ast name constrName constrName
      (monadic_sub mode op') mode ;;
    tmReturn tt (* aka.  `return unit`  *)
             
  | _ , (OnConstructors op') =>
    generic_map_all_constructors
      tm_ast name
      (monadic_sub mode op') mode ;;
    tmReturn tt (* aka.  `return unit`  *)

  (* | _ , DelImplications => *)
  (*   generic_map_all_constructors *)
  (*     tm_ast name *)
  (*     (monadic_sub mode DelImplications) mode ;; *)
  (*   tmReturn tt *)

  | tInd _ _, DelImplications =>
    generic_map_all_constructors
      tm_ast name
      (monadic_sub mode DelImplications) mode ;;
    tmReturn tt

  | tInd _ _, Swap _ _ =>
    generic_map_all_constructors
      tm_ast name
      (monadic_sub mode op) mode ;;
    tmReturn tt
                
  | _, _ =>
    lt <- monadic_sub mode op tm_ast ;;
    monad_iter (fun t =>
                  n <- tmFreshName name ;;
                  tmMsg (append n " created") ;;
                  tmMkDefinition n t)
               lt
  end.
                   
Set Guard Checking.

(* 
   Simile a ( ==>* ) di MuCheck 
*)
Fixpoint monadic_mutants {seed thr: nat}
         (mode: MutationNumber seed thr) (name: ident)
         (ops: list Operator) (tm_ast: term) :=
  match ops with
  | op::ops =>
      generate_mutant mode name op tm_ast ;;
      monadic_mutants mode name ops tm_ast
  | [] => ret unit
  end.


(* =========================================================== *)
(* =========================================================== *)
(* Notazioni *)

Notation "'Mutate' AST 'using' OP" :=
  (monadic_sub AllMutations OP AST)
    (at level 10,
     AST at next level, OP at next level,
     no associativity).
Notation "'Mutate' AST 'using' OP 'named' OPNAME" :=
  (generate_mutant AllMutations OPNAME OP AST)
    (at level 10,
     AST at next level, OP at next level, OPNAME at next level,
     no associativity).
Notation "'Mutate' AST 'using' OP 'named' OPNAME 'generating' MODE" :=
  (generate_mutant MODE OPNAME OP AST)
    (at level 10,
     AST at next level, OP at next level, OPNAME at next level, MODE at next level,
     no associativity).

Notation "'MultipleMutate' AST 'using' OPS 'named' OPNAME" :=
  (monadic_mutants AllMutations OPNAME OPS AST)
    (at level 10,
     AST at next level, OPS at next level, OPNAME at next level,
     no associativity).
Notation "'MultipleMutate' AST 'using' OPS 'named' OPNAME 'generating' MODE" :=
  (monadic_mutants MODE OPNAME OPS AST)
    (at level 10,
     AST at next level, OPS at next level, OPNAME at next level, MODE at next level,
     no associativity).
                                     

  
(* =========================================================== *)
(* =========================================================== *)
(* Sostituire tutte le occorrenze di una definizione all'interno
 di una congettura *)
(* Oss: ma questo serve ancora? Per mutare i nomi posso usare
     a ==> b oppure questa funzione riesce a fare cose migliori? *)     
  
Section namereplace.
  Variables (oldIdent newIdent: ident). 

  Fixpoint name_replace (tm: term): term :=
    if terms_eq (tConst oldIdent []) tm
    then (tConst newIdent []) 
    else
      match tm with
      | tApp fnameTm argsTm => tApp (name_replace fnameTm)
                                    (map name_replace argsTm)
      | tFix [x] i => tFix [(map_def id name_replace x)] i
      | tLambda arg typ body =>
        tLambda arg (name_replace typ) (name_replace body)
      | tCase typ cond boh ls =>
        tCase typ (name_replace cond) (name_replace boh)
              (map (fun tupla => match tupla with (n, t) =>
                                                  (n, name_replace t) end) ls)
      | tProd n a1 a2 => tProd n (name_replace a1) (name_replace a2)
      | tLetIn name tDef tType tBody =>
        tLetIn name (name_replace tDef) (name_replace tType) (name_replace tBody)
      | tInd {| inductive_mind := n; inductive_ind := num |} lst =>
        if n =? oldIdent
        then tInd {| inductive_mind := newIdent; inductive_ind := num |} lst
        else tm
      | _ => tm 
      end.

End namereplace.

Definition change_occurrences (defname oldIdent newIdent outname: ident) :=
  maybename1 <- tmAbout oldIdent ;;
  maybename2 <- tmAbout newIdent ;;
     
  match maybename1, maybename2 with
  | Some (ConstRef old), Some (ConstRef new)
  | Some (IndRef {| inductive_mind := old; inductive_ind := _ |}),
    Some (IndRef {| inductive_mind := new; inductive_ind := _ |})
    =>
      x <- tmQuoteConstant defname false ;;
      
      new_body <- (
        match x with
        | ParameterEntry entry =>
          tmFail "Non implementato." 
          (* ret (name_replace old new (entry.(parameter_entry_body))) *)
        | DefinitionEntry entry =>
          ret (name_replace old new (entry.(definition_entry_body)))
      end) ;;

      newOutName <- tmFreshName outname ;;
      tmMkDefinition newOutName new_body ;;
      tmMsg (append "Created " newOutName)
                     
    | _, _ => tmFail "Couldn't find either old' or new' in change_occurrences"
  end.


Module todo_nested.
  (* Supponi di avere queste tre definizioni: *)
  Definition a := firstn 1 [1;2;3].
  Definition b := a.
  Definition c := b.

  (* E di mutare la definizione a: *)
  Run TemplateProgram (
        Mutate <% a %>
               using (2 ==> 99)
               named "a"
      ).

  (* Supponi ora di voler testare la definizione c, che richiama
     indirettamente la a. Come fare? Devi creare versioni mutate 
     di tutte le definizioni intermedie! *)

  (* Inizia da qui: quoteRec ritorna il global_environment, aka. 
     la lista di definizioni da cui dipende "c" *)
  Run TemplateProgram (
        prog <- tmQuoteRec c ;;
             tmPrint prog
      ).

End todo_nested.

Fixpoint change_ocs (l: list ident) (oldIdent newIdent: ident) : TemplateMonad unit :=
  match l with
  | [] => tmPrint "completed"
  | s::ls =>
    (* new_s <- tmFreshName s ;; *)
    let new_s__ := (s ++ "_mut")%string in
    new_s <- tmEval lazy new_s__ ;; 
    change_occurrences s oldIdent newIdent new_s ;;
    change_ocs ls s new_s
  end.

