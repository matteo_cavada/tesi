Require Import List Arith String Bool.
Require Import MetaCoq.Template.All.
Import ListNotations MonadNotation.

(* =========================================================== *)
(* =========================================================== *)
(* Funzioni fondamentali (nel senso che sono le fondamenta del
   programma, non che sono profonde o inevitabili)             *)


(* Metodo per testare l'eguaglianza di due termini AST *)
Definition terms_eq (t1 t2: term) : bool :=
  string_of_term t1 =? string_of_term t2.


(* Testare l'equaglianza di una lista di argomenti dati a una funzione *)
Fixpoint list_of_term_eq (lt1 lt2: list term) : bool :=
  match lt1, lt2 with
  | [], [] => true
  | [], _  => false
  | _::_, [] => false
  | x::xs, y::ys => if terms_eq x y
                    then list_of_term_eq xs ys
                    else false
  end.


(* Prende due liste di argomenti per una funzione e controlla se
args2 è presente nella testa di args1. 

Serve, per esempio, quando devo fare la sostituzione
  @take nat ==> @drop nat
nel termine
  take 2 [1;2;3]

In questo caso le due liste di argomenti saranno
A := [nat]
B := [nat, 2, [1;2;3]] 
*)
Fixpoint is_partial_app (args1 args2: list term) :=
  match args1, args2 with
  | [], _ => true
  | _, [] => false
  | x::xs, y::ys =>
    if terms_eq x y
    then is_partial_app xs ys
    else false
  end.


(* Substitution for a tApp term; factorized for readability *)
Definition sub_tApp (old new tm: term) : option term :=
  match old, new, tm with
  | tApp fnameOld argsOld,
    tApp fnameNew argsNew,
    tApp fnameTm  argsTm  =>
      if andb (terms_eq fnameOld fnameTm)
              (is_partial_app argsOld argsTm)
      then
        Some (tApp fnameNew
                   (argsNew ++ (List.skipn (List.length argsOld) argsTm)))
      else
        None
          
  | _, _, _ => None
  end.


(* =========================================================== *)
(* =========================================================== *)
(* Generazione di alberi sintattici mutati, in modo che ogni albero
ottenuto contenga una sola mutazione per volta. *)


(* Queste funzioni hanno lo scopo di mutare una sola cosa alla volta.

[arg1; arg2; arg3]
  ==>
[[mut10; mut11];
 [mut20; mut21; mut22];
 [mut30; mut31]]
  ==>
[[mut10; mut20; mut30]; (* nessuna mutazione *)
 [mut11; mut20; mut30];
 [mut10; mut21; mut30];
 [mut10; mut22; mut30];
 [mut10; mut20; mut31]]
 *)
Definition filter_nones {A} (l: list (option A)) :=
  List.concat (
      List.map (fun x => match x with
                         | None => []
                         | Some y => [y] end) l
    ).

Fixpoint bind_lst_rec {A} (lss: list (list A)) : list (list A) :=
  match lss with
  | [] => []
  | lx::ls =>
    match lx with
    | h::ms =>
      (map (fun l => h :: l) (bind_lst_rec ls)) ++ 
      (map (fun m => m :: (filter_nones
                (map (fun l => List.hd_error l) ls)))
           ms)
    | [] => []
    end
  end.

Fixpoint bind_lst {A} (lss: list (list A)) : list (list A) :=
  match lss with
  | [] => []
  | _ => (filter_nones (map (fun l => List.hd_error l) lss)) :: bind_lst_rec lss
  end.

(* Alcuni esempi *)
Module test_bind.
  Lemma ex2 : bind_lst [[1;2]; [11;22]; [111;222;333]] =
              [[1; 11; 111]; [1; 11; 222]; [1; 11; 333]; [1; 22; 111]; [2; 11; 111]].
  Proof. trivial. Qed.

  Lemma ex3 : bind_lst [[1]; [2]] = [[1; 2]].
  Proof. trivial. Qed.

  Lemma ex4 : bind_lst [[1; 2]] = [[1]; [2]].
  Proof. trivial. Qed.

  Lemma ex5 : bind_lst [[1]; [11; 22]; [111]] = [[1; 11; 111]; [1; 22; 111]].
  Proof. trivial. Qed.
  
  Lemma ex6 : bind_lst [[1; 2; 3]; [11;22;33]; [111;222;333]] =
              [[1; 11; 111]; [1; 11; 222]; [1; 11; 333]; [1; 22; 111]; [1; 33; 111]; [2; 11; 111]; [3; 11; 111]].
  Proof. trivial. Qed.

  Lemma ex7 : bind_lst [[1; 2]; [11;22]; [111;222]; [1111;2222]] =
  [[1; 11; 111; 1111]; [1; 11; 111; 2222]; [1; 11; 222; 1111]; [1; 22; 111; 1111]; [2; 11; 111; 1111]].
  Proof. trivial. Qed.
  
End test_bind.

(* =========================================================== *)

Definition invert_first_twos {A} (l: list A) :=
  match l with
  | xa::xb::xs => xb::xa::xs
  | _ => l
  end.

Definition invert_if_clauses (tm: term) :=
  match tm with
  | tCase tipo cond boh (xa::xb::[]) =>
    tCase tipo cond boh (invert_first_twos (xa::xb::[]))
  | _ => tm
  end.
Definition is_tCase (tm: term) :=
  match tm with
  | tCase _ _ _ _ => true
  | _ => false
  end.

(* =========================================================== *)

Definition attempt {A} (ox: option A) (x : A) :=
  match ox with
  | Some thing => thing
  | None       => x
  end.

Notation "OX |> X" := (attempt OX X) (at level 20).

Definition attempt_bool {A} (ox: option A) (b: bool) :=
  match ox with
  | Some _ => true
  | None   => b
  end.

Notation "OX |b> X" := (attempt_bool OX X) (at level 20).

(* =========================================================== *)


(* Combinatoria, genera tutti i possibili swap tra variabili *)
Fixpoint combinations {A} (l1 l2: list A) : list (A × A) :=
  match l1 with
  | x::xs => (map (fun y => (x, y)) l2) ++ (combinations xs l2)
  | [] => []
  end.
