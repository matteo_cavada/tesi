(* Sanity Checks collections *)
Require Import List Arith String Bool Nat.
Require Import MetaCoq.Template.All.
Require Import Unicode.Utf8.

Import ListNotations MonadNotation.
Local Open Scope string_scope.
Local Open Scope list_scope.

Require Import MutantChick.utils.
Require Import MutantChick.Base.
Require Import MutantChick.InductiveTypes.
Require Import MutantChick.Mutator.


Module esempi_semplici.
  Inductive even : nat -> Prop :=
  | ev0 : even 0
  | evs (n: nat): even n -> even (n+2).

  Run TemplateProgram (
        Mutate <% even %>
               using (OnConstructors (2 ==> 9))
               named "two_nine_rand"
      ).
  Print two_nine_rand.
  Example ex_1: ∀ n, two_nine_rand n → two_nine_rand (n + 9).
  Proof. constructor. assumption. Qed.


  (* AGGIUNTA: muto tipo semplice con SWAP *)
  Run TemplateProgram (
        Mutate <% 1 - 9 %>
               using ((Swap <% 1 %> <% 9 %>))
               named "one_nine_swap"
      ).
  Print one_nine_swap.
  Example ex_2: one_nine_swap = 8. Proof. trivial. Qed.
  (* Fail Print one_nine_swap0. *)


  (* AGGIUNTA: muto definizioni usando la sintassi Mutate *)
  Definition ciao (n: nat) :=
    firstn 1 [7;3;5].

  Run TemplateProgram (
        Mutate <% ciao %>
               using (Swap <% 3 %> <% 7 %>)
               named "ciao_mut"
      ).
  Print ciao_mut.
  Example ex_3: forall m, ciao_mut m = firstn 1 [3; 3; 5].
  Proof. trivial. Qed.

  (* Fail Print ciao_mut0. (* Non dovrebbe esistere *) *)
  (* Fail Print ciao_mut1. (* Non dovrebbe esistere *) *)

  
  (** Nested IfThenElse *)
  Run TemplateProgram (
        Mutate <% if Nat.eqb 5 (3+2) then [0] else
                    if Nat.eqb 2 3 then [42] else [1] %>
               using IfThenElse
               named "invert_nested"
      ).
  Print invert_nested.
  Print invert_nested0.

  Example ex_40: invert_nested0 = if (5 =? 3 + 2)%nat then [0] else if (2 =? 3)%nat then [1] else [42].
  Proof. trivial. Qed.
  Example ex_41: invert_nested = if (5 =? 3 + 2)%nat then if (2 =? 3)%nat then [42] else [1] else [0].
  Proof. trivial. Qed.


End esempi_semplici.


Module example_multiple.
  Run TemplateProgram (
        MultipleMutate <% firstn 2 [2;3;2] %>
                          using [2 ==> 9; 3 ==> 5]
                          named "two_nine"
      ).
  Print two_nine. Print two_nine0. Print two_nine1. Print two_nine2.
  Fail Print two_nine3.

  Example ex1: two_nine = firstn 9 [2; 3; 2]. Proof. trivial. Qed.
  Example ex11: two_nine0 = firstn 2 [9; 3; 2]. Proof. trivial. Qed.
  Example ex2: two_nine2 = firstn 2 [2; 5; 2]. Proof. trivial. Qed.
End example_multiple.


Module example_multiple_rand.
  Run TemplateProgram (
        MultipleMutate <% firstn 2 [2;3;2] %>
                       using [2 ==> 9; 3 ==> 5]
                       named "two_nine"
                       generating (RandomMutations 77 100)
      ).
  Print example_multiple_rand.two_nine.
  Print example_multiple_rand.two_nine0.
  
End example_multiple_rand.


Module examples_2.
  Definition simple_def {A} (n : nat) (l: list A) :=
    firstn n l.

  Run TemplateProgram (
        Mutate <% @simple_def %> (* @ for implicit variables *)
               using (@firstn ==> @skipn)
               named "simple_def_mut"
      ).
  Example simple_def_mut0_lemma {A} : forall n l,
      @simple_def_mut A n l = @skipn A n l.
  Proof. trivial. Qed.

  

  Definition simple_if (n: nat) (l: list nat) (b: bool) :=
    if b then firstn n l else l.

  Run TemplateProgram (
        Mutate <% simple_if %>
               using IfThenElse
               named "simple_if_mut"
      ).
  Example simple_if_mut0_lemma : forall n l b,
      simple_if_mut n l b = if b then l else firstn n l.
  Proof. trivial. Qed.


  
  Definition fact' :=
    fix fact1 n :=
      match n with
      | 0 => 1
      | 1 => 1
      | S n' => n * fact1 n'
      end.

  Run TemplateProgram (
        Mutate <% fact' %>
               using (Init.Nat.mul ==> Init.Nat.add)
               named "fact'_mut"
      ).
  Print fact'_mut.
  Fail Print fact'_mut0.

  Fixpoint sum_firstn n := match n with
    | 0 | 1 => 1
    | S n' => n + sum_firstn n' end.
  Example sum_mult: forall n, fact'_mut n = sum_firstn n.
  Proof. trivial. Qed.

End examples_2.



Module vanno_gli_induttivi.
  Print even.

  Inductive even : nat -> Prop :=
  | ev_0 : even 0
  | ev_SS (n: nat) : even 0 -> even n -> even (S (S n))
  .
  
  Run TemplateProgram (
        Mutate <% even %>
                  using (OnConstructors (0 ==> 1))
                  named "even"
      ).
  Print even0.
  Print even1.
  Fail Print even2.


  Run TemplateProgram (
        Mutate <% even %>
                  using (DelImplications)
                  named "even"
      ).
  Print even2.
  Print even3.
  Print even4.
  (* Fail Print even5. *)

  
  Run TemplateProgram (
        Mutate <% even %>
                  using (Swap <%0%> "n")
                  named "even"
      ).
  Print even5.
  Print even6.
  (* Fail Print even7. *)
  
End vanno_gli_induttivi.

(* =========================================================== *)
(* =========================================================== *)
(* =========================================================== *)

Module TestDelImplications.

  Inductive even: nat -> Prop := (* Definition is nonsensical but useful for testing *)
  | ev_Z: even 0
  | ev_SS: forall n, even n -> even 0 -> even (S (S n))
  | ev_PR: forall n m, even (n + m) -> even 0.

  Run TemplateProgram (
        Mutate <% even %>
                  using (OnConstructor "ev_SS" DelImplications)
                  named "even_NOIMPL"
      ).

  (* Example ex1: ∀ n, even_NOIMPL1 (S (S n)). *)
  (* Proof. apply ev_SS2. Qed. *)
  (* Fail Print even_NOIMPL2. *)
    

  Inductive long : nat -> Prop :=
    | lunga_impl : ∀ t1 t2, long 0 -> long t1 -> long 1 -> long t2.

  Run TemplateProgram (
        Mutate <% long %>
               using (OnConstructor "lunga_impl" DelImplications)
               named "shorter"
      ).
  Print shorter5.
  (* Fail Print shorter6. *)

  
End    TestDelImplications.
Module TestDelImplications2.
  (* Identico a sopra, in teoria; testa se è vero anche in pratica *)
  Inductive long : nat -> Prop :=
    | lunga_impl : ∀ t1 t2, long 0 -> long t1 -> long 1 -> long t2.

  Run TemplateProgram (
        Mutate <% long %>
               using (OnConstructors DelImplications)
               named "shorter"
      ).
  Print shorter5.
  Fail Print shorter6.
End TestDelImplications2.

