Module prova.
  Require Import Listmachine.
  Definition stepFunc := stepFunc. 
End prova.

Module qc1.
  Require Import T.Testing.
End qc1.

Require Import MutantChick.Base.
Require Import MutantChick.Mutator.
Import MetaCoq.Template.monad_utils.MonadNotation.

Require Import MetaCoq.Template.All.

Require Import MutantChick.Mutator.
Require Import MutantChick.Base.
Require Import MutantChick.utils.
Require Import MutantChick.InductiveTypes.

Require Import Arith String List Bool.
Require Import Coq.Unicode.Utf8.

Import ListNotations MonadNotation.
Open Scope list_scope.

From T Require Import Maps.

(* Fixpoint reverse (l: list nat) : list nat := *)
(*   match l with *)
(*   | [] => [] *)
(*   | x::xs => reverse xs ++ xs *)
(*   end. *)

(* Fixpoint lists_eq (l1 l2: list nat) := *)
(*   match l1, l2 with *)
(*   | x::xs, y::ys => if beq_nat x y then lists_eq xs ys else false *)
(*   | [], [] => true *)
(*   | _, _ => false *)
(*   end. *)

(* Definition prop_involutive (l: list nat) := *)
(*   lists_eq (reverse (reverse l)) l. *)

(* QuickChick prop_involutive. *)

(* Compute reverse [1;2;3;4]. *)
(* Check sorted. *)


(* (* =========================================================== *) *)
(* M3 - OK
   "stepFunc does not update the store for cons instructions" *)

(* Costruisco un operatore ad hoc; in pratica, ogni
      env # v <- x   diventa semplicemente
      env  
 *)

Definition noUpdate :=
  UserDefined
        (fun tm => match tm with
                   | tApp (tConst "T.Maps.set" []) [_;_;_;_] => true
                   | _ => false end)
        (fun tm => match tm with
                   | tApp (tConst "T.Maps.set" []) [_;env;_;_] => env
                   | _ => tm end).

Run TemplateProgram (
      Mutate <% Listmachine.stepFunc %>
             using noUpdate
             named "M3_0"
    ).
Print M3_0.

Run TemplateProgram (
      Mutate <% Testing.preservationP %>
             using (Listmachine.stepFunc ==> M3_0)
             named "preservationP_M3_0"
    ).
QuickChick preservationP_M3_0.
(* *** Failed after 130 tests and 0 shrinks. (75 discards) *)


Run TemplateProgram (
      change_occurrences "Testing.steps" "Listmachine.stepFunc" "M3_0" "steps_mut_M3" ;;
      change_occurrences "Testing.trySteps" "Testing.steps" "steps_mut_M3" "trySteps_mut_M3" ;;
      change_occurrences "Testing.soundnessP" "Testing.trySteps" "trySteps_mut_M3" "soundnessP_M3"
    ).
QuickChick soundnessP_M3.
(* *** Failed after 4189 tests and 0 shrinks. (5443 discards) *)


(* =========================================================== *)
(* M6 - OK
   "After a step the instructions in instr seq are wrongly reordered" *)
(* Nota: qualche volta, MetaCoq non preserva i nomi di variabile introdotti
   da lambda o simili; in questo caso ad esempio, la variabile "i3" viene
   tradotta semplicemente come "i" - è una cosa fastidiosa ma inevitabile 
   purtroppo *)
Run TemplateProgram (
      Mutate <% Listmachine.stepFunc %>
             using (Swap "i" "i2") (* i al posto di i3 *)
             named "M6"
    ).
Print M6.

Run TemplateProgram (
      Mutate <% Testing.preservationP %>
             using (Listmachine.stepFunc ==> M6)
             named "preservationP_M6"
    ).
(* QuickChick preservationP_M6. *)
(* +++ Passed 10000 tests (4959 discards) ??? *)


Run TemplateProgram (
      change_occurrences "Testing.steps" "Listmachine.stepFunc" "M6" "steps_mut_M6" ;;
      change_occurrences "Testing.trySteps" "Testing.steps" "steps_mut_M6" "trySteps_mut_M6" ;;
      change_occurrences "Testing.soundnessP" "Testing.trySteps" "trySteps_mut_M6" "soundnessP_M6"
    ).

(* QuickChick soundnessP_M6. *)
(* +++ Passed 10000 tests (4959 discards) ??? *)


(* =========================================================== *)
(* M13: NON OK 
   "Missing clause for listcons variance in subtyping" . *)
(* Il problema qui è che le chiamate ricorsive all'interno di un 
   tFix introducono delle variabili lambda che non avevo 
   considerato; questo andrebbe risolto *)

Definition sub_function_with_constant (func const: term) :=
  UserDefined
        (fun tm => match tm with
                   | tApp func' _ => terms_eq func' func
                   | _ => false end)
        (fun tm => match tm with
                   | tApp func' _ =>
                     if terms_eq func' func
                     then const
                     else tm
                   | _ => tm end).


Run TemplateProgram (
      Mutate <% Listmachine.check_subtype %>
             using (sub_function_with_constant <% Listmachine.check_subtype %>
                                               <% false %>)
             named "M3_0"
    ).

(* =========================================================== *)
(* M15 - ok (può tornare utile scelta casuale)
   "using constructor ty listcons instead of ty list in the third 
   value-has-ty predicate clause" *)

(* Run TemplateProgram ( *)
(*       Mutate <% Listmachine.value_has_tyBool %> *)
(*              using (Listmachine.ty_list ==> Listmachine.ty_listcons) *)
(*              named "M15_0" *)
(*     ). *)
(* Print M3_3. *)
(* Print M3_4. *)




(* =========================================================== *)
(* M16 - ok
   bug16-missing-ty_list-fetched-second-field *)

(* Definition rm_monotype_function (func: term) := *)
(*   UserDefined *)
(*         (fun tm => match tm with *)
(*                    | tApp g [_] => terms_eq g func *)
(*                    | _ => false end) *)
(*         (fun tm => match tm with *)
(*                    | tApp g [arg] => *)
(*                      if terms_eq g func *)
(*                      then arg *)
(*                      else tm *)
(*                    | _ => tm *)
(*                    end). *)

                     
(* Run TemplateProgram (       *)
(*       Mutate <% Listmachine.typecheck_instr %> *)
(*              using (rm_monotype_function <% Listmachine.ty_list %>) *)
(*              named "M16_0" *)
(*     ). *)

(* Print M16_0. *)
(* Print M16_1. *)


(* Versione monadica alternativa *)
Definition rm_monotype_function {A} (f: A -> A) :=
  quoted_f <- tmQuote f ;;
  ret (UserDefined
        (fun tm => match tm with
                   | tApp g [_] => terms_eq g quoted_f
                   | _ => false end)
        (fun tm => match tm with
                   | tApp g [arg] =>
                     if terms_eq g quoted_f
                     then arg
                     else tm
                   | _ => tm
                   end)).

Run TemplateProgram (
      op <- rm_monotype_function Listmachine.ty_list ;;
      
      Mutate <% Listmachine.typecheck_instr %>
             using op
             named "M16"
    ).

Run TemplateProgram (
      change_occurrences "Testing.updateEnv" "Listmachine.typecheck_instr" "M16" "updateEnv_mut" ;;
      change_occurrences "Testing.preservationP" "Testing.updateEnv" "updateEnv_mut"
                         "preservationP_M16"
    ).

QuickChick preservationP_M16.



(* =========================================================== *)
(* M17 - ok
   "Checking again the first instruction of a sequence instead of the second" *)

Run TemplateProgram (
      Mutate <% Listmachine.typecheck_block %>
             using (Swap "i1" "i2")
             named "M17_0"
    ).
Print M17_0.
Print Listmachine.typecheck_block.

Run TemplateProgram (
      change_occurrences "Testing.progressProp"
                         "Listmachine.typecheck_block" "M17_0"
                         "progressProp_M17"
    ).
Print progressProp_M17.
QuickChick progressProp_M17. (* Time out: no counterexample found *)


(* =========================================================== *)
(* M18 - ok
   "Actually not checking blocks" del pred *)

Run TemplateProgram (
      Mutate <% Listmachine.typecheck_blocks %>
             using (andb ==> orb) (* && diventa || *) 
             named "M18_0"
    ).
Print M18_0.


(* Alternativa: si può anche definire un operatore custom: *)
Definition remove_left_andb_argument :=
  UserDefined
    (fun tm => match tm with
               | tApp (tConst "Coq.Init.Datatypes.andb" []) [_;_] => true
               | _ => false end)
    (fun tm => match tm with
               | tApp (tConst "Coq.Init.Datatypes.andb" []) [_;b2] => b2
               | _ => tm end).

Run TemplateProgram (
      Mutate <% Listmachine.typecheck_blocks %>
             using remove_left_andb_argument 
             named "M18"
    ).
Print M18.



(* =========================================================== *)
(* =========================================================== *)
(* Snippet utile per debugging *)

(* Run TemplateProgram ( *)
(*       sx <- monadic_return_def_body "T.Listmachine.stepFunc" ;; *)
(*       match sx with *)
(*       | Some body => *)
(*         t <- tmEval all body ;; *)
(*         tmPrint t  *)
(*       | other     => tmFail "fail" *)
(*       end *)
(*     ). *)

(* =========================================================== *)
(* =========================================================== *)
(* Un esperimento con quickchick *)

Inductive func_2_args := (* Contenitore per funzione con due argomenti uguali *)
  | func2 {A} (f: A -> A -> A).

(* Trasforma una funzione di due argmenti (f: a->a->a) in un operatore in cui
   si tiene solo il primo argomento e si butta via tutto il resto *)
Definition keep_fst_arg (foo: func_2_args) : TemplateMonad Operator :=
  match foo with
  | func2 _ func =>
      quoted_func <- tmQuote func ;;
      ret (UserDefined
             (fun tm => match tm with
                        | tApp func' [_;_] => terms_eq func' quoted_func
                        | _ => false end)
             (fun tm => match tm with
                        | tApp func' [_;b2] =>
                          if terms_eq func' quoted_func
                          then b2
                          else tm
                        | _ => tm end))
  end.

(* Due funzioni di Listmachine che hanno forma A -> A -> A *)
Print Listmachine.instr_seq.
Print Listmachine.value_cons.


(* Uso MultipleMutate per mutare con più operatori
   contemporaneamente *)
Run TemplateProgram (
      let foos := [func2 Listmachine.instr_seq;
                   func2 Listmachine.value_cons] in
      operatori <- monad_map keep_fst_arg foos ;;

      MultipleMutate <% Listmachine.stepFunc %>
                     using operatori
                     named ["stepFunc_mut_seq";"stepFunc_mut_cons"]
    ).
(* Print mut_seq. *)
(* Print mut_seq0. *)
(* Print mut_cons. *)

Run TemplateProgram (
      Mutate <% Testing.preservationP %>
             using (Listmachine.stepFunc ==> stepFunc_mut_cons)
             named "preservationP_mut_cons"
    ).

QuickCheck preservationP_mut_cons.
(* Dà un controesempio ma è un po' criptico:
   (Some( (((((prog_block xH (instr_seq (instr_cons xH xH xH) (instr_jump (xI xH))) (prog_block xH (instr_seq (instr_cons xH xH xH) (instr_jump (xI xH))) (prog_block (xO xH) instr_halt (prog_block (xI xH) (instr_seq (instr_fetch_field xH 1 xH) instr_halt) (prog_block (xO (xO xH)) instr_halt prog_end)))),[(xI xH,[(xH,ty_listcons ty_nil)]); (xO (xO xH),[]); (xO xH,[]); (xH,[(xH,ty_nil)])]),[(xH,value_nil)]),[(xH,ty_nil)]),xH),instr_seq (instr_cons xH xH xH) (instr_jump (xI xH))))) *)
