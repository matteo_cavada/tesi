Require Import Arith Bool List ZArith. Import ListNotations.
Require Import Coq.Lists.List.
From QuickChick Require Import QuickChick.
Import QcNotation. (* Suppress some annoying warnings: *)
Set Warnings "-extraction-opaque-accessed,-extraction". 
 Require Import String. Local Open Scope string. 
Require Export ExtLib.Structures.Monads.
Export MonadNotation. 
Open Scope monad_scope.

From T Require Import Maps.

From T Require Import Listmachine.

Module DoNotation.
Notation "'do!' X <- A ; B" :=
  (bindGen A (fun X => B))
    (at level 200, X ident, A at level 100, B at level 200).
End DoNotation.
Import DoNotation.

Derive (Arbitrary,Show) for value.
 
Derive (Arbitrary,Show) for positive. (*gen/show label*)

Derive Show for instr.

Derive Show for program.

Derive (Arbitrary,Show) for ty.

Instance showOption {A:Set} `{Show A} : Show (option A) :=
  {
    show :=
      fun (o: (option A)) =>
        match o with
        |None => "None"
        |Some x => "(Some( " ++ show x ++ "))"
        end
  }.

Definition string_of_map {A:Set}(m:map A) 
        `{Show (list (positive *  A))} : string :=
  show (Maps.toList m).

Instance showMap {A:Set}`{Show (list (positive *  A))} 
       : Show (map A) :=
  {
    show m:= 
      string_of_map m
  }.



(*
 * Generates a Cons instruction: Cons(v0,v1,v)
 * The variables v0 and v1 are taken from existing variables in the environment.
 * The target variable v could be an existing variable, or a new one: the new
 * index is chosen as N if the environment contains variables from 0 to N-1.
 *)

Definition genCons (e: env) : G (option (instr * env)):=
    let vars := (getKeys _ e) in (* assumo (e: env) non vuoto *)
    do! v0 <- elems_ xH vars ;
    do! v1 <- elems_ xH vars ;
    do! v <- fmap Pos.of_nat (choose (0, List.length vars) ) ;
    let t0 := getDefault _ e v0 ty_nil in
    let t1 := getDefault _ e v1 ty_nil in
    match lub (ty_list t0) t1 with
    | ty_list t =>
         returnGen (Some ((instr_cons v0 v1 v), e ## v <- (ty_listcons t)))
    | _ => returnGen None
     end.

(*
 * Generates a BranchIfNil instruction: BranchIfNil(v,l)
 * The variable v is chosen from the environment.
 * The label l has an index not greater to the one of the current block label.
 * This strategy generates only backward jumps to be sure to land in a block
 * that has an already defined block typing.
 * If the landing block typing is not a supertype of the current environment,
 * then a Cons instruction is generated instead.
 *)
Definition genBranchIfNil (l: label) (pi: program_typing)
                          (e: env) : G (option (instr * env)):=
    let vars := (getKeys _ e) in (* assumo (e: env) non vuoto *)
    do! var <- elems_ xH vars ;
    do! target <- fmap Pos.of_nat (choose (0, Pos.to_nat l) ) ;
    let jumpEnv := e ## var <- ty_nil in
    let stayEnv := 
      match e ## var with
      | Some (ty_list t) => e ## var <- (ty_listcons t)
      |  _ => e
      end in
    match pi ## target with 
    |Some targetEnv => 
      if check_env_sub jumpEnv targetEnv
      then returnGen (Some( (instr_branch_if_nil var target), stayEnv))
      else (genCons e)
    |_ => genCons e
    end.

(*
 * Generates a FetchField instruction: FetchField(v,pos,v')
 * The variable v is chosen from the environment.
 * The target variable v' could be an existing variable, or a new one: the new
 * index is chosen as N if the environment contains variables from 0 to N-1.
 * The fetch position pos is randomly chosen between Head and Tail.
 * If the chosen source variable v has not type Ty_listcons, then the FetchField
 * instruction cannot be invoked safely: in that case a Cons instruction is
 * generated instead.
 *)
Definition genFetchField (e: env) : G (option (instr * env)) :=
    let vars := (getKeys _ e) in (* assumo (e: env) non vuoto *)
    do! v <- elems_ xH vars ;
    do! v' <- fmap Pos.of_nat (choose (0, List.length vars) ) ;
    do! f <- elems [0;1] ;
    match e ## v with
    | Some (ty_listcons t)  => 
        let newEnv := 
         match f with
         | 0 => e ## v' <- t
         | _ => e ## v' <- (ty_list t)
         end in
        returnGen (Some ((instr_fetch_field v f v'), newEnv))
    | _ => (genCons e)
    end.

(*
 * Generates a randomly chosen instruction between Cons, BranchIfNil and
 * FetchField. The instruction is always chosen to be safe if executed in the
 * given environment. With the instruction itself, the generators produces the
 * environment updated after the execution of the instruction.
 * The BranchIfNil and FetchField cannot be always generated safely in relation
 * to the given environment. Then the Cons instruction is generated as a
 * fallback. This implies that Cons instructions are more frequent, but this
 * ensures an high probability of having more variables with different types in
 * the program typing and execution environment.
 *)
Definition genInstruction (l: label) (pi: program_typing) 
                          (e: env): G (option (instr * env)) :=
    oneOf [
        genCons e ;
        genBranchIfNil l pi e ;
        genFetchField e
    ].


(*
 * Generates a list of non-terminal instructions (i.e. Jump or Halt).
 * The generated environment is the result of applying the instructions list to
 * the source environment. The given label is the current block label.
 *)

Fixpoint genInstructions (l: label) (pi: program_typing)
                         (e: env) (size: nat) 
                          : G (option ((list instr ) * env)) :=
    match size with 
    | 0 => returnGen (Some([], e))
    | S n =>
       do! x <- (genInstructions l pi e n);
       match x with
       |Some x' =>
          let instructions := fst x' in
          let newEnv := snd x' in
          if List.length (getKeys _ newEnv) <? 1
           then returnGen (Some(instructions, newEnv))
          else 
            do! y <- genInstruction l pi newEnv ;
            match y with
            |Some y' =>
              let i := fst y' in
              let finalEnv := snd y' in
              returnGen (Some((List.app instructions [i]), finalEnv))
            |_ => returnGen None
            end
       |_ => returnGen None
       end
    end.


(* Generates a list of random positive length containing the first naturals *)
Fixpoint genNaturals (size: nat) : G (list nat) :=
    match size with
    | 0 => returnGen [0]
    | S n => oneOf [
               genNaturals n ;
               fmap (fun ns => List.app ns [List.length ns]) 
                    (genNaturals n )
           ]
    end.

(* Sample (sized(genNaturals)). *)

(* Generates a non-empty list of labels with increasing id *)
Definition genLabels (size: nat) : G (list label) :=
    fmap (fun ns => List.map (fun n =>Pos.of_nat (n + 1)) ns) (genNaturals size).

(* Sample (sized(genLabels)). *)

(*
 * If the block represented by the given label hasn't already
 * a non-empty
 * environment set, replace it with the given one
*)
Definition setBlockTypingIfEmpty (pi: program_typing) (l: label)
                          (newEnv: env): (option program_typing) :=
    match pi ## l with
    |Some e =>
      if (isEmpty _ e) then Some (pi ## l <- newEnv)
      else Some pi
    |_ => None 
    end.

(*
 * Generates a block terminal instruction (Jump or Halt).
 * The jump is always forward to a random label with id greater than the current
 * block one. If the chosen landing block has an incompatible environment (it
 * isn't a supertype of the current environment), then an Halt instruction is
 * generated instead. If there isn't any label to jump forward to, an Halt
 * instruction is generated.
 * If the landing block wasn't already the target of a previous Jump
 * instruction, then the environment of the target block is set equal to the
 * current environment.
 * If the block environment is empty (because no previous jumps landed on it),
 * then only an Halt instruction is generated. A Jump instruction could land
 * only on blocks that have an empty environment too. Therefore it would cause
 * a sequence of empty blocks with just Jump instructions leading to the last
 * one with an Halt, so generating here an Halt instruction is equivalent.
 * There is a small chance that not only the last block has an Halt instruction.
 *)
Definition genBlockEnd (from: label) (pi: program_typing)
                       (e: env) : G (option instr) :=
    let labels := List.length (toList pi) in
    let forwardLabels := labels - (Pos.to_nat from + 1) in
    if (forwardLabels = 0)? || (List.length (getKeys _ e) = 0 ?)
    then returnGen (Some instr_halt)
    else
     do! target <- fmap (fun id => Pos.of_nat (id + (Pos.to_nat from)))
                    (choose(1, forwardLabels)) ;
     match pi ## target with
     |Some e1 => 
        if check_env_sub e e1
        then returnGen (Some (instr_jump target))
        else returnGen (Some instr_halt)
     |_ => returnGen None
     end.



(*
 * Converts a list of instructions to nested Seq instructions 
 * terminating with
 * the given last instruction
 *)
Fixpoint rollInstructions (instructions : (list instr)) (last: instr): instr :=
    match instructions with
    | [] => last
    | cons h t =>
      instr_seq h (rollInstructions t last)
    end.

(*
 * Generates an entire block and updates the program typing.
 * If the last instruction is a Jump, then the landing block 
 *  typing is updated
 * with the environment resulting from the instructions list.
 * Except when the landing block has already a non-empty environment.
*)
Definition genBlock (l: label) (pi: program_typing)
                    (size: nat) :G (option (instr * program_typing)) :=
    match pi ## l with
    |Some e =>
        do! x <- (genInstructions l pi e size);
        match x with 
        |Some x' =>
          let instructions := fst x' in
          let env' := snd x' in
          do! last <- genBlockEnd l pi env';
          match last with
          |Some last' =>
              let block := rollInstructions instructions last' in
              match last' with
              |instr_jump l' => 
                match setBlockTypingIfEmpty pi l' env' with
                |Some pi' => 
                  returnGen(Some(block, pi'))
                |None => returnGen None
                end
              |_ => returnGen (Some(block,pi))
              end
          |_ => returnGen None
          end
        |_ => returnGen None
        end
    |_ => returnGen None
    end.


(*
 * Generates a list of consecutive blocks up to the given last label.
 * The size parameter is divided by the number of blocks. Because of integer
 * division, the first blocks may have a larger size because of the remainder.
 *) 
Fixpoint genBlocks (last: nat) (pi: program_typing) (size: nat)
         : G (option (list(label * instr) * program_typing)) :=
    let lastLabel := Pos.of_nat last in
    let blockSize := size / (last+ 1) in
    match last with
    | 0 => fmap (fun x =>
           match x with
           |Some (i, pi') => Some ([(lastLabel, i)], pi')
           |_ => None 
           end) (genBlock lastLabel pi blockSize)
    | S n =>
           do! v <- genBlocks n pi (size - blockSize);
           match v with 
           |Some (is, pi') =>
              do! v' <- genBlock lastLabel pi' blockSize;
              match v' with
              |Some (i, pi'') => 
                returnGen (Some(List.app is [(lastLabel, i)], pi''))
              |_ => returnGen None
              end
           |_ => returnGen None
           end 
    end.

Fixpoint fromListToProgram (l: list(label * instr)) : program :=
  match l with
  | [] => prog_end
  |cons (la , i) l' => 
     prog_block la i (fromListToProgram l')
  end.

(*
 * Generates a complete program and a program typing that typechecks with it.
 * The initial environment is set to [(v0, Ty_nil)]. The program contains at
 * least one block and at least an Halt instruction.
 *)
Definition genProgram (size: nat) : G (option (program * program_typing)) :=
    do! labels <- genLabels size ;
    let pi := ofList (List.map (fun l => (l, empty _)) labels) in
    let initialEnv := (empty _) ## xH <- ty_nil in
    match setBlockTypingIfEmpty pi xH initialEnv with
    |Some pi' =>
      do! x <- genBlocks (Pos.to_nat(List.last labels xH)) pi' size ;
      match x with
      |Some (blocks, pi'') =>
        returnGen (Some(fromListToProgram blocks, pi''))
      |_ => returnGen None
      end
    |_ => returnGen None
    end.

(* Sample (sized genProgram). *)

(*A volte genera qualche None*)
Definition genValueHasTy (t: ty):  G (option value) :=
  suchThatMaybe arbitrary (fun a => value_has_tyBool a t).


Fixpoint genValues (types: list (var * ty)) 
             : G (option (list (var * value))) :=
    match types with
    | [] => returnGen (Some [])
    | cons (v,t) l => 
          do! others <- genValues l;
          match others with
          | Some others' =>
            do! a <- genValueHasTy t;
            match a with
            |Some a' => 
              returnGen (Some(cons (v, a') others'))
            |None => returnGen None
            end
          | None => returnGen None
          end
    end.

(*
 * Generates an environment that typechecks with the given typing environment.
 *)
Definition storeFromEnv (g: env) 
            : G (option store) :=
    let temp0 := genValues(toList g) in
    fmap (fun (t: option (list (var * value))) =>
          match t with
          |Some l => Some (ofList l)
          |None => None
          end )
     temp0.


Fixpoint labelFromProgramAux (p : program) : (list label):=
    match p with
    |prog_block l i p' =>
      l :: (labelFromProgramAux p')
    |prog_end => []
    end.

Definition labelFromProgram (p : program) : G label :=
  elems_ xH (labelFromProgramAux p).

(*
 * Generates a program and a program typing that typecheck, then it chooses
 * a program label and fetches the labeled instruction and its typing
 * environment. Finally it generates an environment (a store) that typechecks
 * with the fetched typing environment.
 *)
Definition genInstructionContext (size: nat) 
      : G(option
           (program * program_typing * store * env *
             label * instr)) :=
  do! p' <- genProgram size ;
  match p' with
  |Some (p, pi) =>
    do! l <- labelFromProgram p ;
    match pi ## l with
    |Some g =>
        do! s' <- storeFromEnv g;
        match s' with 
        |Some s => 
          match program_lookup p l with
          |Some i => returnGen (Some(p, pi, s, g, l, i))
          |None => returnGen None
          end
        |None => returnGen None
        end
    |_ => returnGen None
    end
  |_ => returnGen None
  end.

(* Sample (sized(genInstructionContext)). *)

Definition someOrNone {A:Type}(t:option A) :=
  match t with
  |None => "None"
  |Some _ => "Some _"
  end.


Instance dec_option 
            (t1 t2 : option (program * program_typing * store * env * label *
            instr)) : Dec (t1 = t2).
Proof. dec_eq. Defined.

(*! Section LIST-MACHINE-PROP *)(*! extends MUTANTS *)

Definition progressProp :=
 forAll (sized(genInstructionContext)) (
    fun x =>
      collect (someOrNone x) (
      x <> None ? ==> (
      match x with
      | Some (p , pi, s, g, l, i) =>
          ((typecheck_block pi g i) &&
          (typecheck_program pi p) &&
          (store_has_tyBool s g) ) ==>
          step_or_haltBool p s i
      |_ => true ==> false (*non accade*)
      end)
 )).

(*! QuickCheck progressProp. *)

Definition existsEnvForStoreAndBlock (pi: program_typing) 
    (r': store) (i': instr) (g: (list env)): bool :=
    let l := List.filter (
        fun g' => store_has_tyBool r' g' && 
           (typecheck_block pi g' i')) g in
    negb(List.length l = 0 ? ).


Definition updateEnv
  (pi: program_typing) (g: env) (i: instr) : list env :=
    let empty := empty ty in
    cons empty (
        match i with
        | instr_halt => []
        | instr_jump  l => 
          match pi ## l with
          |Some e => [e]
          |_ => []
          end
        | instr_seq (instr_branch_if_nil v l) _ =>
            match typecheck_instr pi g (instr_branch_if_nil v l) with
            | Some g' => 
                match pi ## l with
                |Some e => [g'; e]
                |_ => []
                end
            | None =>  
              match pi ## l with
                |Some e =>[e]
                |_ => []
               end
            end
        | instr_seq i' _ =>
            match typecheck_instr pi g i' with
            | Some g' =>  [g']
            | None =>  []
            end
        | _ => []
        end
    ).


Definition preservationP :=
 forAll (sized(genInstructionContext)) (
    fun x =>
      collect (someOrNone x) (
      x <> None ? ==> (
      match x with
      | Some (p , pi, r, g, l, i) =>
        ( typecheck_program pi p && (store_has_tyBool r g) &&
        (typecheck_block pi g i) && (canStep p r i)) ==>
           match stepFunc r i p with
           |Some (r', i') =>
              existsEnvForStoreAndBlock pi r' i' (updateEnv pi g i)
           |None => false
           end
      |_ => true ==> false (*non accade*)
      end)
 )).

(*! QuickCheck preservationP. *)


Fixpoint steps (p: program) (r: store) (i: instr) (n: nat)
                : option(store * instr) :=
    match n with
    |0 => Some (r, i)
    |S n =>
      match stepFunc r i p with
      (*! *)
      |Some (r', i') => steps p r' i' n
      (*!! bug8-store-updated-after-every-step *)
      (*! |Some (r', i') => steps p r i' n *)
      |_ => None
      end
    end.


Definition trySteps (p: program) (r: store) 
                    (i: instr) (n: nat): bool :=
  match steps p r i n with
  |None => false
  |_ => true
  end.


Instance dec_option2 
            (t1 t2 : option (program * program_typing)) : Dec (t1 = t2).
Proof. dec_eq. Defined.


Definition soundnessP :=
  forAll (sized(genProgram)) (
    fun x =>
    collect (someOrNone x) (
    x <> None ? ==> (
      match x with
      |Some (p , pi) =>
        forAll (choose(0, 50)) (fun n =>
          let r := (empty _) ## (xH) <- value_nil  in
          let i := program_lookup p xH in
          match i with
          |Some i' =>
            (typecheck_program pi p && (trySteps p r i' n)) ==>
              match (steps p r i' n) with
              |Some (r', i'') => step_or_haltBool p r' i''
              |_ => false (*non accade*) 
              end
          |_ => true ==> false (*non accade*) 
          end
        )
      |_ => true ==> false (*non accade*) 
      end
   )
  )
  ).  

(*! QuickCheck soundnessP. *)


Definition check_subtype_reflProp :=
  forAll arbitrary (
    fun t =>
      check_subtype t t = true ?
  ).

(*! QuickCheck check_subtype_reflProp. *)

(*! Section checking_generators *)(*! extends MUTANTS *)

Definition instructionContext_typecheck_block :=
  forAll (sized(genInstructionContext)) (
    fun x =>
      collect (someOrNone x) (
       x <> None ? ==> (
      match x with
      | Some (p , pi, s, g, l, i) =>
          typecheck_block pi g i
      |_ => true
      end)
 )).

(*! QuickCheck instructionContext_typecheck_block. *)

Definition instructionContext_typecheck_program :=
  forAll (sized(genInstructionContext)) (
    fun x =>
      collect (someOrNone x) (
       x <> None ? ==> (
      match x with
      | Some (p , pi, s, g, l, i) =>
          typecheck_program pi p
      |_ => true
      end)
 )).

(*! QuickCheck instructionContext_typecheck_program. *)

Definition instructionContext_store_has_tyBool :=
  forAll (sized(genInstructionContext)) (
    fun x =>
      collect (someOrNone x) (
       x <> None ? ==> (
      match x with
      | Some (p , pi, s, g, l, i) =>
          store_has_tyBool s g
      |_ => true
      end)
 )).

(*! QuickCheck instructionContext_store_has_tyBool. *)


(*! Section sampling_generators *)

Definition SomeGen {A} (g: G A) :=
  liftGen (Some) g.

Fixpoint genMap_size  (A:Set) (g: G A) (n:nat) : G (map A) :=
  match n with
    | 0 => returnGen (Leaf A)
    | S n' =>
        do! m <- choose (0, n');
        freq [(1,returnGen (Leaf A));
               (n, liftGen3 (Node A)(genMap_size A g (n'-m)) 
                             (SomeGen g)
                             (genMap_size A g (n'-m)))]
    end.

Derive Arbitrary for positive. (*gen label*)

Derive Arbitrary for ty.

(*env = map ty*)
Definition genNotEmptyEnv : G (map ty):=
  do! e <- sized(genMap_size ty arbitrary);
  if isEmpty _ e then 
    returnGen (empty _) ## xH <- (ty_listcons ty_nil)
  else returnGen e.

(* Sample genNotEmptyEnv.*)

(*program_typing = map env : Set*)
Definition genProgramTyping : G (map env) := 
  do! pi <- sized(genMap_size env genNotEmptyEnv);
  if isEmpty _ pi then
    do! e <- genNotEmptyEnv;
    returnGen (empty _) ## xH <- e
  else returnGen pi.

(* Sample genProgramTyping.*)

(*store = map value: Set*)
Definition genStore : G (map value) := 
  sized(genMap_size value arbitrary) .

(* Sample genStore.*)


Definition genConsTest : G (option (instr * env)) :=
  do! e <- genNotEmptyEnv;
  genCons e.

(* Sample genConsTest. *)

Definition genBranchIfNilTest : G (option (instr * env)) :=
  do! l <- arbitrary ;
  do! pi <- genProgramTyping; 
  do! e <- genNotEmptyEnv;
  genBranchIfNil (List.last l xH) pi e.

(* Sample genBranchIfNilTest. *)

Definition genFetchFieldTest : G (option (instr * env)) :=
  do! e <- genNotEmptyEnv;
  genFetchField e.

(* Sample genFetchFieldTest. *)

Definition genInstructionTest : G (option (instr * env)) :=
  do! l <- arbitrary; (*gen label*)
  do! pi <- genProgramTyping; 
  do! e <- genNotEmptyEnv;
  genInstruction l pi e.

(* Sample genInstructionTest. *)

Definition genInstructionsTest : G (option ((list instr ) * env)) :=
  do! l <- arbitrary; (*gen label*)
  do! pi <- genProgramTyping; 
  do! e <- genNotEmptyEnv;
  do! size <- arbitrary;
  genInstructions l pi e size.

(* Sample genInstructionsTest. *)

Definition genBlockEndTest  :=
  do! labels <- sized(genLabels);
  let la := xH in
  let pi := ofList (List.map (fun l => (l, empty ty)) labels) in
  do! e <- genNotEmptyEnv ; 
  let initialEnv := (empty _) ## xH <- ty_nil in
  match setBlockTypingIfEmpty pi xH initialEnv with
  |Some pi' => genBlockEnd la pi' e
  |None => returnGen None
  end.

(* Sample genBlockEndTest. *)

Definition genBlockTest : G (option (instr * program_typing)) :=
  do! labels <- sized(genLabels);
  let la := hd xH labels in
  let pi := ofList (List.map (fun l => (l, empty ty)) labels) in
  let initialEnv := (empty _) ## xH <- ty_nil in
  match setBlockTypingIfEmpty pi xH initialEnv with
  |Some pi' => 
     do! size <- arbitrary;
     genBlock la pi' size
  |None => returnGen None
  end.

(* Sample genBlockTest. *)

Definition genBlocksTest : G (option (list(label * instr) * program_typing)) :=
  do! labels <- sized(genLabels);
  let la := List.last labels xH in
  let pi := ofList (List.map (fun l => (l, empty ty)) labels) in
  let initialEnv := (empty _) ## xH <- ty_nil in
  match setBlockTypingIfEmpty pi xH initialEnv with
  |Some pi' => 
     do! size <- arbitrary;
     genBlocks (Pos.to_nat la) pi' size
  |None => returnGen None
  end.

(* Sample genBlocksTest. *)

Definition genValuesTest : G (option (list (var * value))) :=
  do! var <- arbitrary; 
  do! t <- arbitrary;
  do! l <- listOf (returnGen (var,t));
  genValues l.

(* Sample genValuesTest. *)

Definition storeFromEnvTest : G (option store) :=
  do! e <- genNotEmptyEnv;
  storeFromEnv e.

(* Sample storeFromEnvTest. *)


(*! Section TEST_MAP *)

Definition prop_get_empty := get_empty.

(*! QuickChick prop_get_empty.*)
(*+++ Passed 500 tests (0 discards)*)

Definition prop_get_leaf := get_leaf.
(*! QuickChick prop_get_leaf. *)
(*
QuickChecking prop_get_leaf
+++ Passed 500 tests (0 discards)
*)
 
Definition prop_get_set (A:Set) (g:G A) 
                              `{Show (A)} `{Show (map A)} :=
    forAll arbitrary( fun n:positive =>
      forAll (sized(genMap_size A g))(
        fun (m:map A) => 
              forAll g (
                 fun (x:A) =>
                  match get _ (set _ m n x) n with
                  |Some x => true
                  |_ => false
                  end
              )
      )
    ).

Definition prop_get_set_value := prop_get_set value arbitrary.

(*! QuickChick prop_get_set_value. *)
(*
QuickChecking prop_get_set_value
+++ Passed 500 tests (0 discards)
*)










