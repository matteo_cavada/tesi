Require Import MutantChick.Base.
Require Import MutantChick.Mutator.
Import MetaCoq.Template.monad_utils.MonadNotation.

Require Import MetaCoq.Template.All.

Require Import MutantChick.Mutator.
Require Import MutantChick.Base.
Require Import MutantChick.utils.
Require Import MutantChick.InductiveTypes.

Require Import Arith String List Bool.
Require Import Coq.Unicode.Utf8.

Import ListNotations MonadNotation.
Open Scope list_scope.


Inductive func_2_args := (* Contenitore per funzione con due argomenti uguali *)
| func2 {A} (f: A -> A -> A).


(* Trasforma una funzione di due argmenti (f: a->a->a) in un operatore in cui
   si tiene solo il primo argomento e si butta via tutto il resto *)
Definition keep_fst_arg  (foo: func_2_args) : TemplateMonad Operator :=
  quoted_func <- tmQuote foo ;;
  ret (UserDefined
         (fun tm => match tm with
                    | tApp func' [_;_] => terms_eq func' quoted_func
                    | _ => false end)
         (fun tm => match tm with
                    | tApp func' [b1;_] =>
                      if terms_eq func' quoted_func
                      then b1
                      else tm
                    | _ => tm end)).
Definition keep_snd_arg  (foo: func_2_args) : TemplateMonad Operator :=
  quoted_func <- tmQuote foo ;;
  ret (UserDefined
         (fun tm => match tm with
                    | tApp func' [_;_] => terms_eq func' quoted_func
                    | _ => false end)
         (fun tm => match tm with
                    | tApp func' [_;b2] =>
                      if terms_eq func' quoted_func
                      then b2
                      else tm
                    | _ => tm end)).
Definition swap_args  (foo: func_2_args) : TemplateMonad Operator :=
  quoted_func <- tmQuote foo ;;
  ret (UserDefined
         (fun tm => match tm with
                    | tApp func' [_;_] => terms_eq func' quoted_func
                    | _ => false end)
         (fun tm => match tm with
                    | tApp func' [b1;b2] =>
                      if terms_eq func' quoted_func
                      then tApp func' [b2;b1] 
                      else tm
                    | _ => tm end)).
Definition double_fst_args  (foo: func_2_args) : TemplateMonad Operator :=
  quoted_func <- tmQuote foo ;;
  ret (UserDefined
         (fun tm => match tm with
                    | tApp func' [_;_] => terms_eq func' quoted_func
                    | _ => false end)
         (fun tm => match tm with
                    | tApp func' [b1;_] =>
                      if terms_eq func' quoted_func
                      then tApp func' [b1;b1] 
                      else tm
                    | _ => tm end)).
Definition double_snd_args (foo: func_2_args) : TemplateMonad Operator :=
  quoted_func <- tmQuote foo ;;
  ret (UserDefined
         (fun tm => match tm with
                    | tApp func' [_;_] => terms_eq func' quoted_func
                    | _ => false end)
         (fun tm => match tm with
                    | tApp func' [_;b2] =>
                      if terms_eq func' quoted_func
                      then tApp func' [b2;b2] 
                      else tm
                    | _ => tm end)).

                                         
Definition operators_2_args (l: list func_2_args) :=
  ops1 <- monad_map keep_fst_arg l ;;
  ops2 <- monad_map keep_snd_arg l ;;
  ops3 <- monad_map swap_args l ;;
  let ops' := ops1 ++ ops2 ++ ops3 in 
  ops <- tmEval all ops' ;;
  ret ops'.
  



                    
Definition aa (a b: nat) : nat :=
  a - b.
Definition bb (a: nat) (b: nat) : nat :=
  a + b * a.

Definition functions := [func2 aa; func2 bb].

Run TemplateProgram (
      t <- operators_2_args functions;;
      tt <- tmEval all t ;;
      tmPrint tt
    ).

  
Definition ehi (b: bool) (n: nat) :=
  match n with
  | 0 => if negb b then bb n 3 else 0
  | S n' => match n' with
            | 0 => if negb b then aa 2 n else 1
            | _ => 3
            end
  end.


Run TemplateProgram (
      ops <- operators_2_args functions ;; 
      MultipleMutate <% ehi %>
                     using ops
                     named "ciao"
                     generating (RandomMutations 77 50)
    ).

Print ciao.      
Print ciao0.      
Print ciao1.      
Print ciao2.      



(* Trasforma una funzione di due argmenti (f: a->a->a) in un operatore in cui
   si tiene solo il primo argomento e si butta via tutto il resto *)
Definition keep_fst_arg (foo: func_2_args) : TemplateMonad Operator :=
  match foo with
  | func2 _ func =>
      quoted_func <- tmQuote func ;;
      ret (UserDefined
             (fun tm => match tm with
                        | tApp func' [_;_] => terms_eq func' quoted_func
                        | _ => false end)
             (fun tm => match tm with
                        | tApp func' [_;b2] =>
                          if terms_eq func' quoted_func
                          then b2
                          else tm
                        | _ => tm end))
  end.
