Require Import Arith Bool List ZArith. Import ListNotations.
Require Import Coq.Lists.List.
From QuickChick Require Import QuickChick.
Import QcNotation. (* Suppress some annoying warnings: *)
Set Warnings "-extraction-opaque-accessed,-extraction". 
 Require Import String. Local Open Scope string. 


Extract Constant defNumTests  => "200". 

Inductive nt : Set := 
  |zz 
  | ss (n :nt) 
  | ad (n1 n2 : nt).

Derive(Arbitrary,Show) for nt.

Fixpoint eval (n : nt) : nat := 
  match n with
   | zz =>  0
   | ss m => S (eval m)
   | ad n1 n2 => (eval n1) + (eval n2)
   end.
   
Fixpoint quote (n : nat) : nt := 
   match n with
    | 0 =>  zz
    | S m => ss (quote m)
    end.

Fixpoint isnf (n :nt) := 
  match n with 
  | zz => true (*  to have cex make it false*)
  | ss m => isnf m 
  | _ => false end.

Conjecture cbn: forall (n : nt), isnf (quote (eval n)) = true.
(* QuickCheck cbn. *)
(*
very weak property that says nothing of what we eval and quote to, so far they are numerals
*)

(*  equality among nt is dedidable, so that
?arg_2 : "Checkable (forall n : nt, quote (eval n) = n)"
*)

Instance eq_dec_nt (x y : nt) : Dec (x = y).
Proof. dec_eq. Defined.

Conjecture eq: forall (n : nt),  (quote (eval n)) = n.
QuickCheck eq.
(* cex:  ad zz zz*)


Definition eq_spec (x : nt)  := (quote (eval x) = x)?.
QuickCheck eq. (* we need the coercion to bool here but not in the conjecture *)

Definition eq_spec_nf (x : nt)  :=
   isnf x ==> (quote (eval x) = x ?).
QuickCheck eq_spec_nf.


Conjecture qe: forall (n : nat),  (eval (quote n)) = n.
QuickCheck qe.
(* ok, since nat equality is checkable *)

Definition qe_spec (x : nat)  := (eval (quote x) = x)?.
QuickCheck qe_spec.

(*
Lemma cbn: forall (n : nt), isnf (quote (eval n)) = true.
induction n; simpl.
- reflexivity.
-  trivial.
-  (* isnf (quote (eval n1 + eval n2)) = true*)
Abort.
*)


(*  now, we do it ourselves, rather than via datatype automation*)

Fixpoint genFSized (sz : nat)  : G (nt) :=
  match sz with
    | O => ret zz
    | S sz' =>
      freq [
          (1, ret zz) ;
        (sz, liftM ss (genFSized sz')) ;
        (sz, liftM2 ad  (genFSized sz') (genFSized sz'))
        ]
        end.

Sample (genFSized 3).
Definition genF : G nt := sized genFSized.
Sample genF.

QuickCheck (forAll genF eq_spec).

QuickCheck (forAll genF eq_spec_nf).

Open Scope list.

Fixpoint shrinkNtAux 
               (t : nt)
            : list nt :=
  match t with
  | zz => []
  | ss n => [n] ++ map ss (shrinkNtAux  n)
  | ad l r => [l] ++ [r] ++ 
         map (fun l' => ad l' r) (shrinkNtAux l) ++
         map (fun r' => ad  l r') (shrinkNtAux  r)
  end.
Check shrinkNtAux.

Eval compute in (shrinkNtAux (ad zz (ss (ss zz)))).

Instance shrinkNT   : Shrink ( nt) :=
  {| shrink x := shrinkNtAux  x |}.
Print shrinkNT.


(* the call via custom generator and shrinker *)
  QuickChick 
       (forAllShrink 
          (genFSized 3) 
          shrink
          eq_spec). 

  (* removing ==> *)
Fixpoint genISNFSized (sz : nat)  : G (nt) :=
  match sz with
    | O => ret zz
    | S sz' =>
      freq [
          (1, ret zz) ;
        (sz, liftM ss (genISNFSized sz')) ]
  end.

(* this succeeds since it restricts to nf *)
QuickChick 
       (forAllShrink 
          (genISNFSized 3) 
          shrink
          eq_spec). 
(* or by retaining isnf using suchThat. Since it is partial, we redefine the spec*)
Check suchThatMaybe.

Definition isnfGen (sz : nat) :=
  (suchThatMaybe (genFSized sz) isnf).

Definition eq_spec_opt (x : option nt )  :=
  match x with
    Some y => (quote (eval y) = y)?
  | _ => true
    end.
QuickChick 
       (forAllShrink 
          (isnfGen 3) 
          shrink
          eq_spec_opt). 

Inductive isnfP : nt -> Prop :=
|izz : isnfP zz
| iss n : isnfP n -> isnfP (ss n).

Inductive evalP : nt -> nat -> Prop :=
  ievz : evalP zz O
| ievs n m : evalP n m -> evalP (ss n) (m + 1)
| ieva n1 n2 m1 m2: evalP n1 m1 -> evalP n2 m2 -> evalP (ad n1 n2) (m1 + m2).
Print evalP.

Inductive quoteP : nat -> nt -> Prop :=
  iqz : quoteP O zz
| ieq n m : quoteP n m -> quoteP (n + 1) (ss m).


Lemma qel: forall n, isnf n = true -> (quote (eval n)) = n.
induction n; simpl.
- auto.
-  intros. simpl. apply IHn in H. rewrite H. trivial.
-  intros. inversion H.
Qed.

Lemma eql: forall n, (eval (quote n)) = n.
  induction n; simpl; try rewrite IHn; try reflexivity.
Qed.
