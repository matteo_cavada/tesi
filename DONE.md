# Diario 

### Aggiornamenti delle ultime due settimane

	[2020-01-12]
	
+ Fatto un grande refactoring della libreria, principalmente per due ragioni:
  
  1. Poniamo per esempio l'espressione `firstn 2 [2;5;2]` su cui applichiamo la mutazione banale `2 ==> 9`. Prima, la libreria avrebbe ritornato una nuova definizione comprendente tutte le possibili mutazioni assieme, quindi `firstn 9 [9; 5; 9]`. Al contrario, ora vengono ritornate tre definizioni: `firstn 2 [2;5;9]`, `firstn 2 [9;5;2]` e `firstn 9 [2;5;2]`. Il problema è che ora l'algoritmo di sostituzione è un po' delicato da modificare ed è per niente bello, ma almeno questo problema è stato risolto. Idea per il futuro: generare tutti i possibili alberi sintattici mutati segue un meccanismo simile alla monade del nondeterminismo di Haskell: trarre ispirazione da lì per ripulire il codice? 
  
  2. Com'era prima, la libreria era pensata per applicare trasformazioni da termine `t1` a termine `t2`. Ora, aggiunto il tipo induttivo `Operator T`, è possibile usare operatori di livello leggermente più alto (ad es. ho implementato lo scambio delle clausole then e else, ma se ne potrebbero pensare altre es. riguardanti il pattern matching ecc, oltre a quelle già discusse di modifica di definizioni induttive).
  
+ Riformulate un po' le notazioni, fattorizzato codice per renderlo più chiaro, aggiunti diversi commenti, aggiunti diversi test qua e là giusto per sanity check


### Aggiornamenti delle ultime due settimane
	
	[2019-12-31]

+ Aggiunte mutazioni type-preserving.
+ Aggiunta notazione simil-muCheck (aka. operatori `==>` e `==>*`) 
+ Creata una libreria ad-hoc invece di multipli file tipo `esperimenti1.v` ecc. Adottato, per quanto possibile, uno stile monadico.
+ Il tentativo di aggiungere un comando vernacolare che coniughi la generazione di mutanti e il loro testing via Quickchick è fallito (per il momento).

	L'idea era di creare un'espressione vernacolare tipo:
	
		MutateAndChick definizione_1 (A ==> B) congettura_1.
		
	Il problema è che aggiungere un nuovo comando vernacolare richiede la stesura di un plugin in Ocaml, il che richiede una conoscenza profonda delle API Ocaml di Coq. Esaminare il codice sorgente di altri plugin (per es. vedi MetaCoq che esporta il comando `Run TemplateProgram`, o QuickChick stesso che esporta `QuickChick`) non ha aiutato granché. Si può ovviamente ritornare su questo punto, ma siccome dopo qualche giorno mi sono incartato e non riuscivo a proseguire ho momentaneamente accantonato l'idea per un momento futuro.
	
**Obiettivi** per il futuro prossimo:

+ Integrare le mutazioni generate con QuickChick (questo implica: capire _bene_ come funziona QuickChick su esempi non triviali).
+ Ampliare lo spettro delle possibili mutazioni:
  + Aggiungere nuove possibilità di mutazione dei tipi induttivi (es. creare funzioni `rimuovi_costruttore`, `modifica_costruttore` ecc...)
  + Aggiungere possibilità di mutare lambda / definizioni con parametri

___

### Esaminato MuCheck e come genera i mutanti

	[2019-12-15]
	
Vedi [link](https://bitbucket.org/matteo_cavada/tesi/src/master/appunti/mucheck.md) per ulteriori informazioni. Le due domande di fondo erano:

1. Gli operatori, creabili anche dall'utente, preservano i tipi? (no)
2. Ci sono cose utili che si possono "rubare" da mucheck? (è ancora presto per dirlo, ma probabilmente si)
	
___

### Primi, basilari operatori mutazionali in Coq, via MetaCoq

	[2019-12-11]
	
(v. [file](https://bitbucket.org/matteo_cavada/tesi/src/master/metacoq/esperimenti1.v) su bitbucket)

Alcuni esempi tratti dal file sopra in cui si manipolano gli AST (anche se non si direbbe):

	Make Definition temp1 := 3 becomes 5 inside 3. (* uguale a sopra, diversa sintassi però *)
	Print temp1. (* 5 *)

	Make Definition temp2 := (@ nil nat) becomes [99;100] inside [0;1].
	Print temp2. (* [0; 1; 99; 100] *)

	Make Definition temp3 := 3 becomes 4 inside [1;2;3;4;5;6;7].
	Print temp3. (* [1; 2; 4; 4; 5; 6; 7] *)

	Make Definition temp4 := (Coq.Init.Nat.add) becomes (Coq.Init.Nat.mul) inside (3 + 4).
	Print temp4. (* (3 * 4) *)

	Make Definition temp5 := true becomes false inside [true; true; true].
	Print temp5. (* [false; false; false] *)

Un esempio di definizione mutata:

	(* Definizione semplice *)
	Definition def_da_mutare (n: nat) :=
	   n + 0.
	Compute (def_da_mutare 4). (* => 4 *)

	(* Quoto la definizione *)
	Quote Definition def_da_mutare_quoted := Eval compute in def_da_mutare.

	Definition create_mutant_def (newName: ident) (f: term -> term) (oldTerm: term) :=
		tmMkDefinition newName (f oldTerm).

	(* Genera il mutante *)
	Run TemplateProgram (
		create_mutant_def "def_mutata" (0 diventa 1) def_da_mutare_quoted
		).

	Compute (def_mutata 4). (* => 5 *)

Non è molto, ma è un primo passo; i passi successivi saranno:

1. Integrare questo meccanismo di "mutazione" con QuickCheck, se possibile.
2. Espandere il meccanismo di mutazione.
   
   Per ora è moooolto naive, sono ammesse solo sostituzioni dirette 1 a 1 (tipo: `0 becomes 1`, `false becomes true`, `[] becomes [1;2;3]`, `+ becomes *`, ecc...), in pratica sono semplici sostituzioni tipografiche; ma se per esempio volessi fare una cosa tipo `x::xs becomes xs` non riuscirei!


___

### Scrivere operatori mutazionali in Coq, via MetaCoq?
	
	[2019-12-09 lun]

Si potrebbero scrivere gli operatori mutazionali direttamente in COQ, magari sfruttando una libreria come [[https://github.com/MetaCoq/metacoq][MetaCoq]]; ciò avrebbe numerosi vantaggi, ma forse anche svantaggi:

#### Pro

+ Essendo integrati nella sintassi di COQ, gli operatori così definiti non risentono di eventuali modifiche delle API a basso livello in versioni future di Coq
+ Semplicità di definizione di nuovi operatori, anche user-defined magari
+ Soluzione potenzialmente molto elegante 

#### Contro

Non è immediato capire come integrare tutto ciò con QuickChick e i suoi metodi per l'analisi mutazionale. Per esempio, si può immaginare di tradurre questo snippet scritto con una sintassi pseudo-Quickchick:

	(*! ZBO *)                         # l'operatore che trasforma gli 0 in 1
	Definition silly (n: nat): n + 0.

con il seguente pseudocodice in Metacoq (ipotizzando che esista una funzione generate_mutant che fa esattamente ciò che suggerisce il nome):

	Run TemplateProgram (generate_mutant <% silly %> ZB0).

Ma poi? Come sfruttiamo il mutante così generato? Tra l'altro SERAPI certamente non riesce ad esportare in SEXP il codice generato.

Da approfondire con più calma. 

___

### Uguaglianza tra programmi, via python

	[2019-12-09 lun]

Scritta un'estensione al programmino in Python che, via quickchick, testa se una definizione mutata e la definizione originaria sono uguali. Il codice è per ora molto disordinato; in ogni caso, il processo è più o meno il seguente:

1. Lo script Python muta le definizioni segnalate dal commento `(* @mutate *)` nel file .v; per esempio, la definizione

		(* @ mutate *)
		Definition silly (n: nat) (m: nat) := n + m.

	con l'operatore NPM (che trasforma il + in -) diventerebbe

		Definition silly' (n: nat) (m: nat) := n - m.


2. Viene fatto il check del file così mutato. Nel caso in cui Coq non riesca a fare il typecheking nel mutante, il mutante è considerato ucciso.


3. In caso contrario, il mutante è rimasto vivo; viene aggiunto in calce al file .v mutante una congettura sulla falsariga di:

		Conjecture definition_equality: forall (x y: nat),
			silly x y = silly' x y.

		QuickChick definition_equality.
